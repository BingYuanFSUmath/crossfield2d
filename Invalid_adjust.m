function Q = Invalid_adjust(Q,ex)
%adjust invalid quads
%INPUT:
%Q: Quad class
%OUTPUT:
%Q: Quad class

S = SingularityQ; %record S in invalid Q
n = size(Q.Q,1);
Qnew = Q;
for i = 1:n
    if size(Q.Q{i},2) ~= 4 %invalid quad
        [Qnew,S] = addS(Q,Qnew,S,i,ex); %find S 
    end
end
S = S(2:end);
n = size(S,2);
S_status = zeros(n,3);
for i =1:n
    S_status(i,:) = S(i).status;
end
Q = propagateQ(Q,Qnew,S,S_status,ex); %update Q
%convert Q,QE to array
n = size(Q.Q,1);
QQ = zeros(n,4);
QE = zeros(n,4);
for i = 1:n
    QQ(i,:) = Q.Q{i};
    QE(i,:) = Q.QE{i};
end
Q.Q = QQ;
Q.QE = QE;
if size(S,2)>0 %exist invalid quad
    %construct Q.C
    P = Q.P;
    E = Q.E;
    C = cell(size(P,1),1);
    for i = 1:size(P,1) %point id
        for j = 1:size(E,1) %edge id
            if (i==E(j,1))||(i==E(j,2)) %on edge(j)
                C{i} = [C{i},j];
            end
        end
    end
    Q.C = C;
end
end

function [Qnew,S] = addS(Q,Qnew,S,id,ex)
%Q: quad class
%id: id of invalid Q
if size(Q.Q{id},2) == 3 %triangle
    [S,Qnew] = generateS(S,Q,Qnew,id,ex);
else %pentagon
    theta = zeros(5,1); %5 angles
    mid_angle = zeros(5,1); %record mid angle direction
    for i = 1:5
        pid = Q.Q{id}(i);
        p0 = Q.P(pid,:);
        j = mod(i-2,5)+1;%QE{id}(i-1)
        %find p1 on E_x
        edge_id = Q.QE{id}(j);
        if Q.E(edge_id,1)==pid
            p1 = Q.E_x{edge_id}(2,:);
        else
            p1 = Q.E_x{edge_id}(end-1,:);
        end
        %find p2 on E_x
        edge_id = Q.QE{id}(i);
        if Q.E(edge_id,1)==pid
            p2 = Q.E_x{edge_id}(2,:);
        else
            p2 = Q.E_x{edge_id}(end-1,:);
        end        
        v1 = p1-p0;
        v2 = p2-p0;
        theta(i) = acos(dot(v1,v2)/(norm(v1)*norm(v2))); %[pi/5,pi/3]
        mid_angle(i) = 0.5*(v1/norm(v1)+v2/norm(v2));
    end
    angle_max = 0;
    for i = 1:5
        j = mod(i-2,5)+1; %mod(i-1,n)+1 
        k = mod(i,5)+1;
        angle = theta(j)+theta(k); %sum of two neighbour angles
        if angle>angle_max
            max_i = i;
            pid = [Q.Q{id}(i),Q.Q{id}(j),Q.Q{id}(k)];
            eid = [Q.QE{id}(i),Q.QE{id}(j),Q.QE{id}(k)];
        end
    end
    %add new edge p(j)p(k) to Q.E
    Q.E(end+1) = [pid(2),pid(3)];
    edge_id = size(Q.E,1);
    %add new edge to Q.E_x
    p0 = Q.P(pid(2),:);
    p1 = Q.P(pid(3),:);
    m0 = mid_angle(mod(max_i-2,5)+1,:);
    m1 = mid_angle(mod(max_i,5)+1,:);
    a = norm(p1-p0); %tangent size factor
    m0 = a*m0/norm(m0);
    m1 = a*m1/norm(m1);
    t = 0:0.05:1; %can modify later to spectral nodes
    t = t'; %column vector
    x = (2*t.^3-3*t.^2+1)*p0...
        +(t.^3-2*t.^2+t)*m0...
        +(-2*t.^3+3*t.^2)*p1...
        +(t.^3-t.^2)*m1;
    Q.E_x(end+1) = x;
    %add new quad to Q.Q
    Qids = zeros(1,4);
    QEids = zeros(1,4);
    for i = 1:4
        Qids(i) = Q.Q{id}(mod(max_i+i-1,5)+1);
        QEids(i) = Q.QE{id}(mod(max_i+i-1,5)+1);
    end
    Q.Q{end+1} = Qids;
    %add new quad to Q.QE
    Q.QE{end+1} = QEids;
    Q.QE{end}(4) = edge_id; %new edge
    %modify Q.Q{id}
    Q.Q{id} = pid;
    %modify Q.QE{id}
    Q.QE{id} = eid;
    Q.QE{id}(4) = edge_id; %new edge
    %modify Q.C
    Q.C{pid(2)}(end+1) = edge_id;
    Q.C{pid(3)}(end+1) = edge_id;
    [S,Qnew] = generateS(S,Q,Qnew,id,ex);
end
end
        
    

function [S,Qnew] = generateS(S,Q,Qnew,id,ex)
%generate S in triangle
sid = size(S,2)+1;
S(sid).qid = id; %Q id contains S
S(sid).pids = Q.Q{id};
S(sid).eids = Q.QE{id};
S(sid).pnew = zeros(3,1);
S(sid).enew = zeros(3,3);
S(sid).status = zeros(1,3);
%transfinite interpolation for triangle
x = zeros(3,2);
for i = 1:3
    x(i,:) = Q.P(S(sid).pids(i),:);
end
v = cell(3,1); %edge functions
nodes = Q.E_x(S(sid).eids,:);
for i = 1:3
    if Q.E(S(sid).eids(i),1)~=S(sid).pids(i)
        nodes{i} = flipud(nodes{i});
    end
    v{i} = @(t) piecewise_linear(nodes{i},t);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %use exact bdy for bdy edges
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %2 bdy pts ~= bdy edge
    if S(sid).eids(i)<=Q.nbpts
        %boundary edge
        for k = 1:ex.curve_num
            t1 = ex.curve(k).find_t(nodes{i}(1,:));
            t2 = ex.curve(k).find_t(nodes{i}(end,:));
            if t1>=0 && t2>=0
                break
            end
        end
        if t1<0||t2<0
            error('Error. Can not find t.')
        end
        if t2-t1>1+t1-t2 %cross 0/1
            v{i} = @(t) ex.curve(k).position_at(mod(t1-(1+t1-t2)*t+1,1));
        elseif t1-t2>1+t2-t1
            v{i} = @(t) ex.curve(k).position_at(mod(t1+(1+t2-t1)*t,1));
        else
            v{i} = @(t) ex.curve(k).position_at(t1+(t2-t1)*t);
        end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end
xy = @(l1,l2,l3) l1*(v{1}(l2)+v{3}(1-l3)-x(1,:))...
    +l2*(v{2}(l3)+v{1}(1-l1)-x(2,:))...
    +l3*(v{3}(l1)+v{2}(1-l2)-x(3,:));
E_x3 = cell(3,1);
N = 7;
xc = xy(1/3,1/3,1/3); %center of triangle
for i = 1:3
    E_x3{i} = zeros(N,2);
    E_x3{i}(1,:) = xc;
    for j = 2:N
        t1= 1/3+(j-1)/(6*(N-1));
        t2 = 1/3-(j-1)/(3*(N-1));
        switch i
            case 1
                l1 = t1; l2 = t1; l3 = t2;
            case 2
                l1 = t2; l2 = t1; l3 = t1;
            case 3
                l1 = t1; l2 = t2; l3 = t1;
        end
        E_x3{i}(j,:) = xy(l1,l2,l3);
    end
end
%{
%test plot of transfinite interpolation
figure
for i = 1:3
    plot(nodes{i}(:,1),nodes{i}(:,2),'b')
    hold on
    plot(E_x3{i}(:,1),E_x3{i}(:,2),'r')
    hold on
end
hold off
%}
%update Qnew
Qnew.P(end+1,:) = xc;
cid = size(Qnew.P,1);
% contain boundary edge
for i = 1:3 %three directions
    n_old = Q.nbpts;
    n = Qnew.nbpts;
    Q0 = S(sid).pids;
    QE = S(sid).eids;
    %update Q0,QE, ids in Qnew
    for j = 1:3
        if Q0(j)>n_old
            Q0(j) = Q0(j)+n-n_old;
        end
        if QE(j)>n_old
            QE(j) = QE(j)+n-n_old;
        end
    end
    eid = S(sid).eids(i); %edge id in Q
    eidnew = QE(i); %edge id in Qnew
    S(sid).Snew = Q0; %update Snew
    if eid <= n_old
        S(sid).status(i) = 1;
        p = E_x3{i}(end,:); %new pt
        P = Qnew.P;
        Qnew.P = [P(1:n,:);p;P(n+1:end,:)];
        S(sid).pnew(i) = n+1;
        Qnew.nbpts = Qnew.nbpts+1;
        %update pids in E
        E = Qnew.E;
        for j = n+1:size(E,1)
            for k = 1:2
                if E(j,k)>n
                    E(j,k) = E(j,k)+1;
                end
            end
        end
        %update Q0,QE
        for j = 1:3
            if Q0(j)>n
                Q0(j) = Q0(j)+1;
            end
            if QE(j)>n
                QE(j) = QE(j)+1;
            end
        end
        %update cid
        cid = cid+1;
        %add E,E_x
        id1 = mod(i,3)+1; %next pt id
        pid = S(sid).pnew(i);
        e1 = [pid,Q0(id1)];
        e2 = [Q0(i),pid];
        e3 = [cid,pid];
        N = 6;
        E_x1 = zeros(N,2); E_x2 = zeros(N,2);
        for j = 1:N
            t2 = 0.5/(N-1)*(j-1);
            t1 = t2+0.5;
            E_x1(j,:) = v{i}(t1);
            E_x2(j,:) = v{i}(t2);
        end
        Qnew.E = [E(1:eid-1,:);e1;E(eid+1:n,:);e2;E(n+1:end,:);e3];
        S(sid).enew(i,:) = [eid,n+1,size(Qnew.E,1)];
        E_x = cell(size(Qnew.E,1),1);
        E_x(1:n) = Qnew.E_x(1:n);
        E_x{eid} = E_x1;
        E_x{n+1} = E_x2;
        E_x(n+2:end-1) = Qnew.E_x(n+1:end);
        E_x(end) = E_x3{i};
        Qnew.E_x = E_x;
        %update Qnew.Q/QE
        for j = 1:size(Qnew.Q,1)
            for k = 1:size(Qnew.Q{j},1)
                if Qnew.Q{j}(k)>n
                    Qnew.Q{j}(k) = Qnew.Q{j}(k)+1;
                end
                if Qnew.QE{j}(k)>n
                    Qnew.QE{j}(k) = Qnew.QE{j}(k)+1;
                end
            end
        end
    else %not a bdy edge
        p = E_x3{i}(end,:);
        Qnew.P(end+1,:) = p;
        pid = size(Qnew.P,1);
        S(sid).pnew(i) = pid;
        id1 = mod(i,3)+1; %next pt id
        e1 = [pid,Q0(id1)];
        e2 = [Q0(i),pid];
        e3 = [cid,pid];
        N = 6;
        E_x1 = zeros(N,2); E_x2 = zeros(N,2);
        for j = 1:N
            t2 = 0.5/(N-1)*(j-1);
            t1 = t2+0.5;
            E_x1(j,:) = v{i}(t1);
            E_x2(j,:) = v{i}(t2);
        end
        Qnew.E(eidnew,:) = e1;
        Qnew.E(end+1,:) = e2;
        Qnew.E(end+1,:) = e3;
        ne = size(Qnew.E,1);
        S(sid).enew(i,:) = [eidnew,ne-1,ne];
        Qnew.E_x{eidnew} = E_x1;
        Qnew.E_x{end+1} = E_x2;
        Qnew.E_x{end+1} = E_x3{i};
    end
end
%Add Q,QE
Q0 = zeros(3,4);
QE = zeros(3,4);
pnew = S(sid).pnew;
enew = S(sid).enew;
for i = 1:3
    order = mod(i,3)+1; %next pt id
    Q0(i,:) = [cid,pnew(i),S(sid).Snew(order),pnew(order)];
    QE(i,:) = [enew(i,3),enew(i,1),enew(order,2),enew(order,3)];
end
Qnew.Q{S(sid).qid} = Q0(1,:);
Qnew.Q{end+1} = Q0(2,:);
Qnew.Q{end+1} = Q0(3,:);
Qnew.QE{S(sid).qid} = QE(1,:);
Qnew.QE{end+1} = QE(2,:);
Qnew.QE{end+1} = QE(3,:); 
end


                 

    
  
