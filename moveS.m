function Snew = moveS(fd,fh,h0,SL,S,varargin)
% move singularities using force equilibrium method

dptol=.001; ttol=.1; Fscale=1.2; deltat=.2; geps=.001*h0; deps=sqrt(eps)*h0;
densityctrlfreq=30;

% 1. Construct p & e 
% p: Nx2 matrix, each row stores point location x,y
% e: Mx2 matrix, each row stores start & end point ids
% k_value: Nx1 matrix, each row stores k-value of S(i)
p = NaN(size(SL,1)*(size(SL,2)+1),2);%preallocate p,e
e = NaN(size(SL,1)*size(SL,2),2);
pfix = NaN(size(p,1),1); %pfix record fix point id
count_p = 0;%record p&e element number
count_e = 0;
for i = 1:size(SL,1)-size(S,2)
    %loop through SLs starts from bdy singularities
    for j = 1:size(SL,2)
        if SL(i,j).status %exists SL
            if size(S,2) && ismember(SL(i,j).x(end,:),[[S.x]' [S.y]'],'rows')
                %S is not empty
                %end point is an inside singularity pt
                %need to add this SL
                %check id of end point
                [p,start_id,count_p] = dup_check(p,SL(i,j).x(end,:),count_p);
                %check id of first point
                [p,end_id,count_p] = dup_check(p,SL(i,j).x(1,:),count_p);
                count_e = count_e+1;
                e(count_e,:)=[start_id end_id];
                pfix(end_id) = end_id; %end point is fixed
            end
        end
    end
end
for i = size(SL,1)-size(S,2)+1:size(SL,1)
    %loop through SLs starts from inner singularities
    for j = 1:size(SL,2)
        if SL(i,j).status %exists SL
            %check id of first point
            [p,start_id,count_p] = dup_check(p,SL(i,j).x(1,:),count_p);
            %check id of end point
            [p,end_id,count_p] = dup_check(p,SL(i,j).x(end,:),count_p);
            count_e = count_e+1;
            e(count_e,:)=[start_id end_id];
            if ~ismember(p(end_id,:),[[S.x]' [S.y]'],'rows')
                %end point is not an inner singularity 
                pfix(end_id) = end_id; %end pt is on bdy, fixed
            end
        end
    end
end
for i = 1:size(p,1)
    if isnan(p(i,1))
        p = p(1:i-1,:);%remove NaN elements
        break
    end
end
for i = 1:size(e,1)
    if isnan(e(i,1))
        e = e(1:i-1,:);%remove NaN elements
        break
    end
end
pfix = pfix(~isnan(pfix));%remove NaN elements

%pfix %test--ok!
%size(pfix,1)
Snew(1,size(p,1)) = Singularity;
for i = 1:size(p,1)
    [~,Loc] = ismember(p(i,:),[[S.x]' [S.y]'],'rows');
    if Loc %p is an inner singularity, otherwise Loc=0
        Snew(i).k = S(Loc).k;
        Snew(i).E_ID = Loc; %record id of Snew in S 
    end
end
% 2. Find fix points
%pfix=unique(pfix,'rows'); 
%nfix=size(pfix,1);
%p=[pfix; p];                                         % Prepend fix points
N=size(p,1);                                         % Number of points N

count=0;
pold=inf; 
bars = e; %edge connection is unchanged 
% For first iteration
%while 1
for i = 1:5 
%JUST ONE STEP IS GOOD  ENOUGH
%MORE STEPS CHANGE THE TOPOLOGY

  count=count+1;
  % 3. Retriangulation by the Delaunay algorithm
  if max(sqrt(sum((p-pold).^2,2))/h0)>ttol           % Any large movement?
    pold=p;                                          % Save current positions
    %bars=e;
  end

  % 6. Move mesh points based on bar lengths L and forces F
  barvec=p(bars(:,1),:)-p(bars(:,2),:);              % List of bar vectors
  L=sqrt(sum(barvec.^2,2));                          % L = Bar lengths
  hbars=feval(fh,(p(bars(:,1),:)+p(bars(:,2),:))/2,varargin{:});
  L0=hbars*Fscale*sqrt(sum(L.^2)/sum(hbars.^2));     % L0 = Desired lengths
  
  %{
  % Density control - remove points that are too close
  if mod(count,densityctrlfreq)==0 && any(L0>2*L)
      p(setdiff(reshape(bars(L0>2*L,:),[],1),1:nfix),:)=[];
      N=size(p,1); pold=inf;
      continue;
  end
  %}
  
  F=max(L0-L,0);                                     % Bar forces (scalars)
  Fvec=F./L*[1,1].*barvec;                           % Bar forces (x,y components)
  Ftot=full(sparse(bars(:,[1,1,2,2]),ones(size(F))*[1,2,1,2],[Fvec,-Fvec],N,2));
  Ftot(pfix,:)=0;                          % Force = 0 at fixed points
  p=p+deltat*Ftot;                                   % Update node positions

  % 7. Bring outside points back to the boundary
  d=feval(fd,p,varargin{:}); ix=d>0;                 % Find points outside (d>0)
  dgradx=(feval(fd,[p(ix,1)+deps,p(ix,2)],varargin{:})-d(ix))/deps; % Numerical
  dgrady=(feval(fd,[p(ix,1),p(ix,2)+deps],varargin{:})-d(ix))/deps; %    gradient
  dgrad2=dgradx.^2+dgrady.^2;
  p(ix,:)=p(ix,:)-[d(ix).*dgradx./dgrad2,d(ix).*dgrady./dgrad2];    % Project

  % 8. Termination criterion: All interior nodes move less than dptol (scaled)
  if max(sqrt(sum(deltat*Ftot(d<-geps,:).^2,2))/h0)<dptol, break; end
end

for i = 1:size(p,1)
    Snew(i).x = p(i,1);
    Snew(i).y = p(i,2);
end
Stemp(1,size(S,2)) = Singularity;
for i = 1:size(p,1)
    if Snew(i).k 
        Stemp(Snew(i).E_ID) = Snew(i);%sort Snew by id in S
    end
end
Snew = Stemp;