classdef IntersectionPtChild
    properties
        Child
        Child_SL
    end
    methods
        function self = IntersectionPtChild
            self.Child = [];
            self.Child_SL = [];
        end
        function [self,locations,SL_ids] = init_Child(self,d0,fh,box,SL,...
                locations,SL_ids)
            %recursive
            %cannot use IntersectionPt to pass locations and SL_ids
            [count,self.Child_SL] = self.count_SL(SL,box);
            %{
            %test routine for pt can not find
            if  (box(1,1)<=1.0102)&&(1.0102<=box(2,1))...
                    &&(box(1,2)<=-0.0174)&&(-0.0174<=box(2,2))...
                    &&(count<2)
                count
                box
                figure
                    scatter(box(1,1),box(1,2),'b');
                    hold on
                    scatter(box(1,1),box(2,2),'b');
                    hold on
                    scatter(box(2,1),box(1,2),'b');
                    hold on
                    scatter(box(2,1),box(2,2),'b');
                    hold on
                    for loop = 1:size(self.Child_SL,1)
                        if self.Child_SL(loop).status
                            coord=self.Child_SL(loop).x;
                            plot(coord(:,1),coord(:,2));
                            hold on
                        end
                    end
                    hold off
            end
            %}
            if count >= 2 %two or more SLs in one box
                x_min = box(1,1);
                x_max = box(2,1);
                y_min = box(1,2);
                y_max = box(2,2);
                h = 8*d0*max(max(fh([x_min,y_min]),fh([x_min,y_max])),...
                    max(fh([x_max,y_min]),fh([x_max,y_max])));
                %2d0 may not be large enough
                %what size of h will garantee box large enough?
                hx = x_max-x_min;
                hy = y_max-y_min;
                if min(hx,hy) > h %box side is larger than 4d0
                    %subdivide
                    self.Child = IntersectionPtChild;
                    self.Child(2,2) = IntersectionPtChild; %2x2
                    for i = 1:2
                        for j = 1:2
                            newbox = zeros(2,2);
                            hx = 0.5*(x_max-x_min);
                            hy = 0.5*(y_max-y_min);
                            newbox(1,1) = x_min+hx*(i-1);
                            newbox(2,1) = newbox(1,1)+hx;
                            newbox(1,2) = y_min+hy*(j-1);
                            newbox(2,2) = newbox(1,2)+hy;
                            [self.Child(i,j),locations,SL_ids]...
                                = self.Child(i,j).init_Child...
                                (d0,fh,newbox,self.Child_SL,locations,SL_ids);
                        end
                    end
                else %box is small enough
                    [location,ids] = self.find_intersection_pt...
                        (self.Child_SL);
                    locations = [locations;location];
                    %can be none or more than one
                    SL_ids = [SL_ids;ids];
                    %{
                    if size(location,1)>0
                        figure
                        axis equal
                        scatter(box(1,1),box(1,2),'b');
                        hold on
                        scatter(box(1,1),box(2,2),'b');
                        hold on
                        scatter(box(2,1),box(1,2),'b');
                        hold on
                        scatter(box(2,1),box(2,2),'b');
                        hold on
                        scatter(location(:,1),location(:,2),'r');
                        hold on
                        for loop = 1:size(self.Child_SL,1)
                            if self.Child_SL(loop).status
                                coord=self.Child_SL(loop).x;
                                plot(coord(:,1),coord(:,2));
                                hold on
                            end
                        end
                        hold off
                    end
                    %}
                end
            end
        end
    end
    methods(Static)
        function [location,ids] = find_intersection_pt(SL)
            %find intersection pt locations and intersect SL ids
            location = []; %can be none intersection pt
            ids = [];
            for i = 1:size(SL,1)-1
                if SL(i).status
                    for j = i+1:size(SL,1)
                        status = false; %finding status for SL(j)
                        if SL(j).status
                            for ix = 1:size(SL(i).x,1)-1
                                p1 = SL(i).x(ix,:);
                                p2 = SL(i).x(ix+1,:);
                                for jx = 1:size(SL(j).x,1)-1
                                    q1 = SL(j).x(jx,:);
                                    q2 = SL(j).x(jx+1,:);
                                    if (~isnan(p1(1)))&&(~isnan(q1(1)))...
                                            &&(~isnan(p2(1)))&&(~isnan(q2(1)))
                                        [x,y] = find_int_point(p1(1),p1(2),...
                                            p2-p1,q1(1),q1(2),q2(1),q2(2));
                                        e = 1e-10;%epsilon
                                        if (min(p1(1),p2(1))-e<=x&&x<=max(p1(1),p2(1))+e)...
                                                &&(min(p1(2),p2(2))-e<=y&&y<=max(p1(2),p2(2))+e)...
                                                &&(min(q1(1),q2(1))-e<=x&&x<=max(q1(1),q2(1))+e)...
                                                &&(min(q1(2),q2(2))-e<=y&&y<=max(q1(2),q2(2))+e)
                                            status = true;
                                            location(end+1,:) = [x,y];
                                            ids(end+1,:) = [i,j];
                                            %{
                                            figure
                                            axis equal
                                            plot([p1(1),p2(1)],[p1(2),p2(2)],'b')
                                            hold on
                                            plot([q1(1),q2(1)],[q1(2),q2(2)],'g')
                                            hold on
                                            scatter(x,y,'r')
                                            hold off
                                            %}
                                            break
                                        end
                                    end
                                end
                                if status
                                    break
                                end
                            end
                        end
                    end
                end
            end
        end
        function [count,newSL] = count_SL(SL,box)
            %count number of SLs and find new SL in box
            count = 0;
            newSL(size(SL,1),1) = Streamline;
            for i = 1:size(SL,1)
                if SL(i).status %SL inside larger box
                    x_start = 0;
                    x_end =0;
                    for j = 1:size(SL(i).x,1)
                        if (box(1,1)<=SL(i).x(j,1)&&SL(i).x(j,1)<=box(2,1))...
                                &&(box(1,2)<=SL(i).x(j,2)...
                                &&SL(i).x(j,2)<=box(2,2))
                            %in box
                            if ~x_start %x_start not record
                                if j>1
                                    x_start = j-1;
                                else
                                    x_start = j; %j=1
                                end
                            end
                        else %out of box
                            %record end point
                            if x_start && ~x_end
                                x_end = j;
                                %break %j loop
                                %may have more than one piece in the box
                            end
                            %cross box but no point inside box
                            if j>1 && ~x_start %no point inside box
                                p1 = SL(i).x(j-1,:);
                                p2 = SL(i).x(j,:);
                                pmid = 0.5*(p1+p2); %mid point
                                xmin = box(1,1);
                                xmax = box(2,1);
                                ymin = box(1,2);
                                ymax = box(2,2);
                                %p1,p2,pmid
                                %
                                if ((xmin<=p1(1)&&p1(1)<=xmax)...
                                        &&(ymin<=p2(2)&&p2(2)<=ymax))...
                                        ||((xmin<=p2(1)&&p2(1)<=xmax)...
                                        &&(ymin<=p1(2)&&p1(2)<=ymax))...
                                    ||((xmin<=pmid(1)&&pmid(1)<=xmax)...
                                    &&(ymin<=pmid(2)&&pmid(2)<=ymax))
                                    %}
                                    %{
                                if (xmin<=p1(1)&&p1(1)<=xmax)||...
                                        (xmin<=p2(1)&&p2(1)<=xmax)||...
                                        (ymin<=p1(2)&&p1(2)<=ymax)||...
                                        (ymin<=p2(2)&&p2(2)<=ymax)||...
                                        ((xmin<=pmid(1)&&pmid(1)<=xmax)&&...
                                        (ymin<=pmid(2)&&pmid(2)<=ymax))
                                    %}
                                    %may cross a corner of box
                                    %or cross the whole box
                                    %check intersection point with diagonal
                                    q1 = [xmin,ymin];
                                    q2 = [xmax,ymax];%q1q2:diagonal
                                    [x,y] = find_int_point(p1(1),p1(2),...
                                        p2-p1,q1(1),q1(2),q2(1),q2(2));
                                    %x,y
                                    if (xmin<=x&&x<=xmax)&&(ymin<=y&&y<=ymax)
                                        %in box
                                        x_start = j-1;
                                        x_end = j;
                                        %break %j loop
                                    end
                                    %the orther diagnal
                                    if ~x_start
                                        q1 = [xmin,ymax];
                                        q2 = [xmax,ymin];%q1q2:diagonal
                                        [x,y] = find_int_point(p1(1),p1(2),...
                                            p2-p1,q1(1),q1(2),q2(1),q2(2));
                                        if (xmin<=x&&x<=xmax)&&(ymin<=y&&y<=ymax)
                                            %in box
                                            x_start = j-1;
                                            x_end = j;
                                        end
                                    end
                                    %x,y
                                end
                            end
                        end
                        if x_start && x_end%SL cross the box
                            count = count+1;
                            newSL(i).status = 1;
                            %use NaN to break SL
                            newSL(i).x = [newSL(i).x;SL(i).x(x_start:x_end,:);NaN,NaN];
                            x_start = 0;
                            x_end =0;
                        end
                    end
                    if x_start && (~x_end)
                        %end with a S inside box
                        count = count+1;
                        newSL(i).status = 1;
                        newSL(i).x = [newSL(i).x;SL(i).x(x_start:end,:)];
                    end
                end
            end
        end
    end
end