function Qnew = propagateQ(Q,Qnew,S,S_status,ex)
nq = size(Q.Q,1);
ns = size(S,2);
Q_ids = zeros(ns,1); %record qids contain S
status = zeros(nq,1); %status of each quad
for i = 1:ns
    Q_ids(i) = S.qid;
end
%propagate from each direction of S
for i = 1:ns
    for j = 1:3 %3 directions
        if S_status(i,j) == 0 %not been checked
            %initialize using S
            qid = S(i).qid;
            eid = S(i).eids(j);
            pid = S(i).pnew(j);
            enew = S(i).enew(j,:);
            break_status = 0;
            for loop =1:nq %at most nq loops
                %UPDATES:
                %qid: id in Q
                %eid: id in Q
                %pid: id in Qnew
                %enew: ids in Qnew
                %reach bdy or another S
                if break_status
                    S_status(i,j) = 1;
                    break
                end
                [qid1,~] = find_next_qid(Q,eid,qid); %next qid
                %quad been checked before
                if status(qid1)
                    %update twice
                    %{
                    %qid,eid,pid are ids in Qnew
                    [Qnew,qid,eid,pid,break_status,S_status] =...
                        updateQnew(Qnew,Q,qid,eid,pid,break_status,S_status,Q_ids,S,ex);
                    [Qnew,qid,eid,pid,break_status,S_status] =...
                        updateQnew(Qnew,Q,qid,eid,pid,break_status,S_status,Q_ids,S,ex);
                    %}
                else
                    [Qnew,qid,eid,pid,enew,break_status,S_status,S] =...
                        updateQ(Qnew,Q,qid,eid,pid,enew,break_status,S_status,Q_ids,S,ex);
                end
                if break_status
                    break
                end
                status(qid) = 1;
            end
        end
    end
end
end


function [Qnew,qid,eid,pid,enew,break_status,S_status,S] =...
    updateQ(Qnew,Q,qid0,eid0,pid0,enew0,break_status,S_status,Q_ids,S,ex)
[qid,order] = find_next_qid(Q,eid0,qid0);
%invalid quad should not be neighbours
%current quad will not be invalid
%transfinite interpolation
ids = zeros(4,1); %record pid
Q0 = zeros(1,4);
QE = zeros(1,4);
x = zeros(4,2);
nodes = cell(4,1);
for i = 1:4
    ids(i) = mod(order+i-2,4)+1; %local edge id in Q
    Q0(i) = Q.Q{qid}(ids(i)); %point ids in Q, reorder
    QE(i) = Q.QE{qid}(ids(i)); %edge ids in Q, reorder
    x(i,:) = Q.P(Q0(i),:);
    nodes(i) = Q.E_x(QE(i));
end
eid = QE(3); %find next eid
Gamma = cell(4,1); %store gamma functions
for j = 1:4 %each quad edge
    if j==1 || j==2
        if Q.E(QE(j),1) ~= Q0(j) %exchange
            nodes{j} = flipud(nodes{j});
        end
    end
    if j==3 || j==4
        if Q.E(QE(j),1) == Q0(j) %exchange
            nodes{j} = flipud(nodes{j});
        end
    end
    Gamma{j} = @(t) piecewise_linear(nodes{j},t);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %use exact bdy for bdy edges
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %2 bdy pts ~= bdy edge
    if QE(j)<=Q.nbpts
        %boundary edge
        for k = 1:ex.curve_num
            t1 = ex.curve(k).find_t(nodes{j}(1,:));
            t2 = ex.curve(k).find_t(nodes{j}(end,:));
            if t1>=0 && t2>=0
                break
            end
        end
        if t1<0||t2<0
            error('Error. Can not find t.')
        end
        if t2-t1>1+t1-t2 %cross 0/1
            Gamma{j} = @(t) ex.curve(k).position_at(mod(t1-(1+t1-t2)*t+1,1));
        elseif t1-t2>1+t2-t1
            Gamma{j} = @(t) ex.curve(k).position_at(mod(t1+(1+t2-t1)*t,1));
        else
            Gamma{j} = @(t) ex.curve(k).position_at(t1+(t2-t1)*t);
        end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end
xi = 0.5;
N = size(nodes{2},1); %point number on Gamma_2
E_x3 = zeros(N,2);
for i = 1:N
    eta = 1.0/(N-1)*(i-1);
    xy = (1-eta)*Gamma{1}(xi)+eta*Gamma{3}(xi)...
        +(1-xi)*Gamma{4}(eta)+xi*Gamma{2}(eta)...
        -x(1,:)*(1-xi)*(1-eta)-x(2,:)*xi*(1-eta)...
        -x(3,:)*eta*xi-x(4,:)*(1-xi)*eta;
    E_x3(i,:) = xy;
end
%{
%test plot of transfinite interpolation
figure
for i = 1:4
    plot(nodes{i}(:,1),nodes{i}(:,2),'b')
    hold on
end
for i = 1:size(E_x3,1)-1
    quiver(E_x3(i,1),E_x3(i,2),E_x3(i+1,1)-E_x3(i,1),E_x3(i+1,2)-E_x3(i,2))
    hold on
end
hold off
%}
%find new E_x
n_old = Q.nbpts;
n = Qnew.nbpts;
%update Q0,QE, ids in Qnew
for j = 1:4
    if Q0(j)>n_old
        Q0(j) = Q0(j)+n-n_old;
    end
    if QE(j)>n_old 
        QE(j) = QE(j)+n-n_old; 
    end
    %QE is not accurate, but we only need QE(2,3,4)
end
eidnew = QE(3); %edge id in Qnew
%contain boundary edge
if eid <= n_old
    break_status = 1;
    p = E_x3(end,:); %new pt
    P = Qnew.P;
    Qnew.P = [P(1:n,:);p;P(n+1:end,:)];
    pid = n+1;%new pt id
    Qnew.nbpts = Qnew.nbpts+1;
    %update S
    S.pnew = S.pnew+1;
    S.enew = S.enew+1;
    %not accurate for S has bdy edge
    %but we do not need that direction
    %update pids in E
    E = Qnew.E;
    for j = n+1:size(E,1)
        for k = 1:2
            if E(j,k)>n
                E(j,k) = E(j,k)+1;
            end
        end
    end
    %update Q0,QE
    for j = 1:4
        if Q0(j)>n
            Q0(j) = Q0(j)+1;
        end
        if QE(j)>n
            QE(j) = QE(j)+1;
        end
    end
    %update enew0
    for j = 1:3
        if enew0(j)>n
            enew0(j) = enew0(j)+1;
        end
    end
    %update pid0
    pid0 = pid0+1;
    %add E,E_x
    e1 = [Q0(4),pid];
    e2 = [pid,Q0(3)];
    e3 = [pid0,pid];
    N = 6;
    E_x1 = zeros(N,2); E_x2 = zeros(N,2);
    for j = 1:N
        t1 = 0.5/(N-1)*(j-1);
        t2 = t1+0.5;
        E_x1(j,:) = Gamma{3}(t1);
        E_x2(j,:) = Gamma{3}(t2);
    end
    Qnew.E = [E(1:eid-1,:);e1;E(eid+1:n,:);e2;E(n+1:end,:);e3];
    enew = [eid,n+1,size(Qnew.E,1)];
    E_x = cell(size(Qnew.E,1),1);
    E_x(1:n) = Qnew.E_x(1:n);
    E_x{eid} = E_x1;
    E_x{n+1} = E_x2;
    E_x(n+2:end-1) = Qnew.E_x(n+1:end);
    E_x{end} = E_x3;
    Qnew.E_x = E_x;
    %update Qnew.Q/QE
    for j = 1:size(Qnew.Q,1)
        for k = 1:size(Qnew.Q{j},2)
            if Qnew.Q{j}(k)>n
                Qnew.Q{j}(k) = Qnew.Q{j}(k)+1;
            end
            if Qnew.QE{j}(k)>n
                Qnew.QE{j}(k) = Qnew.QE{j}(k)+1;
            end
        end
    end
else %next Q contians S
    [qid1,~] = find_next_qid(Q,eid,qid);
    if ismember(qid1,Q_ids)
        break_status = 1;
        for i = 1:ns
            for j = 1:3
                if eid == S(i).eid(j)
                    sid = i;
                    id = j;
                    breaki = 1;
                    break
                end
            end
            if breaki
                break
            end
        end
        pid = S(sid).pnew(id);
        Qnew.E(end+1,:) = [pid0,pid];
        Qnew.E_x{end+1} = E_x3;
        S_status(sid,id) = 1;
    end
end
%not a bdy edge
if ~break_status
    p = E_x3(end,:);
    Qnew.P(end+1,:) = p;
    pid = size(Qnew.P,1);
    e1 = [Q0(4),pid];
    e2 = [pid,Q0(3)];
    e3 = [pid0,pid];
    N = 6;
    E_x1 = zeros(N,2); E_x2 = zeros(N,2);
    for j = 1:N
        t1 = 0.5/(N-1)*(j-1);
        t2 = t1+0.5;
        E_x1(j,:) = Gamma{3}(t1);
        E_x2(j,:) = Gamma{3}(t2);
    end
    Qnew.E(eidnew,:) = e1;
    Qnew.E(end+1,:) = e2;
    Qnew.E(end+1,:) = e3;
    ne = size(Qnew.E,1);
    enew = [eidnew,ne-1,ne];
    Qnew.E_x{eidnew} = E_x1;
    Qnew.E_x{end+1} = E_x2;
    Qnew.E_x{end+1} = E_x3;
end
%add Q/QE
Q1 = [pid0,pid,Q0(4),Q0(1)]; %ccw
Q2 = [pid0,Q0(2),Q0(3),pid];
QE1 = [enew(3),enew(1),QE(4),enew0(1)];
QE2 = [enew0(2),QE(2),enew(2),enew(3)];
Qnew.Q{qid} = Q1;
Qnew.Q{end+1} = Q2;
Qnew.QE{qid} = QE1;
Qnew.QE{end+1} = QE2;
end

function [qid,order] = find_next_qid(Q,eid,qid)
%INPUT:
%Q:old quad
%eid: previous eid
%qid: previous qid
%OUTPUT:
%qid: next qid
%order:eid order on next quad
for i = 1:size(Q.Q,1)
    breaki = 0;
    if i~=qid %not the previous quad
        for j = 1:size(Q.Q{i},2)
            if Q.QE{i}(j) == eid
                qid = i;
                order = j;
                breaki = 1;
                break
            end
        end
        if breaki
            break
        end
    end
end
end






