# README #

Running CrossField2D on Matlab R2016b or earlier version. Does not work on newest Matlab R2017.

### What is this repository for? ###

A 2D quad mesh generator designed for spectral methods.

### How do I get set up? ###
Open 'main.m' and click 'run' button on Matlab to run Indian Ocean test. (Test 39)


### Contribution guidelines ###

1.Input files:

1.1 Parametric function of curve: given parameter t, return point location [x,y].
Even distribution of parameter gives butter results.
Name input functions as: 
“curve10_1_p” (10 is example id, 1 is curve id, 1 is always the outer boundary curve)
“curve10_2_p” (10 is example id, 2 is curve id, curve id >1 are inner boundary curves)
Add more if you have more curves.

1.2 Function to find parameter: given point location [x,y], return parameter t if the point is on the curve, “-1” is not on the curve.
Name input functions as: 
“curve10_1_t” (10 is example id, 1 is curve id, 1 is always the outer boundary curve)
“curve10_2_t” (10 is example id, 2 is curve id, curve id >1 are inner boundary curves)
Add more if you have more curves.

1.3 Function to find y: given x, return y value, may have more than one y value, return Inf if x is out of curve range.
Name input functions as: 
“curve10_1_y” (10 is example id, 1 is curve id, 1 is always the outer boundary curve)
“curve10_2_y” (10 is example id, 2 is curve id, curve id >1 are inner boundary curves)
Add more if you have more curves.

The following functions are used for DistMesh. Do not need if use another trimesh generator.
1.4 fd: distance function
1.5 fh: edge length function

2.Auto curve file generators:
2.1 Curve file generator for piecewise linear curves:
poly_generator.m

2.2 Curve file generator for piecewise splines:
spline_generator.m

2.3 Curve file generator for ellipse:
ellipse_generator.m

3.The following examples are provided(numbers are example ids):
%1.Polygon
%2.Half circle
%3.Circle
%4.Rectangle with hole
%5:Four circles inside ellipse with various r
%6:Square inside square
(Example 5 shows auto adjust of invalid quad. But may need several ties to get a valid mesh.)

4. Output
Data: Quad Class, contains
P: point locations
E: edges, store pt ids
E_x: interpolation points on each edge 
Q: quad connectivity, use 4 pts, ccw
QE: quad connectivity, use 4 edges, ccw
C: pt connectivity, edges connect to a pt, store edge ids

Plot:
1. cross field and singularities
2. original cross field streamlines
3. quad mesh after subdivision
4. final mesh after smoothing


### Who do I talk to? ###

byuan@math.fsu.edu