function w = BaryWeights(N,x)
%calculate barycentric weights
%INPUT:
%N: num of points
%x: x values
%OUTPUT:
%w: barycentric weights

w = ones(N,1);
for j = 1:N
    for k = 1:N
        if k ~= j
            w(j) = w(j)*(x(j)-x(k));
        end
    end
end
for j = 1:N
    w(j) = 1/w(j);
end