function dP = LegendrePolyDeri(n,x)
%calculate derivative of legendre polynomial
%INPUT:
%n: order of polynomial
%x: evaluation value
%OUTPUT:
%dP: value of dP_n(x)

P = @LegendrePoly;
dP = n*(x*P(n,x)-P(n-1,x))/(x^2-1);
