%SL_boundary class to define bounday streamlines
classdef SL_boundary
    properties
        p_start %start point of SL, [x y]
        p_end %end point of SL, [x y]
        sign_change %curve changes sign on SL, true/false
        curve_id %on which cure
        
    end
    methods
    end
end