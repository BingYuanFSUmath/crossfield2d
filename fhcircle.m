function d  = fhcircle(p,x,y,r,d0)
%grid size function to control tri mesh density
%INPUT:
%p: evaluation point
%x,y :coordinates of center
%r: circle radius
%d0: grid size on radius
%OUTPUT:
%d: grid size at p

r0 = sqrt((p(:,1)-x).^2+(p(:,2)-y).^2);
d = r0./r.*d0;
