%find next triangle id
%using triangulation class
function eid = find_eid(vid1,vid2,status,TR)
eid = [];
ti = edgeAttachments(TR,vid1,vid2); %triangle attached to edge(v1,v2)
if size(ti{1},2) == 1 %only one element attached, boundary edge
    %return the previous eid
    eid = ti{1};
else
    for i = 1:2
        if ~status(ti{1}(i)) %ti: cell, stores edge id, not passes yet
            eid = ti{1}(i);
            break
        end
    end
end
