% function to find dtheta
function result = dtheta(N,N1,N2)
% N:Nodes, N1:id of Node1, N2:id of Node2
x1 = cos(N(N1).cross); %a cross vector of N1
y1 = sin(N(N1).cross);
%[x1 y1]--ok
theta = ones(4,1)*pi; %initialize dtheta
x = zeros(1,4);
y = zeros(1,4);
for i = 1:4 %four cross vectors of N2
    x(i) = cos(N(N2).cross+(i-1)*pi/4);
    y(i) = sin(N(N2).cross+(i-1)*pi/4);
    if dot([x1 y1],[x(i) y(i)])>0 %acute angle
        x2 = x(i); %copy coordinates
        y2 = y(i);
        theta(i) = atan2(x1*y2-y1*x2,x1*x2+y1*y2);
    end
end %find four dtheta
%theta--ok
[~,index] = min(abs(theta)); %find index of min dtheta
result = theta(index); %return the minimum theta    
end