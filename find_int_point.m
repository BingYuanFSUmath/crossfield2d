%find intersection point
function [x,y] = find_int_point(x0,y0,d,x1,y1,x2,y2)
error = 1e-14;
if abs(d(1))>error %d is not vertival, avoid cancellation in x
    k0 = d(2)/d(1); %slope of line passes x0,d
    if abs(x1-x2)<error %vertical line
        x = x1; %intersection point
        y = k0*(x-x0)+y0;
    else
        k1 = (y1-y2)/(x1-x2); %slope of line passes v1,v2
        if abs(k0-k1)<error %d parallel to one edge
            x = x0; %copy the original point
            y = y0;
        else
            x = (y1-y0-k1*x1+k0*x0)/(k0-k1); %coordinates of intersection point
            y = k0*(x-x0)+y0;
        end
    end
else %d is vertical, use x-x0=k(y-y0)
    k0 = d(1)/d(2); %inverse slope of line passes S, with direction d
    if abs(y1-y2)<error %horizontal line
        y = y1; %intersection point
        x = k0*(y-y0)+x0;
    else
        k1 = (x1-x2)/(y1-y2); %inverse slope of line passes v1,v2
        if abs(k0-k1)<error %d parallel to one edge
            x = x0; %copy the original point
            y = y0;
        else
            y = (x1-x0-k1*y1+k0*y0)/(k0-k1); %coordinates of intersection point
            x = k0*(y-y0)+x0;
        end
    end
end


