function [s_bdy,p,t,e,ex] = trimesh_generator(exampleID)
%examples of trimesh
%INPUT:
%exampleID: example id
%OUTPUT:
%s_bdy: boundary singularity coordinates, first pt duplicated
%p/t/e: points/triangle indices/boundary edges
%ex: example class with d0,fd,fh,box

%Polygons
%1.Square
if exampleID == 1
    curve_num = 1;
    s_bdy = [-1 -1;1 -1;1 1;-1 1;-1 -1];%copy the first point
    d0 = 0.1;
    fd = @(p) dpoly(p,s_bdy);
    fh = @(p) huniform(p);
    box = [-1,-1; 1,1];
    %poly_generator(exampleID,1,s_bdy);%generate polygon curve files
    %[p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    load example1.mat
    e = boundedges(p,t);
end
%2.Concave rectangles
if exampleID == 2
    curve_num = 1;
    s_bdy=[-0.5 0;0.5 0;0.5 1;1 1;1 -1;-1 -1;-1 1;-0.5 1;-0.5 0];
    d0 = 0.1;
    fd = @(p) dpoly(p,s_bdy);
    fh = @(p) huniform(p);
    box = [-1,-1; 1,1];
    %poly_generator(exampleID,1,s_bdy);
    %[p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    load example2.mat
    e = boundedges(p,t);
end
%3.Convex polygon
if exampleID == 3
    curve_num = 1;
    s_bdy=[-0.4 -0.5;0.4 -0.7;0.9 0.1;0.5 0.5;0.1 0.4;-0.4 -0.5];
    d0 = 0.1;
    fd = @(p) dpoly(p,s_bdy);
    fh = @(p) huniform(p);
    box = [-1,-1; 1,1];
    %poly_generator(exampleID,1,s_bdy);
    %[p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    load example3.mat
    e = boundedges(p,t);
end
%4.Concave polygon
if exampleID == 4
    curve_num = 1;
    s_bdy=[-0.4 -0.5;0.4 -0.2;0.4 -0.7;1.5 -0.4;0.9 0.1;1.6 0.8;0.5 0.5;0.2 1;0.1 0.4;-0.7 0.7;-0.4 -0.5];%bdy pts
    d0 = 0.1;
    fd = @(p) dpoly(p,s_bdy);
    fh = @(p) huniform(p);
    box = [-1,-1; 2,1.5];
    %poly_generator(exampleID,1,s_bdy);
    %[p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    load example4.mat
    e = boundedges(p,t);
end
%5.Jagged mesh
if exampleID == 5
    curve_num = 1;
    s_bdy=[0 -1;0.5 0;-0.1 1;-1 0;-0.2 -0.2;0 -1];
    d0 = 0.1;
    fd = @(p) dpoly(p,s_bdy);
    fh = @(p) huniform(p);
    box = [-1,-1; 1,1];
    %poly_generator(exampleID,1,s_bdy);
    %[p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    load example5.mat
    e = boundedges(p,t);
end
%6.Equilateral Triangle
if exampleID == 6
    curve_num = 1;
    d0 = sqrt(3)/20;
    s_bdy=[0 1;-sqrt(3)/2 -1/2;sqrt(3)/2 -1/2;0 1];
    fd = @(p) dpoly(p,s_bdy);
    fh = @(p) huniform(p);
    box = [-sqrt(3)/2,-1/2; sqrt(3)/2,1];
    %poly_generator(exampleID,1,s_bdy);
    %[p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    load example6.mat
    e = boundedges(p,t);
end
%7.Right angle
if exampleID == 7
    curve_num = 1;
    s_bdy=[0 1;-1 0;1 0;0 1];
    d0 = 0.1;
    fd = @(p) dpoly(p,s_bdy);
    fh = @(p) huniform(p);
    box = [-1,-1; 1,1];
    %poly_generator(exampleID,1,s_bdy);
    %[p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    load example7.mat
    e = boundedges(p,t);
end
%8.Obtuse angle
if exampleID == 8
    curve_num = 1;
    s_bdy=[0.5 0;1 0;-1 1;0.5 0];
    d0 = 0.05;
    fd = @(p) dpoly(p,s_bdy);
    fh = @(p) huniform(p);
    box = [-1,-1; 1,1];
    %poly_generator(exampleID,1,s_bdy);
    %[p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    load example8.mat
    e = boundedges(p,t);
end
%9.Half circle
if exampleID == 9
    curve_num = 1;
    s_bdy=[-1 0;1 0;-1 0];
    d0 = 0.1;
    fd=@(p) ddiff(dcircle(p,0,0,1),drectangle(p,-1,1,-1,0));
    fh = @huniform;
    box = [-1,0;1,1]; 
    %[p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    load example9.mat
    e=boundedges(p,t);
end
%10.Circle
if exampleID == 10
    curve_num = 1;
    s_bdy=[];
    d0 = 0.1;
    fd=@(p) sqrt(sum(p.^2,2))-1;%distance function of circle
    fh = @huniform;
    box = [-1,-1;1,1];
    %[p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    load example10.mat
    e=boundedges(p,t);
end
%11.Ellipse
if exampleID == 11
    curve_num = 1;
    s_bdy=[];
    d0 = 0.1;
    fd=@(p) p(:,1).^2/2^2+p(:,2).^2/1^2-1;
    fh = @huniform;
    box = [-2,-1;2,1];
    %[p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    load example11.mat
    e=boundedges(p,t);
end
%12.Rectangle with hole
if exampleID == 12
    curve_num = 2;
    s_bdy=[-1 -1;-1 1;1 1;1 -1;-1 -1];
    d0 = 0.1;
    fd=@(p) ddiff(drectangle(p,-1,1,-1,1),dcircle(p,0,0,0.5));
    fh = @huniform;
    box = [-1,-1;1,1];
    %[p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    load example12.mat
    e = boundedges(p,t);
end
%13.Square in square
if exampleID == 13
    curve_num = 2;
    d0 = 0.1;
    s_bdy=[-2 -2;2 -2;2 2;-2 2;-1 0;0 -1;1 0;0 1;-2 -2];
    pv = [-1 0;0 -1;1 0;0 1;-1 0];%vertices of inner square
    fd=@(p) ddiff(drectangle(p,-2,2,-2,2),dpoly(p,pv));
    fh = @(p) min(max(sqrt(sum(p.^2,2)),1),2);
    box = [-2,-2;2,2];
    %[p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    load example13.mat
    e=boundedges(p,t);
end
%14.Two circles inside rectangle
if exampleID == 14
    curve_num = 3;
    d0=0.05;
    s_bdy=[-1 -1;1 -1;1 1;-1 1;-1 -1];
    fd=@(p) ddiff(drectangle(p,-1,1,-1,1),...
        dunion(dcircle(p,-0.25,-0.25,0.2),dcircle(p,0.25,0.25,0.2)));
    fh = @huniform;
    box = [-1,-1;1,1];
    %poly_generator(exampleID,1,s_bdy);
    %ellipse_generator(exampleID,2,0.2,0.2,-0.25,-0.25);
    %ellipse_generator(exampleID,3,0.2,0.2,0.25,0.25);
    %[p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    load example14.mat
    e=boundedges(p,t);
end
%15.Three circles inside rectangle
if exampleID == 15
    curve_num = 4;
    d0 = 0.1;
    s_bdy=[-2 -1.5;2 -1.5;2 2;-2 2;-2 -1.5];
    fd=@(p) ddiff(drectangle(p,-2,2,-1.5,2),dunion...
        (dunion(dcircle(p,0,1,0.5),dcircle(p,1,0,0.5)),...
        dcircle(p,-1,0,0.5)));
    fh = @huniform;
    box = [-2,-1.5;2,2];
    %poly_generator(exampleID,1,s_bdy);
    %ellipse_generator(exampleID,2,0.5,0.5,0,1);
    %ellipse_generator(exampleID,3,0.5,0.5,1,0);
    %ellipse_generator(exampleID,4,0.5,0.5,-1,0);
    %[p,t]=distmesh2d_checked(fd,fh,d0,[-2,-1.5;2,2],s_bdy);
    load example15.mat
    e=boundedges(p,t);
end
%16.Airfoil(NACA0012)
if exampleID == 16
    curve_num = 2;
    d0 = 0.1;
    xmax = 1.008930411365;
    s_bdy=[xmax,0;-2,-4;6,-4;6,4;-2,4;xmax,0];
    hlead=0.01; htrail=0.04; hmax=d0; circx=2; circr=4;
    a=.6*[0.2969,-0.1260,-0.3516,0.2843,-0.1015];
    fd=@(p) ddiff(drectangle(p,-2,6,-4,4),(abs(p(:,2))-polyval([a(5:-1:2),0],p(:,1))).^2-a(1)^2*p(:,1));
    fh=@(p) min(min(hlead+0.3*dcircle(p,0,0,0),htrail+0.3*dcircle(p,1,0,0)),hmax);
    fixx=1-htrail*cumsum(1.3.^(0:4)');
    fixy=a(1)*sqrt(fixx)+polyval([a(5:-1:2),0],fixx);
    fix=[[circx+[-1,1,0,0]*circr; 0,0,circr*[-1,1]]'; 0,0; xmax,0; fixx,fixy; fixx,-fixy];
    fix=[fix;-2,-4;6,-4;6,4;-2,4];
    box=[circx-circr,-circr; circx+circr,circr];
    h0=min([hlead,htrail,hmax]);
    %[p,t]=distmesh2d_checked(fd,fh,h0,box,fix);
    load NACA0012.mat
    e=boundedges(p,t);
end
%17.example 6(half circle+rectangle)
if exampleID == 17
    curve_num = 1;
    d0 = 0.1;
    s_bdy=[1 0;-1 0;-0.5 0;-0.5 -1;-0.25 -1;0.25 -1;0.5 -1;0.5 0;1 0];
    fd=@(p) ddiff(dunion(ddiff(dcircle(p,0,0,1),drectangle(p,-1,1,-1,0)),drectangle(p,-0.5,0.5,-1,0)),dcircle(p,0,-1,0.25));
    fh = @huniform;
    box = [-1,-1;1,1];
    %[p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    load example17.mat
    e=boundedges(p,t);
end
%18.example 6(half circle+rectangle)
if exampleID == 18
    curve_num = 1;
    d0 = 0.1;
    s_bdy=[1 0;-1 0;-0.5 0;-0.5 -0.5;-0.25 -0.5;0.25 -0.5;0.5 -0.5;0.5 0;1 0];
    fd=@(p) ddiff(dunion(ddiff(dcircle(p,0,0,1),drectangle(p,-1,1,-1,0)),drectangle(p,-0.5,0.5,-0.5,0)),dcircle(p,0,-0.5,0.25));
    fh = @huniform;
    box = [-1,-0.5;1,1];
    %[p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    load example18.mat
    e=boundedges(p,t);
end
%19.Concave rectangles compare with quad tree
if exampleID == 19
    curve_num = 1;
    s_bdy=[2 0;5 0;5 2;0 2;0 1;2 1;2 0];
    d0 = 0.2;
    fd = @(p) dpoly(p,s_bdy);
    fh = @(p) huniform(p);
    box = [0,0; 5,2];
    %poly_generator(exampleID,1,s_bdy);
    %[p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    load example19.mat
    e = boundedges(p,t);
end
%20:Three circles inside rectangle with various r
if exampleID == 20
    curve_num = 4;
    d0 = 0.1;
    s_bdy=[-2 -1.5;2 -1.5;2 2;-2 2;-2 -1.5];
    fd=@(p) ddiff(drectangle(p,-2,2,-1.5,2),dunion...
        (dunion(dcircle(p,0,1,0.3),dcircle(p,1,0,0.4)),...
        dcircle(p,-1,0,0.5)));
    fh = @huniform;
    box = [-2,-1.5;2,2];
    %poly_generator(exampleID,1,s_bdy);
    %ellipse_generator(exampleID,2,0.3,0.3,0,1);
    %ellipse_generator(exampleID,3,0.4,0.4,1,0);
    %ellipse_generator(exampleID,4,0.5,0.5,-1,0);
    %[p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    load example20.mat
    e=boundedges(p,t);
end
%21:Three circles inside ellipse with various r
if exampleID == 21
    curve_num = 4;
    d0 = 0.1;
    s_bdy=[];
    fd=@(p) ddiff(p(:,1).^2/2^2+p(:,2).^2/1^2-1,...
        dunion(dunion(dcircle(p,0,0.5,0.3),...
        dcircle(p,1,0,0.4)),dcircle(p,-1,0,0.2)));
    fh = @huniform;
    box = [-2,-1;2,1];
    %ellipse_generator(exampleID,1,2,1,0,0);
    %ellipse_generator(exampleID,2,0.3,0.3,0,0.5);
    %ellipse_generator(exampleID,3,0.4,0.4,1,0);
    %ellipse_generator(exampleID,4,0.2,0.2,-1,0);
    %[p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    load example21.mat
    e=boundedges(p,t);
end
%22:Rectangle with half hole
if exampleID == 22
    curve_num = 1;
    d0 = 0.1;
    s_bdy=[-1 0;-1 1;1 1;1 0;0.5 0;-0.5 0;-1 0];
    fd=@(p) ddiff(drectangle(p,-1,1,0,1),dcircle(p,0,0,0.5));
    fh = @huniform;
    box = [-1,0;1,1];
    %[p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    load example22.mat
    e = boundedges(p,t);
end
%23:four circles inside ellipse with various r
if exampleID == 23
    curve_num = 5;
    d0 = 0.05;
    s_bdy=[];
    fd=@(p) ddiff(p(:,1).^2/2^2+p(:,2).^2/1^2-1,...
        dunion(dunion(dunion(dcircle(p,0,0.5,0.3),dcircle(p,1,0,0.2)),...
        dcircle(p,-1,0,0.5)),dcircle(p,0.5,-0.5,0.15)));
    fh=@(p) min(min(dcircle(p,0,0.5,0),min(dcircle(p,1,0,0),...
        min(dcircle(p,-1,0,0),dcircle(p,0.5,-0.5,0))))/0.15,0.5/0.15);
    box = [-2,-1;2,1];
    %ellipse_generator(exampleID,1,2,1,0,0);
    %ellipse_generator(exampleID,2,0.3,0.3,0,0.5);
    %ellipse_generator(exampleID,3,0.2,0.2,1,0);
    %ellipse_generator(exampleID,4,0.5,0.5,-1,0);
    %ellipse_generator(exampleID,5,0.15,0.15,0.5,-0.5);
    %[p,t]=distmesh2d(fd,fh,d0,box,s_bdy);
    load example23.mat
    e=boundedges(p,t);
end
%24:Square minus quater circle
if exampleID == 24
    curve_num = 1;
    d0 = 0.1;
    s_bdy=[-1 -1;1 -1;1 0;0 1;-1 1;-1 -1];
    fd=@(p) ddiff(drectangle(p,-1,1,-1,1),dcircle(p,1,1,1));
    fh = @huniform;
    box = [-1,-1;1,1];
    %[p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    load example24.mat
    e=boundedges(p,t);
end
%25:Four circles inside circle r=0.5
if exampleID == 25
    curve_num = 5;
    d0 = 0.1;
    s_bdy=[];
    r=0.5;
    fd=@(p) ddiff(dcircle(p,0,0,2),...
        dunion(dunion(dunion(dcircle(p,0,1,r),dcircle(p,1,0,r)),...
        dcircle(p,-1,0,r)),dcircle(p,0,-1,r)));
    fh = @huniform;
    box = [-2,-2;2,2];
    %ellipse_generator(exampleID,1,2,2,0,0);
    %ellipse_generator(exampleID,2,r,r,0,1);
    %ellipse_generator(exampleID,3,r,r,1,0);
    %ellipse_generator(exampleID,4,r,r,-1,0);
    %ellipse_generator(exampleID,5,r,r,0,-1);
    %[p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    load example25.mat
    e=boundedges(p,t);
end
%26:Four circles inside circle r=0.3
if exampleID == 26
    curve_num = 5;
    d0 = 0.1;
    s_bdy=[];
    r=0.3;
    fd=@(p) ddiff(dcircle(p,0,0,2),...
        dunion(dunion(dunion(dcircle(p,0,1,r),dcircle(p,1,0,r)),...
        dcircle(p,-1,0,r)),dcircle(p,0,-1,r)));
    fh = @huniform;
    box = [-2,-2;2,2];
    %ellipse_generator(exampleID,1,2,2,0,0);
    %ellipse_generator(exampleID,2,r,r,0,1);
    %ellipse_generator(exampleID,3,r,r,1,0);
    %ellipse_generator(exampleID,4,r,r,-1,0);
    %ellipse_generator(exampleID,5,r,r,0,-1);
    %[p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    load example26.mat
    e=boundedges(p,t);
end
%27:Example in Nicolas K
if exampleID == 27
    curve_num = 3;
    d0 = 0.1;
    s_bdy=[-2.5 0;2.5 0;2.5 3;-2.5 3;...
        -1.5 1;-0.5 1;-1.5 2;0.5 1;1.5 1;1.5 2;-2.5 0];
    fd=@(p) dunion(ddiff(drectangle(p,-2.5,2.5,0,3),...
        dunion(drectangle(p,-1.5,-0.5,1,2),drectangle(p,0.5,1.5,1,2))),...
        dunion(ddiff(drectangle(p,-1.5,-0.5,1,2),dcircle(p,-1.5,1,1)),...
        ddiff(drectangle(p,0.5,1.5,1,2),dcircle(p,1.5,1,1))));
    fh = @huniform;
    box = [-2.5,0;2.5,3];
    [p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    e=boundedges(p,t);
end
%28:Example in Fogg 12
if exampleID == 28
    d0 = 0.1;
    s_bdy=[-2 -3;2 -3;2 -1;2 3;-2 3;-2 1.5;-1 1;-2 1;0 1.5;0.5 1;1 1;...
        1 1.5;0.5 2;0 2;-1 0;-1.5 -1;-0.5 -2;-2 -3];
    pv1=[-2 1.5;-1 1;-2 1;-2 1.5];
    pv2=[0 1.5;0.5 1;1 1;1 1.5;0.5 2;0 2;0 1.5];
    pv3=[-1 0;-1.5 -1;-0.5 -2;-1 0];
    fd=@(p) ddiff(drectangle(p,-2,2,-3,3),...
        dunion(dunion(dunion(dpoly(p,pv1),dpoly(p,pv2)),dpoly(p,pv3)),...
        (p(:,1)+p(:,2)).^2*14/9+(p(:,1)-p(:,2)-4).^2*4/9-2));
    fh = @huniform;
    box = [-2,-3;2,3];
    [p,t]=distmesh2d(fd,fh,d0,box,s_bdy);
    e=boundedges(p,t);
end
%29:Example in Fogg 19, spiral
if exampleID == 29
    d0 = 0.1;
    n = 36; %number of partition points
    pv = spiral(n); %points on the spiral
    pv1=[-1.5 0;-1 0;pv;-1.5 0];%points of polygon
    s_bdy=[-1.5 0;-1 0;-1.5 0];
    fd=@(p) ddiff(dpoly(p,pv1),dcircle(p,0,0,0.5));
    fh = @huniform;
    box = [-1.5,-1.5;1.5,1.5];
    [p,t]=distmesh2d(fd,fh,d0,box,s_bdy);
    e=boundedges(p,t);
end
%30:Example in Bunin 6
if exampleID == 30
    curve_num = 2;
    d0 = 0.2;
    alpha = 0.35*pi; %rotation angle
    a = 1.13; %inner square half length
    pv1 = [-4 -4;4 -4;4 4;-4 4;-4 -4];
    z = exp((alpha+pi/4)*1i)*a*[1+1i;-1+1i;-1-1i;1-1i;1+1i];
    %complex coordinates of inner square
    pv2 = [real(z) imag(z)];
    s_bdy=[pv1(1:end-1,:);pv2(1:end-1,:);pv1(1,:)];
    fd=@(p) ddiff(dpoly(p,pv1),dpoly(p,pv2));
    fh = @huniform;
    box = [-4,-4;4,4];
    [p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    e=boundedges(p,t);
end
%31:Example in Bunin 6 (coerse grid size)
if exampleID == 31
    d0 = 0.1;
    r = 0.2; %ratio
    alpha = 0.35*pi; %rotation angle
    a = 1.13; %inner square half length
    pv1 = r*[-4 -4;4 -4;4 4;-4 4;-4 -4];
    z = exp((alpha+pi/4)*1i)*a*[1+1i;-1+1i;-1-1i;1-1i;1+1i];
    %complex coordinates of inner square
    pv2 = r*[real(z) imag(z)];
    s_bdy=[pv1(1:end-1,:);pv2(1:end-1,:);pv1(1,:)];
    fd=@(p) ddiff(dpoly(p,pv1),dpoly(p,pv2));
    fh = @huniform;
    box = r*[-4,-4;4,4];
    [p,t]=distmesh2d(fd,fh,d0,box,s_bdy);
    e=boundedges(p,t);
end
%32:Example in Bunin 7
if exampleID == 32
    curve_num =1;
    d0 = 0.1;
    s_bdy=[-1 0;0.5 sqrt(3)/2;0.5 -sqrt(3)/2;-1 0];
    fd=@(p) dunion(dunion(dcircle(p,-0.5,sqrt(3)/2,1),...
        dcircle(p,-0.5,-sqrt(3)/2,1)),dcircle(p,1,0,1));
    fh = @huniform;
    box = [-2,-2;2,2];
    [p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    e=boundedges(p,t);
end
%33:Example in Bunin 8
if exampleID == 33
    d0 = 0.1;
    s_bdy=[0 0;2 0;0 0];
    %f1=@(p) 3*sqrt(p(:,1).^2+p(:,2).^2)-cos(2*atan(p(:,2)./p(:,1)));
    %f2=@(p) sqrt(p(:,1).^2+p(:,2).^2)-cos(3*atan(p(:,2)./p(:,1)));
    a1 = -pi/30; %rotation angle
    a2 = pi;
    fd=@(p) ddiff(sqrt((cos(a1)*p(:,1)-sin(a1)*p(:,2)).^2 ...
        +(sin(a1)*p(:,1)+cos(a1)*p(:,2)).^2)...
        -3*cos(2*atan((sin(a1)*p(:,1)+cos(a1)*p(:,2))...
        ./(cos(a1)*p(:,1)-sin(a1)*p(:,2)))),...
        sqrt((cos(a2)*(p(:,1)-2)-sin(a2)*p(:,2)).^2 ...
        +(sin(a2)*(p(:,1)-2)+cos(a2)*p(:,2)).^2)...
        -cos(3/2*atan2(sin(a2)*(p(:,1)-2)+cos(a2)*p(:,2),...
        cos(a2)*(p(:,1)-2)-sin(a2)*p(:,2))));
    fh = @huniform;
    box = [0,-3;3,3];
    [p,t]=distmesh2d(fd,fh,d0,box,s_bdy);
    e=boundedges(p,t);
end
%34 Rectangle with hole with non-uniform h
if exampleID == 34
    d0 = 0.1;
    s_bdy=[-1 -1;-1 1;1 1;1 -1;-1 -1];
    fd=@(p) ddiff(drectangle(p,-1,1,-1,1),dcircle(p,0,0,0.5));
    fh=@(p) min(sqrt(sum(p.^2,2))+0.6,2);
    box = [-1,-1;1,1];
    [p,t]=distmesh2d(fd,fh,d0,box,s_bdy);
    e = boundedges(p,t);
end
%35 three elements airfoil 30P30N
if exampleID == 35
    curve_num = 4;
    d0 = 0.1;
    [s_bdy,p,t,e,fd,fh,box] = generate_30P30N;
end
%36 gingerman
if exampleID == 36
    curve_num = 7;
    d0 = 0.3;%both 0.4/0.5 works for d0=0.3
    s_bdy = [-7.44999981,37.8499985;7.44999981,37.8499985;-7.44999981,37.8499985];%bdy pts
    %load ginger.mat
    load ginger_spline.mat 
    %fd = @ginger_fd;
    fd = @(p) ddiff(dpoly(p,ginger{1}),dunion(dpoly(p,ginger{2}),dunion(dpoly(p,ginger{3}),dunion(dpoly(p,ginger{4}),dunion(dpoly(p,ginger{5}),dunion(dpoly(p,ginger{6}),dpoly(p,ginger{7})))))));
    %{
    fh = @(p) min(4,min(fhcircle(p,-3.1,47,2,1),min(fhcircle(p,3.1,47,2,1),...
        min(fhcircle(p,-3.8,41.6,1,1),min(fhcircle(p,0,33.2,2,1),...
        min(fhcircle(p,0,26.9,2,1),min(fhcircle(p,0,18.5,2,1),fhcircle(p,3.8,41.6,1,1))))))));
    %}
    fh = @huniform;
    %pfix = [ginger{1};ginger{2};ginger{3};ginger{4};ginger{5};ginger{6};ginger{7}];
    pfix = [s_bdy;-3.8900001,42.5200005;3.8900001,42.5200005];%pts on smile
    box = [-25,0; 25,60];
    %[p,t]=distmesh2d(fd,fh,d0,box,pfix);%points and triangles
    %load ginger_0.3_unif_scale1.mat %works
    load ginger_0.3_unif_scale2.mat %works, but SL clean up is bad
    %load ginger_0.2_nonuniform.mat %works, but SL clean up is bad
    %load ginger_0.5_unif_scale1.mat %d= 0.4/0.5 does not work
    e = boundedges(p,t);%generate boundary edges
end 
    %38.Rotated Half circle
if exampleID == 38
    curve_num = 1;
    s_bdy=[0,-1;0,1;0,-1];
    d0 = 0.1;
    fd=@(p) ddiff(dcircle(p,0,0,1),drectangle(p,0,1,-1,1));
    fh = @huniform;
    box = [-1,-1;0,1]; 
    [p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    %load example9.mat
    e=boundedges(p,t);
end

%39.Indian Ocean
if exampleID == 39
    %
    curve_num = 3;
    %spline
    d0 = 0.02; 
    %load ocean_spline.mat %d0 = 0.005;fh = @(p) -fd(p)+0.02;
    load ocean_coarse_poly.mat
    %load ocean_coarse_smooth_poly.mat
    fd = @(p) ddiff(dpoly(p,ocean{1}),dunion(dpoly(p,ocean{2}),dpoly(p,ocean{3})));
    fh = @(p) -fd(p)+0.2;
    box = [-4.5,-3.5;1,-1];
    %[p,t]=distmesh2d(fd,fh,d0,box,s_bdy);%points and triangles
    load ocean_coarse_poly_trimesh.mat
    %load ocean_coarse_smooth_trimesh.mat
    e = boundedges(p,t);%generate boundary edges
    %}
    %test on a small region
    %load detail.mat
end 

ex = Example(exampleID,curve_num);
ex.d0= d0;
ex.fd = fd;
ex.fh = fh;
ex.box = box;




