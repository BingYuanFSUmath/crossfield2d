function quad_plot(Quad)
%plot P and E
figure
nq = size(Quad(1).Q,1);
np = size(Quad(1).P,1);
ne = size(Quad,1);
for i = 1:ne
    %scatter(Quad(i).P(:,1),Quad(i).P(:,2))
    hold on
    %text(Quad(i).P(:,1)+0.005*cos((i/ne*2*pi)),...
    %    Quad(i).P(:,2)+0.005*sin((i/ne*2*pi)),...
    %    num2str([i*ones(np,1),(1:np)']))
    hold on
    for j = 1:nq
        x = [Quad(i).P(Quad(i).Q(j,:),1);Quad(i).P(Quad(i).Q(j,1),1)];
        y = [Quad(i).P(Quad(i).Q(j,:),2);Quad(i).P(Quad(i).Q(j,1),2)];
        plot(x,y,'b')
        hold on
    end
end
hold off
axis equal
end