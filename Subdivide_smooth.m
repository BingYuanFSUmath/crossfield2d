function Qnew = Subdivide_smooth(Q,n_poly,ex,s_bdy,fd)
%subdivide quad to smooth
%INPUT:
%Q: quad class
%n_poly: order of poly
%OUTPUT: Qnew

%subdivide each quad to NxN quads
Ne = size(Q.Q,1); %num of elements
N = n_poly+1; %num of pt on each direction to evaluate Jacobian
Qnew(Ne,1) = QuadClass;
for i = 1:Ne
    x = zeros(4,2); %four corner pts
    Qnew(i).P = zeros(N^2,2);
    Qnew(i).C = cell(N^2,1);
    Qnew(i).M = zeros(N^2,1); %flag of local pts on global boundary 
    for j = 1:4
        x(j,:) = Q.P(Q.Q(i,j),:);
    end
    Gamma = cell(4,1); %store gamma functions
    for j = 1:4 %each quad edge
        p0 = Q.H(1,:,Q.QE(i,j));
        p1 = Q.H(2,:,Q.QE(i,j));
        m0 = Q.H(3,:,Q.QE(i,j));
        m1 = Q.H(4,:,Q.QE(i,j));
        if j==1 || j==2
            if Q.E(Q.QE(i,j),1) ~= Q.Q(i,j) %exchange
                t = p0;
                p0 = p1;
                p1 = t;
                t = m0;
                m0 = -m1;
                m1 = -t;
            end
        end
        if j==3 || j==4
            if Q.E(Q.QE(i,j),1) == Q.Q(i,j) %exchange
                t = p0;
                p0 = p1;
                p1 = t;
                t = m0;
                m0 = -m1;
                m1 = -t;
            end
        end
        Gamma{j} = @(t)(2*t^3-3*t^2+1)*p0...
            +(t^3-2*t^2+t)*m0...
            +(-2*t^3+3*t^2)*p1...
            +(t^3-t^2)*m1;
    end
    for m = 1:N
        for n = 1:N
            xi = 1.0/(N-1)*(n-1);
            eta = 1.0/(N-1)*(m-1);
            xy = (1-eta)*Gamma{1}(xi)+eta*Gamma{3}(xi)...
                +(1-xi)*Gamma{4}(eta)+xi*Gamma{2}(eta)...
                -x(1,:)*(1-xi)*(1-eta)-x(2,:)*xi*(1-eta)...
                -x(3,:)*eta*xi-x(4,:)*(1-xi)*eta;
            p_id = n+(m-1)*N;
            Qnew(i).P(p_id,:) = xy;
        end
    end
    %mark all bdy pts
    %can not use elseif, may be more than one bdy edge
    if Q.Q(i,1)<=Q.nbpts && Q.Q(i,2)<=Q.nbpts
        %p1p2 is a bdy edge, local p(:,1) are on bdy
        jj = 1;
        for ii = 1:N
            Qnew(i).M(ii+(jj-1)*N) = 1;
        end
    end
    if Q.Q(i,2)<=Q.nbpts && Q.Q(i,3)<=Q.nbpts
        %p2p3 is a bdy edge, local p(N,:) are on bdy
        ii = N;
        for jj = 1:N
            Qnew(i).M(ii+(jj-1)*N) = 1;
        end
    end
    if Q.Q(i,3)<=Q.nbpts && Q.Q(i,4)<=Q.nbpts
        %p3p4 is a bdy edge, local p(:,N) are on bdy
        jj = N;
        for ii = 1:N
            Qnew(i).M(ii+(jj-1)*N) = 1;
        end
    end
    if Q.Q(i,4)<=Q.nbpts && Q.Q(i,1)<=Q.nbpts
        %p4p1 is a bdy edge, local p(1,:) are on bdy
        ii = 1;
        for jj = 1:N
            Qnew(i).M(ii+(jj-1)*N) = 1;
        end
    end
end
%construct local point connectivity
for m = 1:N
    for n = 1:N
        p_id = n+(m-1)*N;
        for ii = -1:1
            for jj = -1:1
                if (ii*jj==0) && (~(ii==0 && jj==0))
                    %i-1,j;i+1,j;i,j-1;i,j+1
                    nn = n+ii;
                    mm = m+jj;
                    if nn>0 && mm>0 && nn<=N && mm<=N
                        Qnew(1).C{p_id}(end+1,1) = nn+(mm-1)*N;
                    end
                end
            end
        end
    end
end
%local pt connectivity is same for each element
for i = 2:Ne
    Qnew(i).C = Qnew(1).C;
end
%test plot for local pt id->global pt id
%{
for i = 1:Ne
    figure
    for m = 1:N
        for n = 1:N
            pid = n+(m-1)*N;
            scatter(Qnew(i).P(pid,1),Qnew(i).P(pid,2),'b')
            hold on
            text(Qnew(i).P(pid,1)+0.01,Qnew(i).P(pid,2)+0.01,num2str(pid))
            hold on
            text(Qnew(i).P(pid,1)-0.01,Qnew(i).P(pid,2)-0.01,...
                num2str([n,m]),'Color','red')
            hold on
        end
    end
    hold off
    axis equal
end
%}
%test plot for Q.M, bdy pts
%{
figure
for i = 1:Ne
    scatter(Qnew(i).P(Qnew(i).M==1,1),Qnew(i).P(Qnew(i).M==1,2),'r')
    hold on
end
hold off
axis equal
%}

%find all neighbour pts
nei_ids = cell(Ne,N^2); %record all nei_pts, Nx2, [eid local_pid]
dup = cell(Ne,N^2); %record repeated pts
for eid = 1:Ne %each quad
    p = Qnew(eid).P;
    %loop through each point
    for j = 1:N
        for i = 1:N
            pid = i+(j-1)*N; %local pt id
            nei_ids{eid,pid} = [ones(size(Qnew(eid).C{pid}))*eid,Qnew(eid).C{pid}];
            dup{eid,pid} = [];
            %local neighbour pt ids, Nx2, [eid local_pid]
            if i==1||i==N||j==1||j==N %bdy pts
                %find other neighbour pts
                if i==1 && j==1%p1
                    e = Q.E(Q.C{Q.Q(eid,1)},:);
                elseif i==N && j==1%p2
                    e = Q.E(Q.C{Q.Q(eid,2)},:);
                elseif i==N && j==N%p3
                    e = Q.E(Q.C{Q.Q(eid,3)},:);
                elseif i==1 && j==N%p4
                    e = Q.E(Q.C{Q.Q(eid,4)},:);
                else %local bdy pts
                    if j == 1 %on p1p2
                        e = [Q.Q(eid,1),Q.Q(eid,2)];
                        %edge, store global pt ids
                    end
                    if i == N %on p2p3
                        e = [Q.Q(eid,2),Q.Q(eid,3)];
                    end
                    if j == N %on p3p4
                        e = [Q.Q(eid,3),Q.Q(eid,4)];
                    end
                    if i == 1 %on p4p1
                        e = [Q.Q(eid,4),Q.Q(eid,1)];
                    end
                end
                for ne = 1:size(e,1)
                    for id = 1:Ne
                        quadid = 0; %neighbour quad id
                        %if id ~= eid %exclude itself
                        if ~ismember(id,nei_ids{eid,pid}(:,1))
                            for k = 1:4
                                Qe = Q.E(Q.QE(id,k),:); %edge on quads
                                if ((e(ne,1)==Qe(1))&&(e(ne,2)==Qe(2)))||...
                                        ((e(ne,1)==Qe(2))&&(e(ne,2)==Qe(1)))
                                    %find local pt on which edge
                                    quadid = id; %quad id
                                    break %break k
                                end
                            end
                        end
                        if quadid>0 %exist neighbour quad
                            %find local pt id in quad(id)
                            %for a bdy pt: i=1,N j=1,N
                            if i~=1 && i~=N
                                ids = [i,1;N-i+1,1;i,N;N-i+1,N;...
                                    1,i;1,N-i+1;N,i;N,N-i+1];
                                %possible local pt id in quad(id)
                            elseif j~=1 && j~=N
                                ids = [j,1;N-j+1,1;j,N;N-j+1,N;...
                                    1,j;1,N-j+1;N,j;N,N-j+1];
                            else %corner pt
                                ids = [1,1;1,N;N,1;N,N];
                            end
                            dmin = Inf; %record min d
                            local_pid = 0;
                            for l = 1:size(ids,1)
                                temp_pid = ids(l,1)+(ids(l,2)-1)*N;
                                temp_p = Qnew(quadid).P(temp_pid,:);
                                d = norm(p(pid,:)-temp_p);
                                if d<dmin
                                    dmin = d;
                                    local_pid = temp_pid;
                                end
                            end
                            temp = ones(size(Qnew(quadid).C{local_pid}))*quadid;
                            nei_ids2 = [temp,Qnew(quadid).C{local_pid}];
                            %neighbour pt ids in quad(quadid)
                            nei_ids{eid,pid} = [nei_ids{eid,pid};nei_ids2];
                            dup{eid,pid} = [dup{eid,pid};[quadid,local_pid]];
                        end
                    end
                end
            end
            %until now find all neighbour pts ids
        end
    end
end

%Laplacian smoothing one time
%can add to 2-3 times
for eid = 1:Ne %each quad
    p = Qnew(eid).P;
    tol = 1e-5; %tolerance to check a bdy pt
    %loop through each point
    for j = 1:N
        for i = 1:N
            pid = i+(j-1)*N; %local pt id
            fix = 0; %flag of fixed pt
            pnew = p(pid,:);
            if Qnew(eid).M(pid) == 1 %global bdy pt
            %if abs(fd(p(pid,:)))<tol %global bdy pt
                for l = 1:size(s_bdy,1)
                    if norm(p(pid,:)-s_bdy(l,:))<tol %fixed pt
                        fix = 1;
                        break
                    end
                end
                if ~fix %not a fix pt
                    curve_id = 0;
                    for l = 1:ex.curve_num
                        t0 = ex.curve(l).find_t(p(pid,:));
                        if t0>=0
                            curve_id = l;
                            break
                        end
                    end
                    ptemp = zeros(2,2);
                    count = 0;
                    for l = 1:size(nei_ids{eid,pid},1)
                        nei_pts = Qnew(nei_ids{eid,pid}(l,1)).P(nei_ids{eid,pid}(l,2),:);
                        if Qnew(nei_ids{eid,pid}(l,1)).M(nei_ids{eid,pid}(l,2)) == 1 %global bdy pt
                        %if abs(fd(nei_pts))<tol
                            if ex.curve(curve_id).find_t(nei_pts)>=0
                                %should be on the same curve
                                %may connect to another bdy curve
                                count = count+1;
                                ptemp(count,:) = nei_pts;
                            end
                        end %should be only two neighbour pts
                    end
                    t1 = ex.curve(curve_id).find_t(ptemp(1,:));
                    t2 = ex.curve(curve_id).find_t(ptemp(2,:));
                    %[t0,t1,t2]
                    tmin = min(t1,t2);
                    tmax = max(t1,t2);
                    if tmin<t0 && t0<tmax
                        dt = tmin+0.5*(tmax-tmin)-t0;
                        for loop = 0:0
                            tnew = t0+0.5^loop*dt;
                            pnew = ex.curve(curve_id).position_at(tnew);
                            %Qnew(eid).P(pid,:) = pnew;
                            %beta_new = distortion_metric(Qnew.P,Qnew.Q(quad_ids,:));
                            %acc = acc_criteria(beta(quad_ids),beta_new);
                            %if acc %acceptable
                            %    break
                            %end
                        end
                        %if ~acc %not acceptable after 5 loops
                        %    pnew = p(pid);
                        %end
                    else
                        if t0<tmin %0<t0<tmin
                            for loop = 0:0
                                tnew = -t0+0.5^loop*(0.5*(1-tmax-tmin)+t0);
                                if tnew<0
                                    tnew = -tnew;
                                elseif tnew>0
                                    tnew = 1-tnew;
                                end
                                pnew = ex.curve(curve_id).position_at(tnew);
                                %Qnew(eid).P(pid,:) = pnew;
                                %beta_new = distortion_metric(Qnew.P,Qnew.Q(quad_ids,:));
                                %acc = acc_criteria(beta(quad_ids),beta_new);
                                %if acc %acceptable
                                %    break
                                %end
                            end
                            %if ~acc %not acceptable after 5 loops
                            %    pnew = p;
                            %end
                        else %tmax<t0<1
                            for loop = 0:0
                                tnew = 1-t0+0.5^loop*(0.5*(1-tmax-tmin)-1+t0);
                                if tnew<0
                                    tnew = -tnew;
                                elseif tnew>0
                                    tnew = 1-tnew;
                                end
                                pnew = ex.curve(curve_id).position_at(tnew);
                                %Qnew(eid).P(pid,:) = pnew;
                                %beta_new = distortion_metric(Qnew.P,Qnew.Q(quad_ids,:));
                                %acc = acc_criteria(beta(quad_ids),beta_new);
                                %if acc %acceptable
                                %    break
                                %end
                            end
                            %if ~acc %not acceptable after 5 loops
                            %    pnew = p;
                            %end
                        end
                    end
                    acc = 1;
                    if acc %moved
                        %find tangent vector at each p
                        t = tnew+0.01;
                        if t>1
                            t = t-1;
                        end
                        v1 = ex.curve(curve_id).position_at(t);
                        t = tnew-0.01;
                        if t<0
                            t = -t;
                        end
                        v2 = ex.curve(curve_id).position_at(t);
                        v=(v1-pnew)+(pnew-v2); %tangent vector
                        v = v/norm(v);
                        Q.M(id,:) = [1,v]; %only store m for bdy pts
                    end
                end
                %}
            else %not a bdy pt
                %remove duplicated neigbour pts
                %may not affect lot
                nei_pts = ones(size(nei_ids{eid,pid}));
                for l = 1:size(nei_ids{eid,pid},1)
                    nei_pts(l,:) = Qnew(nei_ids{eid,pid}(l,1))...
                        .P(nei_ids{eid,pid}(l,2),:);
                end
                nei_pts = unique_row(nei_pts,1e-10);
                pnew = sum(nei_pts)/size(nei_pts,1);
                %if connect to a bdy pt
                for l = 1:size(nei_ids{eid,pid},1)
                    %check connected pts
                    quadid = nei_ids{eid,pid}(l,1);
                    local_pid = nei_ids{eid,pid}(l,2);
                    if Qnew(quadid).M(local_pid) == 1
                        %connect to a bdy pt
                        p_nrom = norm(p(pid,:)-Qnew(quadid).P(local_pid,:));
                        pnew_nrom = norm(pnew-Qnew(quadid).P(local_pid,:));
                        if pnew_nrom<=p_nrom
                            pnew = p(pid,:);
                            %{
                        else
                            dp = pnew-p(pid,:);
                            for loop = 0:0
                                pnew = p(pid,:)+0.5^loop*dp;
                                %Qnew(eid).P(pid,:) = pnew;
                                %beta_new = distortion_metric(Qnew.P,Qnew.Q(quad_ids,:));
                                %acc = acc_criteria(beta(quad_ids),beta_new);
                                %if acc %acceptable
                                %    break
                                %end
                            end
                            %}
                        end
                    end
                end
                %
            end
            Qnew(eid).P(pid,:) = pnew;
            if size(dup{eid,pid},1)>0 %have duplicated pts
                %update all
                for id = 1:size(dup{eid,pid},1)
                    Qnew(dup{eid,pid}(id,1)).P(dup{eid,pid}(id,2),:)...
                        =pnew;
                end
            end
        end
    end
end

%test plot
figure
for i = 1:Ne
    for j = 1:N^2
        p0 = Qnew(i).P(j,:);
        p = Qnew(i).P(Qnew(i).C{j},:);
        p0 = ones(size(p,1),1)*p0;
        p = p-p0;
        quiver(p0(:,1),p0(:,2),p(:,1),p(:,2),'.','MarkerSize',0.1)
        hold on
    end
end
hold off
axis equal

%{
figure
for i = 1:Ne
    p = Qnew(i).P;
    scatter(p(:,1),p(:,2),'r')
    hold on
end
hold off
axis equal
%}
%{
figure
for eid = 1:Ne
    for pid = 1:N^2
        p0 = Qnew(eid).P(pid,:);
        scatter(p0(:,1),p0(:,2),'b')
        hold on
        if size(dup{eid,pid},1)>0 %have duplicated pts
                %update all
                p = size(dup{eid,pid});
                for id = 1:size(dup{eid,pid},1)
                    p(id,:) = Qnew(dup{eid,pid}(id,1)).P(dup{eid,pid}(id,2),:);
                end
        end
        scatter(p(:,1),p(:,2),'r')
        hold on
    end
end
hold off
axis equal
%}
