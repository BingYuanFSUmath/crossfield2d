function Qnew = Subdivide_smooth_new(Q,n_poly,ex,s_bdy)
%subdivide quad to smooth, use piecewise linear gamma
%INPUT:
%Q: quad class
%n_poly: order of poly
%OUTPUT: Qnew

Ne = size(Q.Q,1); %num of elements
N = n_poly+1; %num of pt on each direction to subdivide
%subdivide each quad to NxN quads
Qnew = subdivide_quad(Q,Ne,N,ex);

%find all neighbour pts/quads
[nei_ps,nei_qs,dup] = quad_neighbour(Qnew,Q,Ne,N);

%test plot before smoothing
%quad_plot(Qnew)

%smoothing
max_iter = 20; %maximum iteration number
move_tol = 1e-4; %minimum move tolerance: max_move_new/max_move_old
iter = 0;
max_move = 1;
max_loop = 10; %loop inside Laplacian smooth
status = ones(Ne,N^2); %move status of each local pt
all_pts = [reshape(ones((N-1)^2,1)*(1:Ne),[Ne*(N-1)^2,1]),...
    reshape((1:(N-1)^2)'*ones(1,Ne),[Ne*(N-1)^2,1])];
beta = zeros(Ne,(N-1)^2);
beta = distortion_metric_new(Qnew,all_pts,beta); %origional distortion metirc
acc_method = 2; %acceptance method 
while (iter<max_iter) && (max_move>move_tol)
    iter = iter+1;
    max_move = 0;
    for eid = 1:Ne %each quad
        %loop through each point
        for pid = 1:N^2 %local pt id
            if status(eid,pid) %movable
                p = Qnew(eid).P(pid,:); %origional p
                fix = 0; %flag of fixed pt
                if Qnew(eid).M(pid) == 1 %global bdy pt
                    for l = 1:size(s_bdy,1)
                        if norm(p-s_bdy(l,:))<1e-5 %fixed pt
                            fix = 1;
                            acc = 0; %not movable
                            status = update_status(status,dup{eid,pid});
                            break
                        end
                    end
                    if ~fix %not a fix pt
                        [t0,dt,curve_id,cross_flag] = ...
                            find_tnew(Qnew,ex,nei_ps,eid,pid,p);
                        %laplacian smoothing on boundary
                        quad_ids = nei_qs{eid,pid}; %neibour quad ids
                        for loop = 1:max_loop
                            tnew = t0+0.5^(loop-1)*dt;
                            if cross_flag
                                if tnew<0
                                    tnew = -tnew;
                                elseif tnew>0
                                    tnew = 1-tnew;
                                end
                            end
                            pnew = ex.curve(curve_id).position_at(tnew);
                            %test Q to update beta_new
                            Qtest = update_Q(Qnew,pnew,eid,pid,dup);
                            beta_new = distortion_metric_new(Qtest,quad_ids,beta);
                            acc = acceptance(beta,beta_new,quad_ids,acc_method);
                            if acc
                                break
                            end
                        end
                    end
                else %not a bdy pt
                    if iter < max_iter-2
                    %equal-space smoothing
                    pnew = equal_space(Qnew,nei_ps{eid,pid},nei_qs{eid,pid},eid,pid,dup);
                    else
                    %Laplacian smoothing
                    pnew = Laplacian_smooth(Qnew,nei_ps{eid,pid});
                    end
                    %weighted Laplacian smoothing
                    %pnew = wLaplacian_smooth(Qnew,nei_ps{eid,pid});
                    dp = pnew-p;
                    quad_ids = nei_qs{eid,pid}; %neibour quad ids
                    for loop = 1:max_loop
                        pnew = p+0.5^(loop-1)*dp;
                        %test Q to update beta_new
                        Qtest = update_Q(Qnew,pnew,eid,pid,dup);
                        beta_new = distortion_metric_new(Qtest,quad_ids,beta);
                        acc = acceptance(beta,beta_new,quad_ids,acc_method);
                        if acc
                            break
                        end
                    end
                end
                if acc %moved
                    Qnew = update_Q(Qnew,pnew,eid,pid,dup);
                    beta = beta_new;
                    d = norm(pnew-p);
                    if d>max_move
                        max_move = d;
                    end
                end
                %update status
                status = update_status(status,dup{eid,pid});
            end
        end
    end
end
iter
max_move
%Test plot for subdivided quad mesh
%quad_plot(Qnew)
%Test plot for final mesh
final_mesh_plot(Qnew,N,Ne,n_poly)



%functions used only in this file, cannot be reached by other files
function status = update_status(status,dup)
%update status of neighbour points
if size(dup,1)>0 %have duplicated pts
    for id = 1:size(dup,1)
        status(dup(id,1),dup(id,2))=0;
    end
end

function [t0,dt,curve_id,cross_flag] = find_tnew(Qnew,ex,nei_ps,eid,pid,p)
curve_id = 0;
for l = 1:ex.curve_num
    t0 = ex.curve(l).find_t(p);
    if t0>=0
        curve_id = l;
        break
    end
end
if t0<0
    error('Error. Can not find t.')
end
ptemp = zeros(2,2); %store two boundary neighbour pts
count = 0; %count bdy neighbour pts, count=2
for l = 1:size(nei_ps{eid,pid},1)
    nei_pts = Qnew(nei_ps{eid,pid}(l,1)).P(nei_ps{eid,pid}(l,2),:);
    if Qnew(nei_ps{eid,pid}(l,1)).M(nei_ps{eid,pid}(l,2)) == 1 %global bdy pt
        if ex.curve(curve_id).find_t(nei_pts)>=0
            %should be on the same curve
            %may connect to another bdy curve
            count = count+1;
            ptemp(count,:) = nei_pts;
        end
    end %should be only two neighbour pts
end
t1 = ex.curve(curve_id).find_t(ptemp(1,:));
t2 = ex.curve(curve_id).find_t(ptemp(2,:));
if t1<0||t2<0
    error('Error. Can not find t.')
end
tmin = min(t1,t2);
tmax = max(t1,t2);
cross_flag = 0; %cross flag
if tmin<t0 && t0<tmax
    dt = tmin+0.5*(tmax-tmin)-t0;
else %cross 0/1
    cross_flag = 1;
    if t0<tmin %0<t0<tmin
        dt = 0.5*(1-tmax-tmin)+t0;
        t0 = -t0;
    else %tmax<t0<1
        dt = 0.5*(1-tmax-tmin)-1+t0;
        t0 = 1-t0;
    end
end

%Laplacian smoothing
function pnew = Laplacian_smooth(Qnew,nei_ps)
%remove duplicated neigbour pts
nei_pts = ones(size(nei_ps));
for l = 1:size(nei_ps,1)
    nei_pts(l,:) = Qnew(nei_ps(l,1)).P(nei_ps(l,2),:);
end
nei_pts = unique_row(nei_pts,1e-10);
pnew = sum(nei_pts)/size(nei_pts,1);

%weighted Laplacian smoothing
function pnew = wLaplacian_smooth(Qnew,nei_ps)
%remove duplicated neigbour pts
nei_pts = ones(size(nei_ps));
for l = 1:size(nei_ps,1)
    nei_pts(l,:) = Qnew(nei_ps(l,1)).P(nei_ps(l,2),:);
end
nei_pts = unique_row(nei_pts,1e-10);
pnew = 0;
total_w = 0; %sum of weights
for l = 1:size(nei_pts,1)
    w = norm(nei_pts(l,:));
    total_w = total_w+w;
    pnew = pnew+w*nei_pts(l,:);
end
pnew = pnew/total_w;
                    
%Test plot for final mesh
function final_mesh_plot(Qnew,N,Ne,n_poly)
figure
side = zeros(1,N,4);
side(:,:,1) = 1:N;
side(:,:,2) = side(:,:,1)*N;
side(:,:,3) = side(:,:,2)-(N-1);
side(:,:,4) = side(:,:,1)+N*(N-1);
for i = 1:Ne
    for j = 1:4
        x = Qnew(i).P(side(:,:,j),:);
        t = 1:size(x,1);
        px = polyfit(t',x(:,1),n_poly);
        py = polyfit(t',x(:,2),n_poly);
        t = 1:0.1:size(x,1);
        x = polyval(px,t);
        y = polyval(py,t);
        plot(x,y,'b')
        hold on
    end
end
hold off
axis equal

%if connect to a bdy pt
%{
for l = 1:size(nei_ids{eid,pid},1)
    %check connected pts
    quadid = nei_ids{eid,pid}(l,1);
    local_pid = nei_ids{eid,pid}(l,2);
    if Qnew(quadid).M(local_pid) == 1
        %connect to a bdy pt
        p_nrom = norm(p(pid,:)-Qnew(quadid).P(local_pid,:));
        pnew_nrom = norm(pnew-Qnew(quadid).P(local_pid,:));
        if pnew_nrom<=p_nrom
            pnew = p(pid,:);
            %
        else
            dp = pnew-p(pid,:);
            for loop = 0:0
                pnew = p(pid,:)+0.5^loop*dp;
                %Qnew(eid).P(pid,:) = pnew;
                %beta_new = distortion_metric(Qnew.P,Qnew.Q(quad_ids,:));
                %acc = acc_criteria(beta(quad_ids),beta_new);
                %if acc %acceptable
                %    break
                %end
            end
            %
        end
    end
end
%}