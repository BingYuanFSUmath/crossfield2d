%2_2_2016,New findS function
function S = find_S(S,N,TR) %find singularity points
E = TR.ConnectivityList; %triangles, num_of_tri*3 matrix
Ec = zeros(size(E)); %Ec stores ccw triangle vertices
for i = 1:size(E,1) %loop through all triangles
    for j = 1:3
        x(j) = N(E(i,j)).x; %vertex coordinates, row arrays
        y(j) = N(E(i,j)).y;
    end
    [xc,yc] = poly2ccw(x,y);% counterclockwise(ccw), row arrays
    for j = 1:3
        index = strmatch([xc(j) yc(j)],[x' y']); %index of vertex in E
        Ec(i,j) = E(i,index); %sort vetices in triangle ccw
    end
    %It seems that E is already in ccw order
    %E(i,:) %test
    %Ec(i,:) %test
    %N.cross in [0,pi/2)
    %dtheta in [-pi/4,pi/4)
    dtheta12 = N(Ec(i,2)).cross-N(Ec(i,1)).cross; %find dtheta
    if dtheta12>pi/4
        dtheta12 = dtheta12-pi/2;
    elseif dtheta12<-pi/4
        dtheta12 = dtheta12+pi/2;
        %elseif abs(dtheta12)<1e-4
        %    dtheta12 = 0;%eliminate numerical error
    end
    dtheta23 = N(Ec(i,3)).cross-N(Ec(i,2)).cross; %find dtheta
    if dtheta23>pi/4
        dtheta23 = dtheta23-pi/2;
    elseif dtheta23<-pi/4
        dtheta23 = dtheta23+pi/2;
        %elseif abs(dtheta23)<1e-4
        %    dtheta23 = 0;
    end
    dtheta31 = N(Ec(i,1)).cross-N(Ec(i,3)).cross; %find dtheta
    if dtheta31>pi/4
        dtheta31 = dtheta31-pi/2;
    elseif dtheta31<-pi/4
        dtheta31 = dtheta31+pi/2;
        %elseif abs(dtheta31)<1e-4
        %    dtheta31 = 0;
    end
    %[dtheta12 dtheta23 dtheta31]--all k=-1
    if dtheta12<0 && dtheta23<0 && dtheta31<0 %find singulairty pts and k
        S(end+1).E_ID = i; %triangle ID
        S(end).k = 1; %k-value
        S(end).x = mean([N(E(i,1)).x N(E(i,2)).x N(E(i,3)).x]);
        S(end).y = mean([N(E(i,1)).y N(E(i,2)).y N(E(i,3)).y]);
        %centriod of triangle i, S position
    end
    if dtheta12>0 && dtheta23>0 && dtheta31>0
        S(end+1).E_ID = i; %triangle ID
        S(end).k = -1; %k-value
        S(end).x = mean([N(E(i,1)).x N(E(i,2)).x N(E(i,3)).x]);
        S(end).y = mean([N(E(i,1)).y N(E(i,2)).y N(E(i,3)).y]);
    end
end
end