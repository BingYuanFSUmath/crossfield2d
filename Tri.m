%Tri class defines the triangle mesh
classdef Tri
    properties
        p %pt locations->Points
        t %triangle nodes id->ConnectivityList
        e %boundary edges->Constriants
        c %constrian pts on the bdy
    end
    methods
        function T = Tri(p,t,e,c) %initalization
            if nargin ~= 0 %num of input arguments
            T.p = p;
            T.t = t;
            T.e = e;
            T.c = c;
            end
        end
    end
end