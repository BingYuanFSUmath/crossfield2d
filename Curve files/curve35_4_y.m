function y = curve35_4_y(x)
 y = []
; n = 2;
 for j = 1:n
 str = ['@curve35_4_',num2str(j),'_y'];
 f = str2func(str);
 yj = f(x);
 y = [y,yj];
 end