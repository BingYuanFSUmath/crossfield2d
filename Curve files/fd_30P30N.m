function dmin = fd_30P30N(p)
%signed distance function for 30P30N
%INPUT:
%p: point coordinates
%OUTPUT:
%dmin: distance from point to boundary, + outside - inside 0 on boundary

%distance to curve 1, outer boundary
d = zeros(4,1);
p1 = [-10,-10;10,-10;10,10;-10,10;-10,-10]; %outer boundary, ccw
d(1) = dpoly(p,p1);
%distance to curve 2,3,4, inner curves
load p_30P30N.mat
%p1,p2,p3 are clockwise, first pt duplicated
curve = cell(3,1);
curve{1} = p1;
curve{2} = p2;
curve{3} = p3;
for i = 1:3
    pi = [curve{i}(:,1)-p(1),curve{i}(:,2)-p(2)]; %pi-p
    dmin = Inf;
    id = 0;
    n = size(pi,1);
    for j = 1:n-1 %check each segment
        d1 = norm(pi(j,:)); %distance to end points
        d2 = norm(pi(j+1,:));
        d3 = p_l_distance(curve{i}(:,1),curve{i}(:,2),p); %distance to line
        di = min([d1,d2,d3]);
        if di<dmin
            dmin = di;
            id = j;
        end
    end
    str = ['@curve35_',num2str(i+1),'_t'];
    f = str2func(str);
    if id>1
        id1 = id-1;
    else
        id1 = n-1;
    end
    if id<n
        id2 = id+1;
    else
        id2 = 2;
    end
    t1 = f(curve{i}(id1,:));
    t2 = f(curve{i}(id2,:));
    str = ['@curve35_',num2str(i+1),'_p'];
    f = str2func(str);
    if t1<t2
        [~,d(i+1)] = fminbnd(@(t)norm(f(t)-p),t1,t2);
    else %cross 0/1
        [~,d1] = fminbnd(@(t)norm(f(t)-p),t1,1);
        [~,d2] = fminbnd(@(t)norm(f(t)-p),0,t2);
        d(i+1) = min(d1,d2);
    end
    %decide sign by tangentxp
    tang1 = curve{i}(id,:)-curve{i}(id1,:); %tangent vector 1
    tang2 = curve{i}(id2,:)-curve{i}(id,:); %tangent vector 2
    nor = [p(1)-curve{i}(id,1),p(2)-curve{i}(id,2)]; %normal vector
    cross1 = cross([tang1,0],[nor,0]);
    cross2 = cross([tang2,0],[nor,0]);
    d(i+1) = sign(max(cross1(3),cross2(3)))*d(i+1);
end
dmin = max(-d(2:end));
dmin = max(d(1),dmin);




