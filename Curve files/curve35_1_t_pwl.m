function T = curve35_1_t(p)
%curve function of example 35, three elements airfoil
%given point location on curve, return global parameter T
%INPUT:
%p: [x y], point coordinates on curve
%OUTPUT:
%T: parameter, [0,1], counterclockwise

T = -1; %NEED THIS TO FIND CURVE!
tol = 1e-5; %1e-10 will have problem, d may greater than 1e-10
load p_30P30N.mat %p1,p2,p3
s_bdy = p1;%bdy pts
n = size(s_bdy,1)-1; %num of sides
for i = 1:n
    p1 = s_bdy(i,:);
    p2 = s_bdy(i+1,:);
    d = p_l_distance(p1,p2,p);
    xmin = min(p1(1),p2(1));
    xmax = max(p1(1),p2(1));
    ymin = min(p1(2),p2(2));
    ymax = max(p1(2),p2(2));
    if abs(d)<tol && (xmin-tol<=p(1)&&p(1)<=xmax+tol)...
            && (ymin-tol<=p(2)&&p(2)<=ymax+tol) %on side i
        t0 = norm(p-p1)/norm(p2-p1); %t on side i
        T = (i-1+t0)/n;
        break
    end
end




