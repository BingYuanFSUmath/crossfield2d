%ex32generator
function p = curve32_1_p(t)
a = 1; b = 1;
cx1 = -0.5; cy1 = sqrt(3)/2; 
cx2 = -0.5; cy2 = -sqrt(3)/2;
cx3 = 1; cy3 = 0;
ta1 = 11*pi/6; ta2 = pi/2;
ta3 = 7*pi/6; dt = 5*pi/3;
if t<=1/3
    p = ellipsecurve_p(t,a,b,cx1,cy1,ta1,dt);
elseif t<=2/3
    p = ellipsecurve_p(t,a,b,cx2,cy2,ta2,dt);
else
    p = ellipsecurve_p(t,a,b,cx3,cy3,ta3,dt);
end



