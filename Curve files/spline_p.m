function s = spline_p(A,t)
%given parameter t and spline coefs A, evaluate x/y at t
%INPUT:
%A: space curve spline coefs, (N-1)x4
%t: parameter, 0<=t<=1
%OUTPUT:
%s = spline value at t

N = size(A,1)+1; %num of pts
x = t*(N-1)+1; %x = 1,2,...,N
for i = 1:N-1
    if i<=x && x<=i+1
        break %whcih s(i)
    end
end
dx = x-i;
s = 0;
for k = 4:-1:2
    s = (s+A(i,k))*dx;
end
s = s+A(i,1);
