function p = curve38_1_p(t)
%curve function of example 9, half circle
%INPUT:
%t: parameter, [0,1], counterclockwise
%OUTPUT:
%p: [x y], point coordinate on curve

r = 1;
if t<0.5 %left part
    theta = t*2*pi+pi/2;
    x = r*cos(theta);
    y = r*sin(theta);
else
    y = -r+(t-0.5)*4*r;
    x = 0;
end
p = [x y];


