function T = curve24_1_t(p)
%curve function of example 24, Square minus quater circle
%given point location on curve, return global parameter T
%INPUT:
%p: [x y], point coordinates on curve
%OUTPUT:
%T: parameter, [0,1], counterclockwise

x = p(1);
y = p(2);
tol = 1e-5; %y=0 has machine error
if abs(x-1)<tol && -1<=y && y<=0
    t = y+1;
    T = t/5;
elseif abs((x-1)^2+(y-1)^2-1)<1e-3 && x<1 && y<1
    theta = atan2(y-1,x-1);
    t = (-0.5*pi-theta)/(0.5*pi);
    T = 1/5+t/5;
elseif abs(y-1)<tol && -1<=x && x<=0
    t = -x;
    T = 2/5+t/5;
elseif abs(x+1)<tol && -1<=y && y<=1
    t = 1-(y+1)/2;
    T = 3/5+t/5;
elseif abs(y+1)<tol && -1<=x && x<=1
    t = (x+1)/2;
    T = 4/5+t/5;
else
    T = -1;
end
    



