function y = curve11_1_y(x)
%curve function of example 11, ellipse
%INPUT:
%x: x coordinate
%a/b: length of axis, x^2/a^2+y^2/b^2=1
%cx,cy: center coordinates
%OUTPUT:
%y: y coordinate, may be several, row vector

a = 2;
b = 1;
cx = 0;
cy = 0;
y = ellipsecurve_y(x,a,b,cx,cy);
