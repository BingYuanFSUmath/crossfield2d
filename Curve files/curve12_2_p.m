function p = curve12_2_p(t)
%curve function of example 12, rectangle with hole
%INPUT:
%t: parameter, [0,1], counterclockwise
%OUTPUT:
%p: [x y], point coordinate on curve

r = 0.5;
theta = t*2*pi;
x = r*cos(theta);
y = r*sin(theta);
p = [x y];

