function y = spline_y(x,Ax,Ay,p)
%given x and spline coefs, find y
%INPUT:
%x: x coordinate
%Ax,Ay: spline coefficients
%p: coordinates of spline sample points, Nx2
%OUTPUT:
%y: y coordinate, may be several, row vector

y = [];
xmin = min(p(:,1));
xmax = max(p(:,1));
N = size(p,1); %num of pts
if x < xmin || x > xmax %out of region
    y = 1e10; %helps to find intersection using root finding method
else 
    for i = 1:N-1
        xmax = max(p(i,1),p(i+1,1));
        xmin = min(p(i,1),p(i+1,1));
        if xmin<=x && x<=xmax
            %find t
            func = @(t) Ax(i,1)+Ax(i,2)*(t-i)+Ax(i,3)*(t-i)^2+...
                Ax(i,4)*(t-i)^3-x;
            t = bisection(func,[i,i+1],1e-10);
            t = (t-1)/(N-1);
            y(1,end+1) = spline_p(Ay,t);
        end
    end
end
y = unique_list(y,1e-10);