function p = curve0_1_2_p(t)
 Ax = [-0.892733785 -0.013409345 0 0];
 Ay = [-0.95594322 0.00117977199999997 0 0];
 x = spline_p(Ax,t);
 y = spline_p(Ay,t);
 p = [x,y];