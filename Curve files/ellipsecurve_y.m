function y = ellipsecurve_y(x,a,b,cx,cy,varargin)
%curve function of ellipse
%INPUT:
%x: x coordinate
%a/b: length of axis, x^2/a^2+y^2/b^2=1
%cx,cy: center coordinates
%ta,tb: angle range of ellipse
%OUTPUT:
%y: y coordinate, may be several, row vector

x = x-cx;
tol = 1e-10; 
if (isempty(varargin))
    ta = 0;
    dt = 2*pi;
else
    ta = varargin{1};
    dt = varargin{2};
end
xmin = -a+cx;
xmax = a+cx;
if x < xmin || x > xmax %out of region
    y = 1e10; %helps to find intersection using root finding method
elseif abs(x+a)<tol || abs(x-a)<tol
    y = cy;
else
    y = zeros(2,1);
    y(1) = sqrt(b^2*(1-x^2/a^2));
    y(2) = -y(1)+cy;
    y(1) = y(1)+cy;
    for i = 1:2
    if (atan2(y(i)/b,x/a)+pi-ta<0 || atan2(y(i)/b,x/a)+pi-ta>dt) 
        y(i) = 1e10;
    end
    end
end
