function p = curve35_3_p(T)
 n = 1;
 lt = [0;1];
 for j = 1:n
 if T>=lt(j) && T<=lt(j+1)
 t = (T-lt(j))/(lt(j+1)-lt(j));
 str = ['@curve35_3_',num2str(j),'_p'];
 f = str2func(str);
 p = f(t);
 break
 end
 end