function p = ellipsecurve_p(t,a,b,cx,cy,varargin)
%curve function of ellipse
%INPUT:
%t: parameter, [0,1], counterclockwise
%a/b: length of axis, x^2/a^2+y^2/b^2=1
%cx,cy: center coordinates
%ta: start angle [0,2pi]
%dt: angle range, ccw [0,2pi]
%OUTPUT:
%p: [x y], point coordinate on curve
if (isempty(varargin))
    theta = t*2*pi;
else
    ta = varargin{1};
    dt = varargin{2};
    theta = ta+t*dt;
end
x = a*cos(theta)+cx;
y = b*sin(theta)+cy;
p = [x y];
