function p = curve9_1_p(t)
%curve function of example 9, half circle
%INPUT:
%t: parameter, [0,1], counterclockwise
%OUTPUT:
%p: [x y], point coordinate on curve

r = 1;
if t<0.5 %upper part
    theta = t*2*pi;
    x = r*cos(theta);
    y = r*sin(theta);
else
    x = -r+(t-0.5)*4*r;
    y = 0;
end
p = [x y];


