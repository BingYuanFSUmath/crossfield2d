function y = curve10_1_y(x)
%curve function of example 10, circle
%INPUT:
%x: x coordinate
%OUTPUT:
%y: y coordinate, may be several, row vector

xmin = -1;
xmax = 1;
tol = 1e-10;
if x < xmin || x > xmax %out of region
    y = 1e10; %helps to find intersection using root finding method
elseif abs(x-xmin)<tol || abs(x-xmax)<tol
    y = 0;
else
    y = [0 0];
    y(1) = sqrt(1-x^2);
    y(2) = -sqrt(1-x^2);
end
    



