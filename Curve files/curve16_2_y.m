function y = curve16_2_y(x)
%curve function of example 16
%outer circle with center (2,0) and radius 4,
%INPUT:
%x: x coordinate, [-2,6]
%OUTPUT:
%y: y coordinate, may be several

%{
xmin = -2;
xmax = 6;
f = @(x) sqrt(16-(x-2)^2);

if x<xmin || x>xmax %out of region
    y = 1e10; %helps to find intersection using root finding method
else
    y = f(x);
end

if x>xmin && x<xmax %out of region
    
    y(end+1) = -f(x);
end
%}
y = [];
tol = 1e-10; 
s_bdy=[-10,-10;10,-10;10,10;-10,10;-10,-10];%bdy pts
xmin = min(s_bdy(:,1));
xmax = max(s_bdy(:,1));
n = size(s_bdy,1)-1; %num of sides
if x < xmin || x > xmax %out of region
    y = 1e10; %helps to find intersection using root finding method
else 
    for i = 1:n
        x1 = s_bdy(i,1);
        y1 = s_bdy(i,2);
        x2 = s_bdy(i+1,1);
        y2 = s_bdy(i+1,2);
        xmin = min(x1,x2);
        xmax = max(x1,x2);
        if xmin<=x && x<=xmax
            if abs(x1-x2)<tol %vertical line
                y(end+1) = y1; %NOT ACCURATE
            else
                y(end+1) = (y2-y1)*(x-x1)/(x2-x1)+y1;
            end
        end
    end
end
y = unique(y);



