function read_30P30N_spline
%read 30P30N points for spline curve
%total 3 inner curves
load p_30P30N.mat
%p1,p2,p3 are clockwise, first pt duplicated
curve = cell(3,1);
curve{1} = p1;
curve{2} = p2;
curve{3} = p3;
for i = 2:4
    spline_generator(35,i,curve{i-1})
end
