function y = curve13_1_y(x)
%curve function of example 13, square in square
%INPUT:
%x: x coordinate
%OUTPUT:
%y: y coordinate, may be several, row vector

s_bdy=[-2 -2;2 -2;2 2;-2 2;-2 -2];
y = polycurve_y(x,s_bdy);
    



