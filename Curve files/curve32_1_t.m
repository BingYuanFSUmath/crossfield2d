function t = curve32_1_t(p)
a = 1; b = 1;
cx1 = -0.5; cy1 = sqrt(3)/2; 
cx2 = -0.5; cy2 = -sqrt(3)/2;
cx3 = 1; cy3 = 0;
ta1 = 11*pi/6; ta2 = pi/2;
ta3 = 7*pi/6; dt = 5*pi/3;
t1 = ellipsecurve_t(p,a,b,cx1,cy1,ta1,dt);
t2 = ellipsecurve_t(p,a,b,cx2,cy2,ta2,dt);
t3 = ellipsecurve_t(p,a,b,cx3,cy3,ta3,dt);
t = max([t1,t2,t3]);