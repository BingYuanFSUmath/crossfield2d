function read_ginger_spline
%read ginger points for spline curve
%curve 1-2,3,...,8,total 7 curves
fileID = fopen('ginger_spline.txt','r');
N = 8; %number of curves
curve = cell(N,1);

for id = 1:N
    formatSpec = '%d %d';
    psize = [1,2]; %output size, fill in column
    pt_num = fscanf(fileID,formatSpec,psize); %read in row
    formatSpec = '%f %f';
    psize = [2 pt_num(2)];
    A = fscanf(fileID,formatSpec,psize);
    curve{id} = A';
    plot(A(1,:),A(2,:),'b')
    hold on
end
hold off
fclose(fileID);

%curve 1_1,1_2 compose curve 1
for i = 1:2
    pt = curve{i};
    %end slope end conditions
    Ax = spline_coef(pt(:,1),2);
    Ay = spline_coef(pt(:,2),2);
    pt = mat2str(pt);
    Ax = mat2str(Ax);
    Ay = mat2str(Ay);
    %p file
    name = ['curve36_1_',num2str(i),'_p'];
    fileID = fopen([name,'.m'],'w');
    content = ['function p = ',name,'(t)'...
        '; Ax = ',Ax,'; Ay = ',Ay,'; x = spline_p(Ax,t);',...
        'y = spline_p(Ay,t); p = [x,y];'];
    fprintf(fileID, '%s', content);
    fclose(fileID);
    %t file
    name = ['curve36_1_',num2str(i),'_t'];
    fileID = fopen([name,'.m'],'w');
    content = ['function T = ',name,'(p); pt = ',pt,...
        '; Ax = ',Ax,'; Ay = ',Ay,'; T = spline_t(p,Ax,Ay,pt);'];
    fprintf(fileID, '%s', content);
    fclose(fileID);
    %y file
    name = ['curve36_1_',num2str(i),'_y'];
    fileID = fopen([name,'.m'],'w');
    content = ['function y = ',name,'(x); pt = ',pt,...
        '; Ax = ',Ax,'; Ay = ',Ay,'; y = spline_y(x,Ax,Ay,pt);'];
    fprintf(fileID, '%s', content);
    fclose(fileID);
end
 
for i = 3:N
    pt = curve{i};
    %periodic end conditions
    Ax = spline_coef(pt(:,1),1);
    Ay = spline_coef(pt(:,2),1);
    pt = mat2str(pt);
    Ax = mat2str(Ax);
    Ay = mat2str(Ay);
    %p file
    name = ['curve36_',num2str(i-1),'_p'];
    fileID = fopen([name,'.m'],'w');
    content = ['function p = ',name,'(t)'...
        '; Ax = ',Ax,'; Ay = ',Ay,'; x = spline_p(Ax,t);',...
        'y = spline_p(Ay,t); p = [x,y];'];
    fprintf(fileID, '%s', content);
    fclose(fileID);
    %t file
    name = ['curve36_',num2str(i-1),'_t'];
    fileID = fopen([name,'.m'],'w');
    content = ['function T = ',name,'(p); pt = ',pt,...
        '; Ax = ',Ax,'; Ay = ',Ay,'; T = spline_t(p,Ax,Ay,pt);'];
    fprintf(fileID, '%s', content);
    fclose(fileID);
    %y file
    name = ['curve36_',num2str(i-1),'_y'];
    fileID = fopen([name,'.m'],'w');
    content = ['function y = ',name,'(x); pt = ',pt,...
        '; Ax = ',Ax,'; Ay = ',Ay,'; y = spline_y(x,Ax,Ay,pt);'];
    fprintf(fileID, '%s', content);
    fclose(fileID);
end

figure
for i = 1:7
    t = (0:0.001:1)';
    p = zeros(size(t,1),2);
    name = ['curve36_',num2str(i),'_p'];
    f = str2func(name);
    for j = 1:size(t,1)
        p(j,:) = f(t(j));
    end
    plot(p(:,1),p(:,2),'r')
    hold on
end
hold off

ginger = cell(N-1,1);
scale = 2; %inserted sample pts
np1 = size(curve{1},1)-1;
np2 = size(curve{2},1)-1;
np = np1+np2; %num of sample polygon pts
np1 = np1*scale;
np2 = np2*scale;
np = np*scale;
p = zeros(np,2);
%make sure end points of head and body are in p
for i = 1:np1
    t = (i-1)/np1;
    p(i,:) = curve36_1_1_p(t);
end
for i = 1:np2
    t = (i-1)/np2;
    p(np1+i,:) = curve36_1_2_p(t);
end
ginger{1} = [p;p(1,:)];
for i = 3:N
    np = size(curve{i},1)-1; %num of sample polygon pts
    np = np*scale;
    p = zeros(np,2);
    name = ['curve36_',num2str(i-1),'_p'];
    f = str2func(name);
    for j = 1:np
        t = (j-1)/np;
        p(j,:) = f(t);
    end
    ginger{i-1} = [p;p(1,:)];
end
save ginger_spline.mat ginger
