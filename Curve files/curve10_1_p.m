function p = curve10_1_p(t)
%curve function of example 10, circle
%INPUT:
%t: parameter, [0,1], counterclockwise
%OUTPUT:
%p: [x y], point coordinate on curve

r = 1;
theta = t*2*pi;
x = r*cos(theta);
y = r*sin(theta);
p = [x y];


