function A = spline_coef(x,para)
%find piecewise cubic spline coefficients for space curve: 
%t = 1,2,...,size(x) x = x1,x2,...xn
%INPUT:
%x: x or y values, column vector
%for a closed curve, x1=xn, repeat first pt
%para: end condition parameter
%para = 0: natural, s"(a) = s"(b) = 0
%para = 1: periodic, s'(a) = s'(b), s"(a) = s"(b)
%para = 2: end slope, use start/end points to approx s'(a), s'(b)
%OUTPUT:
%A: coefficients of s_i(x)=sum A(i,k)(x-xi)^(k-1), k = 1-4

N = size(x,1); %num of pts
m = zeros(N,1);
d = zeros(N,1); %r.h.s.
if para == 0 %natural
    a = 4*ones(N-2,1); %diagonal
    b = ones(N-2,1); %d-1
    c = ones(N-2,1); %d+1
    m(1) = 0;
    m(N) = 0;
    for i = 2:N-1
        d(i) = 6*(x(i+1)-2*x(i)+x(i-1));
    end
    m(2:N-1) = tridiag(a,b,c,d(2:N-1));
elseif para == 1 %periodic
    n = N-1; %m(1)~m(n+1)
    p = zeros(n-1,1); q = p; u = p; s = p; t = p; v = p; 
    for i = 2:n
        d(i) = x(i+1)-2*x(i)+x(i-1);
    end
    d(n+1) = x(2)-x(1)-x(n+1)+x(n);
    p(1) = 4; q(1) = 0.25; u(1) = 6*d(2)/4;
    for i = 2:n-1
        p(i) = 4-q(i-1);
        q(i) = 1/p(i);
        u(i) = (6*d(i+1)-u(i-1))/p(i);
    end
    s(1) = -1/p(1);
    for i = 2:n-1
        s(i) = -s(i-1)/p(i);
    end
    t(n) = 1; v(n) = 0;
    for i = n-1:-1:1
        t(i) = -q(i)*t(i+1)+s(i);
        v(i) = -q(i)*v(i+1)+u(i);
    end
    m(n+1) = (6*d(n+1)-v(1)-u(n-1))/(t(1)+s(n-1)-q(n-1)+4);
    for i = 2:n-1
        m(i) = t(i-1)*m(n+1)+v(i-1);
    end
    m(n) = u(n-1)+(s(n-1)-q(n-1))*m(n+1);
    m(1) = m(n+1);
else %para=2, end slope
    dsa = x(2)-x(1); %s'(a)
    dsb = x(end)-x(end-1); %s'(b)
    a = 4*ones(N,1); %diagonal
    a(1) = 2;
    a(N) = 2;
    b = ones(N,1); %d-1
    c = ones(N,1); %d+1
    d(1) = x(2)-x(1)-dsa;
    d(N) = dsb-(x(N)-x(N-1));
    for i = 2:N-1
        d(i) = x(i+1)-2*x(i)+x(i-1);
    end
    m(1:N) = tridiag(a,b,c,6*d);
end
A = zeros(N-1,4);
for i = 1:N-1
    A(i,1) = x(i);
    A(i,2) = x(i+1)-x(i)-(m(i+1)+2*m(i))/6;
    A(i,3) = 0.5*m(i);
    A(i,4) = (m(i+1)-m(i))/6;
end
        
        
    

