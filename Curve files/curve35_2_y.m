function y = curve35_2_y(x)
 y = []
; n = 3;
 for j = 1:n
 str = ['@curve35_2_',num2str(j),'_y'];
 f = str2func(str);
 yj = f(x);
 y = [y,yj];
 end