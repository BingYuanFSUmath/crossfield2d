function p = curve24_1_p(t)
%curve function of example 24, Square minus quater circle
%INPUT:
%t: parameter, [0,1], counterclockwise
%OUTPUT:
%p: [x y], point coordinate on curve

if t<=1/5
    x = 1;
    y = -1+5*t;
elseif t<=2/5
    theta = -5*(t-1/5)*0.5*pi-0.5*pi;
    x = cos(theta)+1;
    y = sin(theta)+1;
elseif t<=3/5
    x = -5*(t-2/5);
    y = 1;
elseif t<=4/5
    x = -1;
    y = 1-5*(t-3/5)*2;
else 
    x = -1+5*(t-4/5)*2;
    y = -1;
end
p = [x y];


