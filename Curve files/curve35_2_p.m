function p = curve35_2_p(T)
 n = 3;
 lt = [0;0.0303747060753143;0.920512235000751;1];
 for j = 1:n
 if T>=lt(j) && T<=lt(j+1)
 t = (T-lt(j))/(lt(j+1)-lt(j));
 str = ['@curve35_2_',num2str(j),'_p'];
 f = str2func(str);
 p = f(t);
 break
 end
 end