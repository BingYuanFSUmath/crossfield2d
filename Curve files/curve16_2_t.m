function T = curve16_2_t(p)
%curve function of example 16
%outer circle with center (2,0) and radius 4,
%INPUT:
%p: [x y], point coordinates
%OUTPUT:
%t: global parameter

%{
tol = 1e-4; %based on trimesh generator
r0 = 4; %radius of circle
c = [2 0]; %circle center
x = p(1);
y = p(2);
theta = mod(atan2(y-c(2),x-c(1)),2*pi); %[0,2pi]
r = norm(p-c);
xmin = -2;
xmax = 6;
if x < xmin || x > xmax || abs(r-r0)>tol %out of region
    t = -1;
else
    t = theta/(2*pi);
end
%}
T = -1; %NEED THIS TO FIND CURVE!
tol = 1e-5; %1e-10 will have problem, d may greater than 1e-10
s_bdy = [-2,-4;6,-4;6,4;-2,4;-2,-4];
n = size(s_bdy,1)-1; %num of sides
for i = 1:n
    p1 = s_bdy(i,:);
    p2 = s_bdy(i+1,:);
    d = p_l_distance(p1,p2,p);
    xmin = min(p1(1),p2(1));
    xmax = max(p1(1),p2(1));
    ymin = min(p1(2),p2(2));
    ymax = max(p1(2),p2(2));
    if abs(d)<tol && (xmin-tol<=p(1)&&p(1)<=xmax+tol)...
            && (ymin-tol<=p(2)&&p(2)<=ymax+tol); %on side i
        t0 = norm(p-p1)/norm(p2-p1); %t on side i
        T = (i-1+t0)/n;
        break
    end
end

