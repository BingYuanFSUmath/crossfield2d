function T = curve35_4_t(p)
 T = -1;
 n = 2;
 lt = [0;0.382713898076173;1];
 for j = 1:n
 str = ['@curve35_4_',num2str(j),'_t'];
 f = str2func(str);
 t = f(p);
 if t >= 0
 T = t*(lt(j+1)-lt(j))+lt(j);
 break
 end
 end