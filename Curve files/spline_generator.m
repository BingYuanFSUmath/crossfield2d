function sp = spline_generator(id,i,p,theta_sp)
%generate spline curve files
%INPUT:
%id: example id
%i: curve number
%p: points on curve
%theta_sp: split angle criteria
%OUTPUT:
%sp: split points, used to be 5*pi/6
%write curve function files: p/t/y

curve = curve_split(p,theta_sp); %split given points
subcurve = size(curve,2); %number of subcurves
sp = zeros(subcurve,2);
%decide end condition
condi = 2; %end slope
if subcurve == 1
    v1 = curve{1}(2,:)-curve{1}(1,:);
    v2 = curve{1}(end-1,:)-curve{1}(end,:);
    theta = acos(dot(v1,v2)/(norm(v1)*norm(v2)));
    if theta >= theta_sp %smooth curve
        condi = 1; %periodic
        sp = [];
    end
end
%generate subcurve files
lt = zeros(subcurve+1,1); %reord subcurve length ratio
l = 0; %reord subcurve length
for j = 1:subcurve
    if size(sp,1)>0
        sp(j,:) = curve{j}(1,:); 
    end
    pt = curve{j};
    Ax = spline_coef(pt(:,1),condi);
    Ay = spline_coef(pt(:,2),condi);
    l = l+curvelength(pt);
    lt(j+1) = l;
    pt = mat2str(pt);
    Ax = mat2str(Ax);
    Ay = mat2str(Ay);
    %p file
    %t can be modified to represent curve length ratio
    name = ['curve',num2str(id),'_',num2str(i),'_',num2str(j),'_p'];
    fileID = fopen([name,'.m'],'w');
    content = ['function p = ',name,'(t)'...
        '\n Ax = ',Ax,';\n Ay = ',Ay,';\n x = spline_p(Ax,t);\n',...
        ' y = spline_p(Ay,t);\n p = [x,y];'];
    fprintf(fileID,content);
    fclose(fileID);
    %t file
    name = ['curve',num2str(id),'_',num2str(i),'_',num2str(j),'_t'];
    fileID = fopen([name,'.m'],'w');
    content = ['function T = ',name,'(p)\n pt = ',pt,...
        ';\n Ax = ',Ax,';\n Ay = ',Ay,';\n T = spline_t(p,Ax,Ay,pt);'];
    fprintf(fileID,content);
    fclose(fileID);
    %y file
    name = ['curve',num2str(id),'_',num2str(i),'_',num2str(j),'_y'];
    fileID = fopen([name,'.m'],'w');
    content = ['function y = ',name,'(x)\n pt = ',pt,...
        ';\n Ax = ',Ax,';\n Ay = ',Ay,';\n y = spline_y(x,Ax,Ay,pt);'];
    fprintf(fileID,content);
    fclose(fileID);
end
%seam subcurves
lt = lt/l; %t value at each curve end
lt = mat2str(lt);
%p file
name = ['curve',num2str(id),'_',num2str(i),'_p'];
fileID = fopen([name,'.m'],'w');
content = ['function p = ',name,'(T)\n'...
    ' n = ',num2str(subcurve),';\n lt = ',lt,';\n'...
    ' for j = 1:n\n if T>=lt(j) && T<=lt(j+1)\n'...
    ' t = (T-lt(j))/(lt(j+1)-lt(j));\n'...
    ' str = [''@curve',num2str(id),'_',num2str(i),'_'',num2str(j),''_p''];\n'...
    ' f = str2func(str);\n p = f(t);\n break\n end\n end'];
fprintf(fileID,content);
fclose(fileID);
%t file
name = ['curve',num2str(id),'_',num2str(i),'_t'];
fileID = fopen([name,'.m'],'w');
content = ['function T = ',name,'(p)\n T = -1;\n'...
    ' n = ',num2str(subcurve),';\n lt = ',lt,';\n for j = 1:n\n'...
    ' str = [''@curve',num2str(id),'_',num2str(i),'_'',num2str(j),''_t''];\n'...
    ' f = str2func(str);\n t = f(p);\n if t >= 0\n'...
    ' T = t*(lt(j+1)-lt(j))+lt(j);\n break\n end\n end'];
fprintf(fileID,content);
fclose(fileID);
%y file
name = ['curve',num2str(id),'_',num2str(i),'_y'];
fileID = fopen([name,'.m'],'w');
content = ['function y = ',name,'(x)\n y = []\n;'...
    ' n = ',num2str(subcurve),';\n for j = 1:n\n'...
    ' str = [''@curve',num2str(id),'_',num2str(i),'_'',num2str(j),''_y''];\n'...
    ' f = str2func(str);\n yj = f(x);\n'...
    ' y = [y,yj];\n end'];
fprintf(fileID,content);
fclose(fileID);
