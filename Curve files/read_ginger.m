function curve = read_ginger
fileID = fopen('ginger.txt','r');
N = 7; %number of curves
curve = cell(7,2);
figure
for id = 1:N
    for i = 1:2
        formatSpec = '%d %d';
        psize = [1 2];
        pt_num = fscanf(fileID,formatSpec,psize);
        formatSpec = '%f %f';
        psize = [2 pt_num(2)];
        A = fscanf(fileID,formatSpec,psize);
        curve{id,i} = A';
        plot(A(1,:),A(2,:),'b')
        hold on
    end
end
hold off
fclose(fileID);

ginger = cell(N,1);
for i = 1:N
    %p file
    name = ['curve36_',num2str(i),'_p'];
    fileID = fopen([name,'.m'],'w');
    t = curve{i,2};
    t = t';
    t = fliplr(t);
    t  =t';
    pt = [curve{i,1};t(2:end,:)];
    ginger{i} = pt;
    pt = mat2str(pt);
    content = ['function p = ',name,'(t); '...
        'pt = ',pt,'; p = polycurve_p(t,pt);'];
    fprintf(fileID, '%s', content);
    fclose(fileID);
    %t file
    name = ['curve36_',num2str(i),'_t'];
    fileID = fopen([name,'.m'],'w');
    content = ['function t = ',name,'(p); '...
        'pt = ',pt,'; t = ginger_t(p,pt);'];
    fprintf(fileID, '%s', content);
    fclose(fileID);
    %y file
    name = ['curve36_',num2str(i),'_y'];
    fileID = fopen([name,'.m'],'w');
    content = ['function y = ',name,'(x); '...
        'pt = ',pt,'; y = polycurve_y(x,pt);'];
    fprintf(fileID, '%s', content);
    fclose(fileID);
end
save ginger.mat ginger