function p = curve11_1_p(t)
%curve function of example 11, ellipse
%INPUT:
%t: parameter, [0,1], counterclockwise
%a/b: length of axis, x^2/a^2+y^2/b^2=1
%cx,cy: center coordinates
%OUTPUT:
%p: [x y], point coordinate on curve

a = 2;
b = 1;
cx = 0;
cy = 0;
p = ellipsecurve_p(t,a,b,cx,cy);

