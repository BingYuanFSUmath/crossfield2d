function T = curve10_1_t(p)
%curve function of example 10, circle
%given point location on curve, return global parameter T
%INPUT:
%p: [x y], point coordinates on curve
%OUTPUT:
%T: parameter, [0,1], counterclockwise
T = -1;
x = p(1);
y = p(2);
tol = 1e-5; %1e-10 is too strict
if abs(sqrt(x^2+y^2)-1)<tol %on the circle
    theta = atan2(y,x); %[-pi,pi]
    theta = mod(theta,2*pi); %[0,2pi]
    T = theta/(2*pi);
end




