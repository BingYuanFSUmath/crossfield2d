function y = curve35_1_y(x)
%curve function of example 35, outer square
%INPUT:
%x: x coordinate
%OUTPUT:
%y: y coordinate, may be several, row vector

y = [];
tol = 1e-10; 
s_bdy=[-10,-10;10,-10;10,10;-10,10;-10,-10];%bdy pts
xmin = min(s_bdy(:,1));
xmax = max(s_bdy(:,1));
n = size(s_bdy,1)-1; %num of sides
if x < xmin || x > xmax %out of region
    y = 1e10; %helps to find intersection using root finding method
else 
    for i = 1:n
        x1 = s_bdy(i,1);
        y1 = s_bdy(i,2);
        x2 = s_bdy(i+1,1);
        y2 = s_bdy(i+1,2);
        xmin = min(x1,x2);
        xmax = max(x1,x2);
        if xmin<=x && x<=xmax
            if abs(x1-x2)<tol %vertical line
                y(end+1) = y1; %NOT ACCURATE
            else
                y(end+1) = (y2-y1)*(x-x1)/(x2-x1)+y1;
            end
        end
    end
end
y = unique(y);

