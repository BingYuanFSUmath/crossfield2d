function p = curve35_1_p(t)
%curve function of example 35, outer square
%INPUT:
%t: parameter, [0,1], counterclockwise
%OUTPUT:
%p: [x y], point coordinate on curve

s_bdy=[-10,-10;10,-10;10,10;-10,10;-10,-10];%bdy pts
n = size(s_bdy,1)-1; %num of sides
dt = 1.0/n; 
nt = ceil(t/dt); %on which side
if t == 0
    nt = 1;
end
t0 = (t-dt*(nt-1))*n; %t on side nt
x1 = s_bdy(nt,1);
y1 = s_bdy(nt,2);
x2 = s_bdy(nt+1,1);
y2 = s_bdy(nt+1,2);
x = x1+t0*(x2-x1);
y = y1+t0*(y2-y1);
p = [x y];


