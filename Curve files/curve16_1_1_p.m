 function p = curve16_1_1_p(t)
%curve function of example 16, NACA 0012
%inner air foil, upper part
%INPUT:
%t: parameter, [0,1], counterclockwise
%OUTPUT:
%p: [x y], point coordinate on curve

%{
%parameterize by t = sqrt(x)
xmin = 0;
xmax = sqrt(1.008930411365);
y = @(x) 0.6*(0.2969*x-0.1260*x^2-0.3516*x^4+0.2843*x^6-0.1015*x^8);
x = xmin+(1-t)*(xmax-xmin); %equaly divid sqrt(x)
p = [x^2 y(x)];
%}

%
%parameterize by t = x
xmin = 0;
xmax = 1.008930411365;
y = @(x) 0.6*(0.2969*sqrt(x)-0.1260*x-0.3516*x^2+0.2843*x^3-0.1015*x^4);
x = xmin+(1-t)*(xmax-xmin); %equaly divid x
p = [x y(x)];
%}

%{
%parameterize by arc length
%x = sqrt(X) to avoid singularity at xmin
xmin = 0;
xmax = sqrt(1.008930411365);
y = @(x) 0.6*(0.2969*x-0.1260*x^2-0.3516*x^4+0.2843*x^6-0.1015*x^8);
%syms s
%dy = matlabFunction(diff(0.6*(0.2969*s-0.1260*s^2 ...
%    -0.3516*s^4+0.2843*s^6-0.1015*s^8),s));
%quite slow using diff
dy = @(x) 0.6*(0.2969-0.1260*2*x-0.3516*4*x.^3+0.2843*6*x.^5 ...
    -0.1015*8*x.^7);
f = @(x) sqrt(1+dy(x).^2);
l = integral(f,xmin,xmax); %total curve length
g = @(x) integral(f,xmin,x)-(1-t)*l;
x = fzero(g,[xmin xmax]);
p = [x^2 y(x)];
%}


