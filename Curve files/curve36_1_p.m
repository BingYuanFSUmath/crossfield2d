function p = curve36_1_p(t)
if t<=0.2
    p = curve36_1_1_p(5*t);
else
    p = curve36_1_2_p(1.25*(t-0.2));
end
