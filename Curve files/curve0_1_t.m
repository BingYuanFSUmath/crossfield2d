function T = curve0_1_t(p)
 T = -1;
 n = 2;
 lt = [0;0.50099869668308;1];
 for j = 1:n
 str = ['@curve0_1_',num2str(j),'_t'];
 f = str2func(str);
 t = f(p);
 if t >= 0
 T = t*(lt(j+1)-lt(j))+lt(j);
 break
 end
 end