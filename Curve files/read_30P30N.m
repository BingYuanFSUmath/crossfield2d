%read 30P30N, enlarge by 10
fileID = fopen('30P30N_1.txt','r');
formatSpec = '%f %f';
sizeA = [2 Inf];
p1 = fscanf(fileID,formatSpec,sizeA);
fclose(fileID);
p1 = p1';
p1(:,1) = p1(:,1)-0.5;
p1 = p1.*10;

fileID = fopen('30P30N_2.txt','r');
formatSpec = '%f %f';
sizeA = [2 Inf];
p2 = fscanf(fileID,formatSpec,sizeA);
fclose(fileID);
p2 = p2';
p2(:,1) = p2(:,1)-0.5;
p2 = p2.*10;

fileID = fopen('30P30N_3.txt','r');
formatSpec = '%f %f';
sizeA = [2 Inf];
p3 = fscanf(fileID,formatSpec,sizeA);
fclose(fileID);
p3 = p3';
p3(:,1) = p3(:,1)-0.5;
p3 = p3.*10;

fileID = fopen('30P30N_4.txt','r');
formatSpec = '%f %f';
sizeA = [2 Inf];
p4 = fscanf(fileID,formatSpec,sizeA);
fclose(fileID);
p4 = p4';


%test plot
figure 
plot(p1(:,1),p1(:,2))
hold on
plot(p2(:,1),p2(:,2))
hold on
plot(p3(:,1),p3(:,2))
hold on
plot(p4(:,1),p4(:,2))
hold off

save p_30P30N.mat p1 p2 p3