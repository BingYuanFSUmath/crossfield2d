alpha = 0.35*pi; %rotation angle
a = 1.13; %inner square half length
pv1 = [-4 -4;4 -4;4 4;-4 4;-4 -4];
z = exp((alpha+pi/4)*1i)*a*[1+1i;-1+1i;-1-1i;1-1i;1+1i];
%complex coordinates of inner square
pv2 = [real(z) imag(z)];
curve = cell(2,1);
curve{1} = pv1;
curve{2} = pv2;
for i = 1:2
    %p file
    name = ['curve30_',num2str(i),'_p'];
    fileID = fopen([name,'.m'],'w');
    pt = mat2str(curve{i});
    content = ['function p = ',name,'(t); '...
        'pt = ',pt,'; p = polycurve_p(t,pt);'];
    fprintf(fileID, '%s', content);
    fclose(fileID);
    %t file
    name = ['curve30_',num2str(i),'_t'];
    fileID = fopen([name,'.m'],'w');
    content = ['function t = ',name,'(p); '...
        'pt = ',pt,'; t = polycurve_t(p,pt);'];
    fprintf(fileID, '%s', content);
    fclose(fileID);
    %y file
    name = ['curve30_',num2str(i),'_y'];
    fileID = fopen([name,'.m'],'w');
    content = ['function y = ',name,'(x); '...
        'pt = ',pt,'; y = polycurve_y(x,pt);'];
    fprintf(fileID, '%s', content);
    fclose(fileID);
end