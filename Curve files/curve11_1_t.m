function T = curve11_1_t(p)
%curve function of example 11, ellipse
%given point location on curve, return global parameter T
%INPUT:
%p: [x y], point coordinates on curve
%a/b: length of axis, x^2/a^2+y^2/b^2=1
%cx,cy: center coordinates
%OUTPUT:
%T: parameter, [0,1], counterclockwise

a = 2;
b = 1;
cx = 0;
cy = 0;
T = ellipsecurve_t(p,a,b,cx,cy);
    