function sp = curve_split(p,theta_sp)
%split a curve by examing the angle between two tangent vectors
%INPUT:
%p: a set of points defined curve
%theta_sp: split angle criteria
%OUTPUT:
%sp: a cell array, each cell contains splited pts

sp = cell(0,1);
N = size(p,1); %num of pts
start = 1; %start id
for i = 2:N-1
    v1 = p(i-1,:)-p(i,:);
    v2 = p(i+1,:)-p(i,:);
    theta = acos(dot(v1,v2)/(norm(v1)*norm(v2)));
    if theta < theta_sp
        %split
        sp{end+1} = p(start:i,:);
        start = i;
    end
end
sp{end+1} = p(start:end,:);
if size(sp,2)>1 %more than one subcurve
    %check angle between start/end
    %curve may begin in the middle of a smooth curve
    v1 = p(2,:)-p(1,:);
    v2 = p(end-1,:)-p(end,:);
    theta = acos(dot(v1,v2)/(norm(v1)*norm(v2)));
    if theta >= 5*pi/6 %smooth curve, seam
        sp{1} = [sp{end};sp{1}(2:end,:)]; %p start/end duplicated
        sp(end) = [];
    end
end
    
