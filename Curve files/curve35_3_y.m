function y = curve35_3_y(x)
 y = []
; n = 1;
 for j = 1:n
 str = ['@curve35_3_',num2str(j),'_y'];
 f = str2func(str);
 yj = f(x);
 y = [y,yj];
 end