function y = curve24_1_y(x)
%curve function of example 24, Square minus quater circle
%INPUT:
%x: x coordinate
%OUTPUT:
%y: y coordinate, may be several, row vector

if -1<=x && x<=0
    y = [-1;1];
elseif 0<=x && x<=1
    y = [-sqrt(1-(x-1)^2)+1;-1];
else
    y = 1e10;
end

    



