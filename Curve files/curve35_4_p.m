function p = curve35_4_p(T)
 n = 2;
 lt = [0;0.382713898076173;1];
 for j = 1:n
 if T>=lt(j) && T<=lt(j+1)
 t = (T-lt(j))/(lt(j+1)-lt(j));
 str = ['@curve35_4_',num2str(j),'_p'];
 f = str2func(str);
 p = f(t);
 break
 end
 end