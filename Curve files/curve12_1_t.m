function T = curve12_1_t(p)
%curve function of example 12, rectangle with hole
%given point location on curve, return global parameter T
%INPUT:
%p: [x y], point coordinates on curve
%OUTPUT:
%T: parameter, [0,1], counterclockwise

s_bdy=[-1 -1;-1 1;1 1;1 -1;-1 -1];
T = polycurve_t(p,s_bdy);




