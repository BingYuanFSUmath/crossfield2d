function T = curve38_1_t(p)
%curve function of example 9, half circle
%given point location on curve, return global parameter T
%INPUT:
%p: [x y], point coordinates on curve
%OUTPUT:
%T: parameter, [0,1], counterclockwise
T = -1;
x = p(1);
y = p(2);
tol = 1e-10; %y=0 has machine error
if y>=-1 && y<=1
    if x < -tol %left part
        if abs(norm(p)-1)<1e-5 %on the half circle
            theta = atan(y/x); %[-pi/2,pi/2]
            theta = theta+pi/2; %[0,pi]
            T = 0.5*theta/pi;
        end
    elseif abs(x)<=tol
        %do not use circle for y=0
        %theta = acos(x); %[0,pi]
        %theta = 2*pi-theta; %[pi,2*pi]
        T = (y+1)/4+0.5;
    end
end



