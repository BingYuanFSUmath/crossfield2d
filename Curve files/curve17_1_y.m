function y = curve17_1_y(x)
%curve function of example 17, mushroom
%INPUT:
%x: x coordinate
%OUTPUT:
%y: y coordinate, may be several, row vector

tol = 1e-10;
if (-1<=x && x<=-0.5)||(0.5<=x && x<=1)
    y = [0;sqrt(1-x^2)];
elseif (-0.5<=x && x<=-0.25)||(0.25<=x && x<=0.5)
    y = [-1;sqrt(1-x^2)];
elseif -0.25<=x && x<=0.25
    y = [sqrt(1-x^2);sqrt(0.25^2-x^2)-1];
else 
    y = 1e10;
end
if abs(x+1)<tol || abs(x-1)<tol
    y = 0;
elseif abs(x+0.5)<tol || abs(x-0.5)<tol
    y = [sqrt(1-x^2);0;-1];
end
    



