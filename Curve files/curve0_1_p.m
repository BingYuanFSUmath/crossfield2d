function p = curve0_1_p(T)
 n = 2;
 lt = [0;0.50099869668308;1];
 for j = 1:n
 if T>=lt(j) && T<=lt(j+1)
 t = (T-lt(j))/(lt(j+1)-lt(j));
 str = ['@curve0_1_',num2str(j),'_p'];
 f = str2func(str);
 p = f(t);
 break
 end
 end