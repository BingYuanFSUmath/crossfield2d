function y = curve22_1_y(x)
%curve function of example 22, Rectangle with half hole
%INPUT:
%x: x coordinate
%OUTPUT:
%y: y coordinate, may be several, row vector

if (-1<=x && x<=-0.5)||(0.5<=x && x<=1)
    y = [0;1];
elseif -0.5<=x && x<=0.5
    y = [sqrt(0.5^2-x^2);1];
else
    y = 1e10;
end

    



