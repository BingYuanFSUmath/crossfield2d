function p = curve13_2_p(t)
%curve function of example 13, square in square
%INPUT:
%t: parameter, [0,1], counterclockwise
%OUTPUT:
%p: [x y], point coordinate on curve

s_bdy=[-1 0;0 -1;1 0;0 1;-1 0];
p = polycurve_p(t,s_bdy);


