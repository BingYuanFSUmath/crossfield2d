function T = curve35_2_t(p)
 T = -1;
 n = 3;
 lt = [0;0.0303747060753143;0.920512235000751;1];
 for j = 1:n
 str = ['@curve35_2_',num2str(j),'_t'];
 f = str2func(str);
 t = f(p);
 if t >= 0
 T = t*(lt(j+1)-lt(j))+lt(j);
 break
 end
 end