function T = curve17_1_t(p)
%curve function of example 17, mushroom
%given point location on curve, return global parameter T
%INPUT:
%p: [x y], point coordinates on curve
%OUTPUT:
%T: parameter, [0,1], counterclockwise
T = -1;
x = p(1);
y = p(2);
tol = 1e-5; %y=0 has machine error
if abs(x^2+y^2-1)<1e-3 && y>tol
    t = atan2(y,x)/pi;
    T = t*1/8;
elseif -1<=x && x<=-0.5 && abs(y)<tol
    t = (x+1)/0.5;
    T = t/8+1/8;
elseif abs(x+0.5)<tol && -1<=y && y<=0
    t = -y;
    T = t/8+2/8;
elseif -0.5<=x && x<=-0.25 && abs(y+1)<tol
    t = (x+0.5)/0.25;
    T = t/8+3/8;
elseif abs(x^2+(y+1)^2-0.25^2)<1e-3 && y+1>tol
    t = 1-atan2(y+1,x)/pi;
    T = t/8+4/8;
elseif 0.25<=x && x<=0.5 && abs(y+1)<tol
    t = (x-0.25)/0.25;
    T = t/8+5/8;
elseif abs(x-0.5)<tol && -1<=y && y<=0
    t = y+1;
    T = t/8+6/8;
elseif 0.5<=x && x<=1 && abs(y)<tol
    t = (x-0.5)/0.5;
    T = t/8+7/8;
end    
end



