function T = curve13_2_t(p)
%curve function of example 13, square in square
%given point location on curve, return global parameter T
%INPUT:
%p: [x y], point coordinates on curve
%OUTPUT:
%T: parameter, [0,1], counterclockwise

s_bdy=[-1 0;0 -1;1 0;0 1;-1 0];
T = polycurve_t(p,s_bdy);




