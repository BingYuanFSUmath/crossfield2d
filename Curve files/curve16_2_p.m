function p = curve16_2_p(t)
%curve function of example 16
%outer circle with center (2,0) and radius 4,
%INPUT:
%t: global parameter
%OUTPUT:
%p: [x y], point coordinates on curve

%{
%use polar coordinates for circle
theta = t*2*pi; %angle, [0,2pi]
r = 4; %radius
c = [2 0]; %center
p = [r*cos(theta) r*sin(theta)]+c;
%}
s_bdy = [-2,-4;6,-4;6,4;-2,4;-2,-4];%bdy pts
n = size(s_bdy,1)-1; %num of sides
dt = 1.0/n; 
nt = ceil(t/dt); %on which side
if abs(t) < 1e-10 %t == 0
    nt = 1;
end

t0 = (t-dt*(nt-1))*n; %t on side nt
x1 = s_bdy(nt,1);
y1 = s_bdy(nt,2);
x2 = s_bdy(nt+1,1);
y2 = s_bdy(nt+1,2);
x = x1+t0*(x2-x1);
y = y1+t0*(y2-y1);
p = [x y];


