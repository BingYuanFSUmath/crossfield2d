function p = curve22_1_p(t)
%curve function of example 22, Rectangle with half hole
%INPUT:
%t: parameter, [0,1], counterclockwise
%OUTPUT:
%p: [x y], point coordinate on curve

if t<=1/6
    x = 1;
    y = 6*t;
elseif t<=2/6
    x = 1-6*(t-1/6)*2;
    y = 1;
elseif t<=3/6
    x = -1;
    y = 1-6*(t-2/6);
elseif t<=4/6
    x = -1+6*(t-3/6)*0.5;
    y = 0;
elseif t<=5/6
    theta = pi-6*(t-4/6)*pi;
    x = 0.5*cos(theta);
    y = 0.5*sin(theta);
else
    x = 0.5+6*(t-5/6)*0.5;
    y = 0;
end
p = [x y];


