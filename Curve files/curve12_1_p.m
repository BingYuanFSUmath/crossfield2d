function p = curve12_1_p(t)
%curve function of example 12, rectangle with hole
%INPUT:
%t: parameter, [0,1], counterclockwise
%OUTPUT:
%p: [x y], point coordinate on curve

s_bdy=[-1 -1;-1 1;1 1;1 -1;-1 -1];
p = polycurve_p(t,s_bdy);


