function y = curve16_1_y(x)
%curve function of example 16, NACA 0012
%INPUT:
%x: x coordinate
%OUTPUT:
%y: y coordinate, may be several, row vector

xmin = 0;
xmax = 1.008930411365;
if x < xmin || x > xmax %out of region
    y = 1e10; %helps to find intersection using root finding method
else %coordinates on curve 1
    y = 0.6*(0.2969*sqrt(x)-0.1260*x-0.3516*x^2+0.2843*x^3-0.1015*x^4);
end
%coordinates on curve 2
if x > xmin && x < xmax
    y(end+1) = -0.6*(0.2969*sqrt(x)-0.1260*x-0.3516*x^2+0.2843*x^3-0.1015*x^4);
end

    



