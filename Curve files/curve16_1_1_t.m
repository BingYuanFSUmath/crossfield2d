function t = curve16_1_1_t(p)
%curve function of example 16, NACA 0012
%inner air foil, upper part
%given point location on curve, return local parameter t
%INPUT:
%p: [x y], point coordinates on curve
%OUTPUT:
%t: parameter, [0,1], counterclockwise

t = -1; %if not on this curve, return -1
%{
%parameterize by t = sqrt(x)
xmin = 0;
xmax = sqrt(1.008930411365);
x0 = sqrt(p(1));
y0 = p(2);
tol = 1e-4; %based on trimesh generator

if x0 >= xmin-1e-10 && x0 <= xmax+1e-10 %possibly on this curve
    y = @(x) 0.6*(0.2969*x-0.1260*x^2-0.3516*x^4+0.2843*x^6-0.1015*x^8);
    if abs(y0-y(x0))<tol %on this curve
        t= abs(1-(x0-xmin)/(xmax-xmin));
    end
end
%}

%
%parameterize by t = x
xmin = 0;
xmax = 1.008930411365;
x0 = p(1);
y0 = p(2);
tol = 1e-4; %based on trimesh generator
if x0 >= xmin-1e-10 && x0 <= xmax+1e-10 %possibly on this curve
    y = @(x) 0.6*(0.2969*sqrt(x)-0.1260*x-0.3516*x^2+0.2843*x^3-0.1015*x^4);
    if abs(y0-y(x0))<tol %on this curve
        t= abs(1-(x0-xmin)/(xmax-xmin));
    end
end
%}

%{
%parameterize by arc length
if x0 >= xmin && x0 <= xmax %possibly on this curve
    y = @(x) 0.6*(0.2969*x-0.1260*x^2-0.3516*x^4+0.2843*x^6-0.1015*x^8);
    if abs(y0-y(x0))<tol %on this curve
        syms s
        dy = matlabFunction(diff(0.6*(0.2969*s-0.1260*s^2 ...
            -0.3516*s^4+0.2843*s^6-0.1015*s^8),s));
        f = @(x) sqrt(1+dy(x).^2);
        l = integral(f,xmin,xmax); %total curve length
        l0 =l-integral(f,xmin,x0); %counterclockwise
        t = l0/l;
    end
end
%}

