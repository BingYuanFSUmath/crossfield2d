function T = curve22_1_t(p)
%curve function of example 22, Rectangle with half hole
%given point location on curve, return global parameter T
%INPUT:
%p: [x y], point coordinates on curve
%OUTPUT:
%T: parameter, [0,1], counterclockwise

x = p(1);
y = p(2);
tol = 1e-5; %y=0 has machine error
if abs(x-1)<tol && 0<=y && y<=1
    t = y;
    T = t/6;
elseif abs(y-1)<tol && -1<=x && x<=1
    t = 1-(x+1)/2;
    T = 1/6+t/6;
elseif abs(x+1)<tol && 0<=y && y<=1
    t = 1-y;
    T = 2/6+t/6;
elseif abs(y)<tol && -1<=x && x<=-0.5
    t = (x+1)/0.5;
    T = 3/6+t/6;
elseif abs(x^2+y^2-0.5^2)<1e-3 && abs(y)>0
    theta = atan2(y,x);
    t = (pi-theta)/pi;
    T = 4/6+t/6;
elseif abs(y)<tol && 0.5<=x && x<=1
    t = (x-0.5)/0.5;
    T = 5/6+t/6;
else
    T = -1;
end
    



