function T = curve16_1_t(p)
%curve function of example 16, NACA 0012
%inner air foil
%given point location on curve, return global parameter T
%INPUT:
%p: [x y], point coordinates on curve
%OUTPUT:
%T: parameter, [0,1], counterclockwise

T = -1; 
n = 2; %number of curves
for i = 1:n
    str = ['@curve16_1_' num2str(i) '_t'];
    f = str2func(str);
    t = f(p);
    if t >= 0 %on that curve
        T = t/n+1/n*(i-1);
        break
    end
end


