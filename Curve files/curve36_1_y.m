function y = curve36_1_y(x)
y1 = curve36_1_1_y(x);
y2 = curve36_1_2_y(x);
if y1(1) == 1e10
    y = y2;
elseif y2(1) == 1e10
    y = y1;
else 
    y = [y1,y2];
end
