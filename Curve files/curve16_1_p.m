function p = curve16_1_p(T)
%curve function of example 16, NACA 0012
%INPUT:
%T: global parameter, [0,1], counterclockwise
%OUTPUT:
%p: [x y], point coordinate on curve
n = 2; %number of curves
for i = 1:n
    if T >= (i-1)/n && T <= i/n
        t = T*n-(i-1);
        str = ['@curve16_1_' num2str(i) '_p'];
        f = str2func(str);
        p = f(t);
    end
end



