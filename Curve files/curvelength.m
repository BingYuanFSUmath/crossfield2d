function l = curvelength(p)
%find the length of piecewise linear curve defined by p
%INPUT:
%p: points define the piecewise linear curve
%OUTPUT:
%l: curve length
l = 0;
for i = 2:size(p,1)
    l = l+norm(p(i,:)-p(i-1,:));
end