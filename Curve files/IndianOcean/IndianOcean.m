%read ocean points for spline curve
fileID = fopen('IndianOcean.txt','r');
N = 3; %number of curves
curve = cell(N,1); %pts on each curve
sp = cell(N,1); %split points on each curve
for id = 1:N
    formatSpec = '%d %d';
    psize = [1,2]; %output size, fill in column
    pt_num = fscanf(fileID,formatSpec,psize); %read in row
    formatSpec = '%f %f';
    psize = [2 pt_num(2)];
    p = fscanf(fileID,formatSpec,psize);
    curve{id} = flipud(p');
    %remove close points to avoid bad spline
    curve{id} = unique_row(curve{id},1e-3);
    curve{id}(end+1,:) = curve{id}(1,:);
    sp{id} = spline_generator(39,id,curve{id},3*pi/4);
end
fclose(fileID);

ocean = cell(N,1);
scale = 3; %num of polygon pts = scale*num of pts
s_bdy = [];
for i = 1:N
    nsp = size(sp{i},1);
    name = ['curve39_',num2str(i),'_t'];
    f = str2func(name);
    np = size(curve{i},1)*scale;
    dt = 1/np;
    t0 = 0;
    t = [];
    for j = 1:nsp
        t1 = f(sp{i}(j,:)); %find t of split points
        t = [t,t0:dt:t1-dt];
        if t1-t0<=dt
            t = [t,t0];
        end
        t0 = t1;
    end
    t = [t,t0:dt:1];
    name = ['curve39_',num2str(i),'_p'];
    f = str2func(name);
    ocean{i} = zeros(size(t,2),2);
    for j = 1:size(t,2)
        ocean{i}(j,:) = f(t(j));
    end
    s_bdy = [s_bdy;sp{i}];
end
s_bdy = [s_bdy;s_bdy(1,:)];

figure
for i = 1:N
    t = (0:0.0001:1)';
    p = zeros(size(t,1),2);
    name = ['curve39_',num2str(i),'_p'];
    f = str2func(name);
    for j = 1:size(t,1)
        p(j,:) = f(t(j));
    end
    plot(curve{i}(:,1),curve{i}(:,2),'b')
    hold on
    plot(p(:,1),p(:,2),'r')
    hold on
    scatter(sp{i}(:,1),sp{i}(:,2),10)
    hold on
    scatter(ocean{i}(:,1),ocean{i}(:,2),5,'filled')
    hold on
end
hold off      
save ocean_spline.mat ocean s_bdy