%Generate curve points for coarse mesh

load ocean_coarse_p_Q.mat 
%find boundary points
p_bdy = [];
for pid = 1:size(p,1)
    %find quads connected to p
    %record edges in quads
    edges = [];
    for qid = 1:size(Q,1)
        q = Q(qid,:);
        if ismember(pid,q)
            %add edges
            for i = 1:4
                id1 = i;
                id2 = i+1;
                if i == 4
                    id2 = 1;
                end
                if q(id1)~=pid && q(id2)~=pid 
                    edges = [edges;q(id1),q(id2)];
                end
            end
        end
    end
    %check closure of edges
    pts = unique(edges);
    for i = 1:size(pts,1)
        count = 0;
        for j = 1:size(edges,1)
            if pts(i)==edges(j,1)
                count = count+1;
            end
            if pts(i)==edges(j,2)
                count = count+1;
            end
        end
        if count==1 %find boundary point
            p_bdy(end+1,:) = pid;
            break
        end
    end        
end
%split boundary points to different boundaries
curve = cell(1);
curve_id = 1;
pid =  p_bdy(1);

while ~isempty(p_bdy)
    %find edges connected to boundary pt
    for qid = 1:size(Q,1)
        flag = 0; %inicator of finding next pid
        q = Q(qid,:);
        if ismember(pid,q)
            %add edges
            for i = 1:4
                id1 = i;
                id2 = i+1;
                if i == 4
                    id2 = 1;
                end
                if (q(id1)==pid && ismember(q(id2),p_bdy))...
                        ||(q(id2)==pid && ismember(q(id1),p_bdy))
                    %delete pid in p_bdy
                    p_bdy = p_bdy(p_bdy~=pid);
                    curve{curve_id}(end+1,:) = p(pid,:);
                    edge = [q(id1),q(id2)];
                    if ~bounday_edge(edge,Q)
                        continue
                    end
                    x = [p(edge(1),:);p(edge(2),:)];
                    %find next pid
                    pid = edge(edge~=pid);
                    flag = 1;
                    break
                end
            end
        end
        if flag
            break %break qid
        end
    end
    if ~flag
        %finish loop
        %delete pid in p_bdy
        p_bdy = p_bdy(p_bdy~=pid);
        curve{curve_id}(end+1,:) = p(pid,:);
        if ~isempty(p_bdy)
            curve_id = curve_id+1;
            curve(end+1,:) = cell(1);
            pid = p_bdy(1);
        end
    end
end

%scale curve so dmin = 0.1
dmin = Inf;
for i = 1:size(curve,1)
    d = curve{i}(2:end,:)-curve{i}(1:end-1,:);
    d = norm(d);
    d0 = min(d);
    if d0<dmin
        dmin = d0;
    end
end
scale = 0.1/dmin;

figure 
for i = 1:size(curve,1)
    %p is not unique!
    %unique does not keep order
    curve{i} = unique_row(curve{i},1e-6);
    curve{i} = scale*curve{i};
    curve{i}(end+1,:) = curve{i}(1,:);
    %plot(curve{i}(:,1),curve{i}(:,2))
    quiver(curve{i}(1:end-1,1),curve{i}(1:end-1,2),...
        curve{i}(2:end,1)-curve{i}(1:end-1,1),...
        curve{i}(2:end,2)-curve{i}(1:end-1,2))
    hold on
end
hold off
curve{1} = flipud(curve{1});
curve{3} = flipud(curve{3});
figure 
for i = 1:size(curve,1)
    quiver(curve{i}(1:end-1,1),curve{i}(1:end-1,2),...
        curve{i}(2:end,1)-curve{i}(1:end-1,1),...
        curve{i}(2:end,2)-curve{i}(1:end-1,2))
    hold on
end
hold off

%cut region
x1 = [-3.82099897183338,-2.32088030263833];
x2 = [0.728963652002926,-2.80220459510117];
for i = 1:size(curve{1},1)
    if norm(curve{1}(i,:)-x1)<1e-10
        pstart = i;
    end
    if norm(curve{1}(i,:)-x2)<1e-10
        pend = i;
        break
    end
end
curve{1}(pstart+1,:) = [x1(1),-3.5];
curve{1}(pstart+2,:) = [x2(1),-3.5];
curve{1}(pstart+3:pend-1,:) = [];

figure 
for i = 1:size(curve,1)
    quiver(curve{i}(1:end-1,1),curve{i}(1:end-1,2),...
        curve{i}(2:end,1)-curve{i}(1:end-1,1),...
        curve{i}(2:end,2)-curve{i}(1:end-1,2))
    hold on
end
hold off

save ocean_coarse.mat curve

function flag = bounday_edge(edge,Q)
count = 0;
for i = 1:size(Q,1)
    if ismember(edge(1),Q(i,:)) && ismember(edge(2),Q(i,:))
        count = count +1;
    end
end
if count == 1
    flag = 1;
else
    flag = 0;
end
end