%generate polygon curve files for coarse mesh
load ocean_coarse;
s_bdy = [];
for i = 1:size(curve,1)
    poly_generator(39,i,curve{i})
    %all points on curve are fixed
    %s_bdy = [s_bdy;curve{i}(1:end-1,:)];
    %only fix points not that smooth
    theta = 3*pi/4; %angle decide smoothness
    sp = curve_split(curve{i},theta);
    for j = 1:size(sp,2)
        s_bdy(end+1,:) = sp{j}(1,:);
    end
end
s_bdy(end+1,:) = s_bdy(1,:);
ocean = curve;
save ocean_coarse_poly.mat ocean s_bdy
