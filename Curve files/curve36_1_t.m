function T = curve36_1_t(p)
t = curve36_1_1_t(p);
if t>-1
    T = t*0.2;
else %t == -1 on first curve
    t = curve36_1_2_t(p);
    if t>-1
        T = t*0.8+0.2;
    else
        T = -1;
    end
end


    
