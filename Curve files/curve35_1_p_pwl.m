function p = curve35_1_p(t)
%curve function of example 35, three elements airfoil
%INPUT:
%t: parameter, [0,1], counterclockwise
%OUTPUT:
%p: [x y], point coordinate on curve

load p_30P30N.mat %p1,p2,p3
n = size(p1,1)-1; %num of sides
dt = 1.0/n; 
nt = ceil(t/dt); %on which side
if t == 0
    nt = 1;
end
t0 = (t-dt*(nt-1))*n; %t on side nt
x1 = p1(nt,1);
y1 = p1(nt,2);
x2 = p1(nt+1,1);
y2 = p1(nt+1,2);
x = x1+t0*(x2-x1);
y = y1+t0*(y2-y1);
p = [x y];


