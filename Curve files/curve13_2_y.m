function y = curve13_2_y(x)
%curve function of example 13, square in square
%INPUT:
%x: x coordinate
%OUTPUT:
%y: y coordinate, may be several, row vector

s_bdy=[-1 0;0 -1;1 0;0 1;-1 0];
y = polycurve_y(x,s_bdy);
    



