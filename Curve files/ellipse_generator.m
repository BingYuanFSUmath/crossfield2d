function ellipse_generator(id,i,a,b,cx,cy,varargin)
%generate ellipse curve files
%INPUT:
%id: example id
%i: curve number
%a/b: length of axis, x^2/a^2+y^2/b^2=1
%cx,cy: center coordinates
%ta,tb: angle range of ellipse
%OUTPUT:
%curve functions: p/t/y

a = mat2str(a);
b = mat2str(b);
cx = mat2str(cx);
cy = mat2str(cy);
if (nVarargs == 0 ) %[0,2pi]
%p file
name = ['curve',num2str(id),'_',num2str(i),'_p'];
fileID = fopen([name,'.m'],'w');
content = ['function p = ',name,'(t); '...
    'a = ',a,';b = ',b,';cx = ',cx,';cy = ',cy,';'...
    'p = ellipsecurve_p(t,a,b,cx,cy);'];
fprintf(fileID, '%s', content);
fclose(fileID);
%t file
name = ['curve',num2str(id),'_',num2str(i),'_t'];
fileID = fopen([name,'.m'],'w');
content = ['function t = ',name,'(p); '...
    'a = ',a,';b = ',b,';cx = ',cx,';cy = ',cy,';'...
    't = ellipsecurve_t(p,a,b,cx,cy);'];
fprintf(fileID, '%s', content);
fclose(fileID);
%y file
name = ['curve',num2str(id),'_',num2str(i),'_y'];
fileID = fopen([name,'.m'],'w');
content = ['function y = ',name,'(x); '...
    'a = ',a,';b = ',b,';cx = ',cx,';cy = ',cy,';'...
    'y = ellipsecurve_y(x,a,b,cx,cy);'];
fprintf(fileID, '%s', content);
fclose(fileID);
else %[theta_a,theta_b]
    ta = varargin{1};
    dt = varargin{2};
%p file
name = ['curve',num2str(id),'_',num2str(i),'_p'];
fileID = fopen([name,'.m'],'w');
content = ['function p = ',name,'(t); '...
    'a = ',a,';b = ',b,';cx = ',cx,';cy = ',cy,';'...
    'ta = ',ta,';dt = ',dt,';'...
    'p = ellipsecurve_p(t,a,b,cx,cy,ta,dt);'];
fprintf(fileID, '%s', content);
fclose(fileID);
%t file
name = ['curve',num2str(id),'_',num2str(i),'_t'];
fileID = fopen([name,'.m'],'w');
content = ['function t = ',name,'(p); '...
    'a = ',a,';b = ',b,';cx = ',cx,';cy = ',cy,';'...
    'ta = ',ta,';dt = ',dt,';'...
    't = ellipsecurve_t(p,a,b,cx,cy,ta,dt);'];
fprintf(fileID, '%s', content);
fclose(fileID);
%y file
name = ['curve',num2str(id),'_',num2str(i),'_y'];
fileID = fopen([name,'.m'],'w');
content = ['function y = ',name,'(x); '...
    'a = ',a,';b = ',b,';cx = ',cx,';cy = ',cy,';'...
    'ta = ',ta,';dt = ',dt,';'...
    'y = ellipsecurve_y(x,a,b,cx,cy,ta,dt);'];
fprintf(fileID, '%s', content);
fclose(fileID);
end