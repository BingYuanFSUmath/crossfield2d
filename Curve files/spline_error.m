function spline_error 
%test routine for spline
t = 0:0.01:1; %error pts
t0 = 0:0.1:1; %sample pts
%%%%%%%%%%%%%%%%%%%%%%
%test1:circle
%%%%%%%%%%%%%%%%%%%%%%
theta0 = t0*2*pi;
x0 = cos(theta0); %sample pts
y0 = sin(theta0);
theta = t*2*pi;
x = cos(theta); %exact pts
y = sin(theta);
xs = x; %spline pts
ys = y;
A1 = spline_coef(x0',1); %periodic
A2 = spline_coef(y0',1); %periodic
%A1 = spline_coef(x0',2,x0(2)-x0(1),x0(end)-x0(end-1)); %end slope
%A2 = spline_coef(y0',2,y0(2)-y0(1),y0(end)-y0(end-1)); %end slope
for i = 1:size(t,2)
    xs(i) = spline_p(A1,t(i));
    ys(i) = spline_p(A2,t(i));
end
figure
plot(x,y,'b')
hold on
plot(xs,ys,'r')
hold off
axis equal
ex = max(abs(x-xs));
ey = max(abs(y-ys));
s = ['circle error: x,y,',num2str(ex),',',num2str(ey)];
disp(s)
%%%%%%%%%%%%%%%%%%%%%%
%test2:poly n=3
%%%%%%%%%%%%%%%%%%%%%%
x0 = t0; %sample pts
y0 = 0.3*x0.^3+2*x0-1;
x = t; %exact pts
y = 0.3*x.^3+2*x-1;
xs = x; %spline pts
ys = y;
%A1 = spline_coef(x0',0); %natural
%A2 = spline_coef(y0',0); %natural
A1 = spline_coef(x0',2,x0(2)-x0(1),x0(end)-x0(end-1)); %end slope
A2 = spline_coef(y0',2,y0(2)-y0(1),y0(end)-y0(end-1)); %end slope
for i = 1:size(t,2)
    xs(i) = spline_p(A1,t(i));
    ys(i) = spline_p(A2,t(i));
end
figure
plot(x,y,'b')
hold on
plot(xs,ys,'r')
hold off
axis equal
ex = max(abs(x-xs));
ey = max(abs(y-ys));
s = ['poly n=3 error: x,y,',num2str(ex),',',num2str(ey)];
disp(s)
%%%%%%%%%%%%%%%%%%%%%%
%test2:poly n=5
%%%%%%%%%%%%%%%%%%%%%%
x0 = t0.^5+2*t0.^3-3*t0; %sample pts
y0 = -t0.^5-t0.^2+1;
x = t.^5+2*t.^3-3*t; %exact pts
y = -t.^5-t.^2+1;
xs = x; %spline pts
ys = y;
A1 = spline_coef(x0',0); %natural
A2 = spline_coef(y0',0); %natural
for i = 1:size(t,2)
    xs(i) = spline_p(A1,t(i));
    ys(i) = spline_p(A2,t(i));
end
figure
plot(x,y,'b')
hold on
plot(xs,ys,'r')
hold off
axis equal
ex = max(abs(x-xs));
ey = max(abs(y-ys));
s = ['poly n=5 error: x,y,',num2str(ex),',',num2str(ey)];
disp(s)

%%%%%%%%%%%%%%%%%%%%%%
%spline_t tests
%%%%%%%%%%%%%%%%%%%%%%
t = 0:0.01:1; %error pts
t0 = 0:0.1:1; %sample pts
%%%%%%%%%%%%%%%%%%%%%%
%test1:circle
%%%%%%%%%%%%%%%%%%%%%%
%{
theta0 = t0*2*pi;
x0 = cos(theta0); %sample pts
y0 = sin(theta0);
theta = t*2*pi;
xs = t; %spline pts
ys = t;
ts = t;
A1 = spline_coef(x0',1); %periodic
A2 = spline_coef(y0',1); %periodic
for i = 1:size(t,2)
    xs(i) = spline_p(A1,t(i));
    ys(i) = spline_p(A2,t(i));
    ts(i) = spline_t([xs(i),ys(i)],A1,A2,[x0',y0']);
end
%}
%%%%%%%%%%%%%%%%%%%%%%
%test2:parametric functions
%%%%%%%%%%%%%%%%%%%%%%
x0 = t0.^3+t0; %sample pts
y0 = 0.*t0;
xs = t; %spline pts
ys = t;
ts = t;
A1 = spline_coef(x0',1); %periodic
A2 = spline_coef(y0',1); %periodic
for i = 1:size(t,2)
    xs(i) = spline_p(A1,t(i));
    ys(i) = spline_p(A2,t(i));
    ts(i) = spline_t([xs(i),ys(i)],A1,A2,[x0',y0']);
    ts = ts';
end