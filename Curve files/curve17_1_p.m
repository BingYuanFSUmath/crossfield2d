function p = curve17_1_p(t)
%curve function of example 17, mushroom
%INPUT:
%t: parameter, [0,1], counterclockwise
%OUTPUT:
%p: [x y], point coordinate on curve

if t<=1/8
    theta = 8*t*pi;
    x = cos(theta);
    y = sin(theta);
elseif t<=2/8
    x = -1+8*(t-1/8)*0.5;
    y = 0;
elseif t<=3/8
    x = -0.5;
    y = -8*(t-2/8);
elseif t<=4/8
    x = -0.5+8*(t-3/8)*0.25;
    y = -1;
elseif t<=5/8
    theta = pi-8*(t-4/8)*pi;
    x = 0.25*cos(theta);
    y = 0.25*sin(theta)-1;
elseif t<=6/8
    x = 0.25+8*(t-5/8)*0.25;
    y = -1;
elseif t<=7/8
    x = 0.5;
    y = -1+8*(t-6/8);
else
    x = 0.5+8*(t-7/8)*0.5;
    y = 0;
end
p = [x,y];


