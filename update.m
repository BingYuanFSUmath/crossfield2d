function [xnew,ynew,v1new,v2new,eid] = update(x,y,d,eid,vid1,vid2,TR,N)
%track eid, may interpolate in the same triangle
%d has already been interpolated at x
%eid is the current triangle id
error = 1e-14;
status = false; %have not found interpolation point
count = 0; %force to quit
%need to loop twice if int point is in the original triangle
while ~status
    count = count+1;
    if count == 3
        break
    end
    for id = TR.ConnectivityList(eid,:) %loop through verteices of triangle eid
        if (id ~= vid1) && (id ~= vid2)
            vid3 = id; %find the 3rd vertex id
            break
        end
    end
    %find intersection point on v1,v3
    x1 = N(vid1).x;
    y1 = N(vid1).y;
    x2 = N(vid3).x;
    y2 = N(vid3).y;
    [xint,yint] = find_int_point(x,y,d,x1,y1,x2,y2); %intersection coordinates
    if min([x1 x2])-error<=xint && xint<=max([x1 x2]+error)... %xint on edge
            && min([y1 y2])-error<=yint && yint<=max([y1 y2]+error)%yint on edge
        status = true; %int point found
        xnew = xint;
        ynew = yint;
        v1new = vid1;
        v2new = vid3;
    end
    if ~status %haven't found int point
        %find intersection point on v2,v3
        x1 = N(vid2).x;
        y1 = N(vid2).y;
        [xint,yint] = find_int_point(x,y,d,x1,y1,x2,y2); %intersection coordinates
        status = true;
        xnew = xint;
        ynew = yint;
        v1new = vid2;
        v2new = vid3;
    end
    %interpolation point could be in the incoming triangle
    if dot([xint yint]-[x y],d)<0 %interpolation in opposite direction
        status = false; %need to find interpolation pt agian
        ti = edgeAttachments(TR,vid1,vid2); %triangle attached to edge(v1,v2)
        %ti: should be 1x1 cell, row arrary stores edge ids
        if size(ti{1},2) == 1 %only one element attached,boundary
            %THIS SHOULD NOT HAPPEN, BECAUSE BOUNDARY HAS BEEN CHECKED BEFORE
            eidnew = ti{1};
        else
            for i = ti{1}
                if i~=eid %ti: cell, stores triangle id
                    eidnew = i;%goes back to previous triangle
                    break
                end
            end
        end
        eid = eidnew; %find int pt agian in the previous triangle
    end
end

