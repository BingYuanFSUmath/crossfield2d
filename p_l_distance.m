function d = p_l_distance(p1,p2,p)
%distance from point p to line p1p2
%line is defined by 2 points p1&p2
%INPUT:
%p1,p2: coordinates of line pts
%p: coordinates of point  
%OUTPUT:
%d: distance from poin to line

x1 = p1(1);
y1 = p1(2);
x2 = p2(1);
y2 = p2(2);
x = p(1);
y = p(2);
d = abs((y2-y1)*x-(x2-x1)*y+x2*y1-y2*x1)/...
    sqrt((y2-y1)^2+(x2-x1)^2);

