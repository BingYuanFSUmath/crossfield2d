 classdef AdaptCurveHermite
    %automatically refined piecewise hermite
    %on a curve to a given tolerance.
    properties
        locations
        child_left
        child_right
    end
    methods
        function self = AdaptCurveHermite(curve,tol,curve_interval)
            %initialize with order and curve
            self.child_left = [];
            self.child_right = [];
            tmin = curve_interval(1);
            tmax = curve_interval(2);
            %both tmin/tmax could be greater than 1, last interval
            self.locations = tmin;
            % see if single interpolant is good enough
            p0 = curve.position_at(mod(tmin,1));
            p1 = curve.position_at(mod(tmax,1));
            p0t = curve.position_at(mod(tmin+1e-3,1));
            p1t = curve.position_at(mod(tmax-1e-3+1,1));
            m0 = p0t-p0;
            m1 = p1-p1t;
            a = norm(p1-p0);
            m0 = a*m0/norm(m0);
            m1 = a*m1/norm(m1);
            e_max = max_hermite_error(curve,[tmin tmax],p0,p1,m0,m1);
            if e_max > tol
                %subdivide the interval
                self.child_left = AdaptCurveHermiteChild;
                self.child_right = AdaptCurveHermiteChild;
                [self.child_left,self.locations] = ...
                    self.child_left.init_child...
                    ([tmin 0.5*(tmin+tmax)],...
                    curve,tol,self.locations);
                [self.child_right,self.locations] = ...
                    self.child_right.init_child...
                    ([0.5*(tmin+tmax) tmax],...
                    curve,tol,self.locations);
            end
            %gather location of subdivisions
            self.locations(end+1) = tmax;
        end
    end
end
            
                
            
        
    
   
