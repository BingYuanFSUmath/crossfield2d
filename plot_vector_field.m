%plot unit vector filed 
%the vector field should be continuous
%using triangulation class
function plot_vector_field(N,S,TR,id)
n = size(N);
x = zeros(n);
y = zeros(n);
d = zeros(n);
for i = 1:size(N,1)
    x(i) = N(i).x;
    y(i) = N(i).y;
    d(i) = N(i).d;
end
sx = zeros(size(S)); %singularity pts coordinates
sy = zeros(size(S));
for i = 1:size(S,2)
    sx(i) = S(i).x;
    sy(i) = S(i).y;
end

figure
triplot(TR,'y')
hold on
scatter(sx,sy,[],'r','filled')
quiver(x,y,cos(d),sin(d),0.3,'b') %plot original cross vectors
hold off
axis equal
title(['Example ',num2str(id)])
end



