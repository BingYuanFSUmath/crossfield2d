function plot_mesh(N,S,SL,e,id)
%plot triangle mesh, crosses, singularities, streamlines and boundary
figure
%plot S with SLs
sx = NaN(size(S)); %singularity pts coordinates
sy = NaN(size(S));
for i = 1:size(S,2)
    %if S(i).k ~= -4 && S(i).k ~= 0
    if S(i).k ~= 0
        sx(i) = S(i).x;
        sy(i) = S(i).y;
    end
end
scatter(sx,sy,[],'r','filled')
hold on
%plot SLs
for i = 1:size(SL,1)
    for j = 1:size(SL,2)
        if SL(i,j).status
            plot(SL(i,j).x(:,1),SL(i,j).x(:,2),'r')
            hold on
        end
    end
end
%plot boundary
for i = 1:size(e,1)
    plot([N(e(i,:)).x]',[N(e(i,:)).y]','b','LineWidth',1.25)
    hold on
end
hold off
axis equal
title(['Example ',num2str(id)])
end