function [p,t] = subdived_t(p,t,S,hmin)
%subdived triangle contains S
%INPUT:
%p,t: points and triangles
%S: inner singularity
%hmin: minimum grid size
%OUTPUT:
%p,t: points and triangles
for sid = 1:size(S,2)
    e = S(sid).E_ID;
    d1 = norm(p(t(e,1))-p(t(e,2)));
    d2 = norm(p(t(e,3))-p(t(e,2)));
    d3 = norm(p(t(e,1))-p(t(e,3)));
    d = max([d1,d2,d3]); %max side length of triangle
    if d >hmin %need subdivision
        n = ceil(d/hmin)+1;
        np = 0.5*(n+1)*(n+2); %num of local pts
        ne = n^2; %num of local triangles
        p_id = zeros(np,1); %record global pt ids
        p0 = zeros(np,2); %record local pt locations
        %e_id = zeros(ne,1); %record local triangle ids
        tnew = zeros(size(t,1)+ne,3); %new t
        p1 = p(t(e,1),:);
        p2 = p(t(e,2),:);
        p3 = p(t(e,3),:);
        %generate new pts
        for i = 1:n+1
            for j = 1:i
                id = 0.5*i*(i-1)+j;%local pt id
                if i == 1
                    p_id(id) = t(e,1);
                    p0(id,:) = p1;
                elseif i == n+1
                    if j == 1
                        p_id(id) = t(e,2);
                        p0(id,:) = p2;
                    elseif j == n+1
                        p_id(id) = t(e,3);
                        p0(id,:) = p3;
                    else %inner pts on last row
                        p_id(id) = size(p,1)+0.5*n*(n+1)+j-2;
                        s = (j-1)/n;
                        p0(id,:) = (1-s)*p2+s*p3;
                    end
                else %inner rows
                    p_id(id) = size(p,1)+0.5*i*(i-1)+j-1;
                    s1 = (i-1)/n;
                    s2 = (j-1)/n;
                    p0(id,:) = (1-s1)*p1+(s1-s2)*p2+s2*p3;
                end
            end
        end
        pnew = zeros(size(p,1)+np-3,2); %new p
        pnew(1:size(p,1),:) = p;
        for id = 1:np
            pnew(p_id(id),:) = p0(id,:);
        end
        p = pnew;
        %generate new triangles
        if 1
        for i = 1:n
            for j = 1:i
                id = 0.5*i*(i-1)+j; %local t id
                e_id = size(t,1)+id; %global t id
                id1 = id; %local pt id
                id2 = 0.5*i*(i+1)+j;
                id3 = id2+1;
                tnew(e_id,:) = [p_id(id1),p_id(id2),p_id(id3)];
            end
        end
        end
        if 1
        for i = n+1:-1:3
            for j = 2:i-1
                id = 0.5*n*(n+1)+0.5*(n+i-2)*(n+1-i)+j-1; %local t id
                e_id = size(t,1)+id; %global t id
                id1 =0.5*i*(i-1)+j; %local pt id
                id2 = 0.5*(i-1)*(i-2)+j-1;
                id3 = id2+1;
                tnew(e_id,:) = [p_id(id1),p_id(id2),p_id(id3)];
            end
        end
        end
        tnew(1:size(t,1),:) = t;
        t = tnew;
        t(e,1) = 0; %mark t need to be removed
    end    
end
t(t(:,1)==0,:)=[];
end
