function plot_Q_order(Q)
figure
for i = 1:size(Q.Q,1)
    plot(Q.P(Q.Q(i,:),1),Q.P(Q.Q(i,:),2));
    hold on
    for j = 1:4
        k = j+2;
        if k>4
            k = k-4;
        end
        text(0.9*Q.P(Q.Q(i,j),1)+0.1*Q.P(Q.Q(i,k),1),...
            0.9*Q.P(Q.Q(i,j),2)+0.1*Q.P(Q.Q(i,k),2),num2str(j));
        hold on
    end
end
hold off