function plot_quad_Q(Quad)
%plot quad by Q
figure
for i = 1:size(Quad.Q,1)
    x = Quad.P(Quad.Q(i,:),:);
    fill(x(:,1),x(:,2),'g')
    hold on
end
hold off
axis equal

%plot quad by QE
figure
for i = 1:size(Quad.QE,1)
    for j = 1:4
        x = Quad.P(Quad.E(Quad.QE(i,j),:),:);
        plot(x(:,1),x(:,2),'b')
        hold on
    end
end
hold off
axis equal