function [Lia,Locb] = find_element(a,b)
%decide whether a is in b or not; if in, return location
%INPUT:
%a: salar or row array
%b: vector or matrix
%OUTPUT:
%Lia: 1 if a is in b, 0 if not
%Locb: position of a in b, row number for matrix b
Tol = 1e-6; %set tolerance when compare elements
Lia = 0;%initialization
Locb = 0;
if size(a,2)==1 && size(b,1)==1
    %a is a scalar and b is a row array
    b = b';%change b to column array
end

for i = 1:size(b,1) %loop through each row
        abst = abs(a-b(i,:));%compare each element in each row
    if abst<[Tol Tol] %all 1's
        Lia = 1;
        Locb = i;%row number
        break
    end
end


        