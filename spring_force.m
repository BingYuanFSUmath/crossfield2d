function Quad = spring_force(Quad,fd,fh,h0,s_bdy,varargin)
% move Quad.P using force equilibrium method

dptol=.001; ttol=.1; Fscale=1.2; deltat=.2; geps=.001*h0; deps=sqrt(eps)*h0;

% 1. Construct p & e 
% p: Nx2 matrix, each row stores point location x,y
% e: Mx2 matrix, each row stores start & end point ids
% k_value: Nx1 matrix, each row stores k-value of S(i)
p = Quad.P;%preallocate p,e
e = Quad.E;
pfix = zeros(size(s_bdy,1),1);
for i = 1:size(s_bdy,1)
    %[~,pfix(i)] = ismember(s_bdy(i,:),p,'rows');
    %error using ismember
    for j = 1:size(p,1)
        if norm(s_bdy(i,:)-p(j,:))<1e-10
            pfix(i) = j;
            break
        end
    end
end
N=size(p,1);                                         % Number of points N
count=0;
pold=inf; 
bars = e; %edge connection is unchanged 

for i = 1:5 
%JUST ONE STEP IS GOOD  ENOUGH
%MORE STEPS CHANGE THE TOPOLOGY
  count=count+1;
  % 3. Retriangulation by the Delaunay algorithm
  if max(sqrt(sum((p-pold).^2,2))/h0)>ttol           % Any large movement?
    pold=p;                                          % Save current positions
    %bars=e;
  end

  % 6. Move mesh points based on bar lengths L and forces F
  barvec=p(bars(:,1),:)-p(bars(:,2),:);              % List of bar vectors
  L=sqrt(sum(barvec.^2,2));                          % L = Bar lengths
  hbars=feval(fh,(p(bars(:,1),:)+p(bars(:,2),:))/2,varargin{:});
  L0=hbars*Fscale*sqrt(sum(L.^2)/sum(hbars.^2));     % L0 = Desired lengths
  F=max(L0-L,0);                                     % Bar forces (scalars)
  Fvec=F./L*[1,1].*barvec;                           % Bar forces (x,y components)
  Ftot=full(sparse(bars(:,[1,1,2,2]),ones(size(F))*[1,2,1,2],[Fvec,-Fvec],N,2));
  Ftot(pfix,:)=0;                          % Force = 0 at fixed points
  p=p+deltat*Ftot;                                   % Update node positions

  % 7. Bring outside points back to the boundary
  d=feval(fd,p,varargin{:}); ix=d>0;                 % Find points outside (d>0)
  dgradx=(feval(fd,[p(ix,1)+deps,p(ix,2)],varargin{:})-d(ix))/deps; % Numerical
  dgrady=(feval(fd,[p(ix,1),p(ix,2)+deps],varargin{:})-d(ix))/deps; %    gradient
  dgrad2=dgradx.^2+dgrady.^2;
  p(ix,:)=p(ix,:)-[d(ix).*dgradx./dgrad2,d(ix).*dgrady./dgrad2];    % Project

  % 8. Termination criterion: All interior nodes move less than dptol (scaled)
  if max(sqrt(sum(deltat*Ftot(d<-geps,:).^2,2))/h0)<dptol, break; end
end
Quad.P = p;
