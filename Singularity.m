%define singularity class using triangulation class
classdef Singularity
    properties
        E_ID %trangle index 
        k %k-value
        x %coordinates
        y
    end
    methods
        function S = Singularity %construct default values
            S.E_ID = 0;
            S.k = 0;
            S.x = 0;
            S.y = 0;
        end
        
        
        function S = findS(S,N,TR) %find singularity points
            E = TR.ConnectivityList; %triangles, num_of_tri*3 matrix
            Ec = zeros(size(E)); %Ec stores ccw triangle vertices
            x = zeros(1,3);
            y = zeros(1,3);
            for i = 1:size(E,1) %loop through all triangles
                for j = 1:3
                    x(j) = N(E(i,j)).x; %vertex coordinates, row arrays
                    y(j) = N(E(i,j)).y;
                end
                [xc,yc] = poly2ccw(x,y);% counterclockwise(ccw), row arrays
                for j = 1:3 
                    index = strmatch([xc(j) yc(j)],[x' y']); %index of vertex in E
                    Ec(i,j) = E(i,index); %sort vetices in triangle ccw
                end
                %It seems that E is already in ccw order
                %E(i,:) %test
                %Ec(i,:) %test
                dtheta12 = dtheta(N,Ec(i,1),Ec(i,2)); %find dtheta 
                dtheta23 = dtheta(N,Ec(i,2),Ec(i,3));
                dtheta31 = dtheta(N,Ec(i,3),Ec(i,1)); %test--ok
                %[dtheta12 dtheta23 dtheta31]--all k=-1
                if dtheta12<0 && dtheta23<0 && dtheta31<0 %find singulairty pts and k
                    S(end+1).E_ID = i; %triangle ID
                    S(end).k = 1; %k-value
                    S(end).x = mean([N(E(i,1)).x N(E(i,2)).x N(E(i,3)).x]);
                    S(end).y = mean([N(E(i,1)).y N(E(i,2)).y N(E(i,3)).y]);
                    %centriod of triangle i, S position
                end
                if dtheta12>0 && dtheta23>0 && dtheta31>0
                    S(end+1).E_ID = i; %triangle ID
                    S(end).k = -1; %k-value
                    S(end).x = mean([N(E(i,1)).x N(E(i,2)).x N(E(i,3)).x]);
                    S(end).y = mean([N(E(i,1)).y N(E(i,2)).y N(E(i,3)).y]);
                end
            end
        end
    end
end