function [Q,pnew,d] = Weighted_Laplacian_smooth(Q,id,fd,ex,beta,s_bdy,poly)
%constrained Weighted Laplacian smooth
%INPUT:
%Q: Quad class
%id: point id
%fd: boundary function
%ex: Example class
%beta: original distortion metric
%s_bdy: fixed pts on bdy
%Q: update hermite if pt is on bdy
%OUTPUT:
%pnew: pt coordinates after smoothing
%d: move distance
p = Q.P(id,:); %record original pt
tol = 1e-5; %tolerance to check a bdy pt
fix = 0; %flag of fixed pt
nei_ids = unique(Q.E(Q.C{id,:},:)); %neighbour pt ids, column vector
quad_ids = neighbour_quads(Q,id); %neighbour quad ids
Qnew = Q; %record Q after movement
for i = 1:size(nei_ids,1)
    if nei_ids(i) == id
        nei_ids(i) = []; %remove itself
        break
    end
end
p_nei = Q.P(nei_ids,:);
if poly
    temp = abs(fd(p,s_bdy));
else
    temp = abs(fd(p));
end
if temp<tol %bdy pt
    for i = 1:size(s_bdy,1)
        if norm(p-s_bdy(i,:))<tol %fixed pt
            pnew = p;
            fix = 1;
            break
        end
    end
    if ~fix %not a fix pt
        for i = 1:ex.curve_num
            t0 = ex.curve(i).find_t(p);
            if t0>=0
                curve_id = i;
                break
            end
        end
        ptemp = zeros(2,2);
        count = 0;
        for i = 1:size(p_nei,1)
            if poly
                temp = abs(fd(p_nei(i,:),s_bdy));
            else
                temp = abs(fd(p_nei(i,:)));
            end
            if temp<tol %neighbour bdy pt
                if ex.curve(curve_id).find_t(p_nei(i,:))>=0
                    %should be on the same curve
                    %may connect to another bdy curve
                    count = count+1;
                    ptemp(count,:) = p_nei(i,:);
                end
            end %should be only two neighbour pts
        end
        t1 = ex.curve(curve_id).find_t(ptemp(1,:));
        t2 = ex.curve(curve_id).find_t(ptemp(2,:));
        tmin = min(t1,t2);
        tmax = max(t1,t2);
        if tmin<t0 && t0<tmax
            dt = tmin+0.5*(tmax-tmin)-t0;
            for loop = 0:5
                tnew = t0+0.5^loop*dt;
                pnew = ex.curve(curve_id).position_at(tnew);
                Qnew.P(id,:) = pnew;
                beta_new = distortion_metric(Qnew.P,Qnew.Q(quad_ids,:));
                acc = acc_criteria(beta(quad_ids),beta_new);
                if acc %acceptable
                    break
                end
            end
            if ~acc %not acceptable after 5 loops
                pnew = p;
            end
        else
            if t0<tmin %0<t0<tmin
                for loop = 0:5
                    tnew = -t0+0.5^loop*(0.5*(1-tmax-tmin)+t0);
                    if tnew<0
                        tnew = -tnew;
                    elseif tnew>0
                        tnew = 1-tnew;
                    end
                    pnew = ex.curve(curve_id).position_at(tnew);
                    Qnew.P(id,:) = pnew;
                    beta_new = distortion_metric(Qnew.P,Qnew.Q(quad_ids,:));
                    acc = acc_criteria(beta(quad_ids),beta_new);
                    if acc %acceptable
                        break
                    end
                end
                if ~acc %not acceptable after 5 loops
                    pnew = p;
                end
            else %tmax<t0<1
                for loop = 0:5
                    tnew = 1-t0+0.5^loop*(0.5*(1-tmax-tmin)-1+t0);
                    if tnew<0
                        tnew = -tnew;
                    elseif tnew>0
                        tnew = 1-tnew;
                    end
                    pnew = ex.curve(curve_id).position_at(tnew);
                    Qnew.P(id,:) = pnew;
                    beta_new = distortion_metric(Qnew.P,Qnew.Q(quad_ids,:));
                    acc = acc_criteria(beta(quad_ids),beta_new);
                    if acc %acceptable
                        break
                    end
                end
                if ~acc %not acceptable after 5 loops
                    pnew = p;
                end
            end
        end
        if acc %moved
            %find tangent vector at each p
            t = tnew+0.01;
            if t>1
                t = t-1;
            end
            v1 = ex.curve(curve_id).position_at(t);
            t = tnew-0.01;
            if t<0
                t = -t;
            end
            v2 = ex.curve(curve_id).position_at(t);
            v=(v1-pnew)+(pnew-v2); %tangent vector
            v = v/norm(v);
            Q.M(id,:) = [1,v]; %only store m for bdy pts
        end
    end
else %not a bdy pt
    n = size(p_nei,1);
    c = zeros(n,1);
    sum_top = 0;
    sum_bot = 0;
    for i = 1:n
        c(i) = norm(p_nei(i,:)-p);
        sum_top = sum_top+c(i)*(p_nei(i,:)-p);
        sum_bot = sum_bot+c(i);
    end
    dp = sum_top/sum_bot;
    for loop = 0:5
        pnew = p+0.5^loop*dp;
        Qnew.P(id,:) = pnew;
        beta_new = distortion_metric(Qnew.P,Qnew.Q(quad_ids,:));
        acc = acc_criteria(beta(quad_ids),beta_new);
        if acc %acceptable
            break
        end
    end
end
d = norm(pnew-p);

