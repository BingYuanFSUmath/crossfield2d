%plot distance filed and cross field
%using triangulation class
function plot_distance_field(N,S,SL,TR)
n = size(N);
x = zeros(n);
y = zeros(n);
d = zeros(n);
theta = zeros(n);
for i = 1:size(N,1)
    x(i) = N(i).x;
    y(i) = N(i).y;
    d(i) = N(i).d;
    theta(i) = N(i).cross;
end
d = real(d);
u1 = cos(theta); %vector represents on direction of a cross
v1 = sin(theta);
u2 = cos(theta+pi/2); 
v2 = sin(theta+pi/2);
u3 = cos(theta+pi); 
v3 = sin(theta+pi);
u4 = cos(theta+1.5*pi); 
v4 = sin(theta+1.5*pi);

sx = zeros(size(S)); %singularity pts coordinates
sy = zeros(size(S));
for i = 1:size(S,2);
    sx(i) = S(i).x;
    sy(i) = S(i).y;
end

%[sx' sy']

figure
triplot(TR,'y')
hold on
if 0
quiver(x,y,u1,v1,0.3,'b') %plot cross field
hold on
quiver(x,y,u2,v2,0.3,'b')
hold on
quiver(x,y,u3,v3,0.3,'b')
hold on
quiver(x,y,u4,v4,0.3,'b')
hold on
end
%scatter(x,y,[],d,'filled') %d represents color,plot distance field
%hold on 
scatter(sx,sy,[],'r','filled')
hold on
sx = zeros(size(S)); %singularity pts coordinates
sy = zeros(size(S));
for i = 1:size(S,2)
    sx(i) = S(i).x;
    sy(i) = S(i).y;
end
scatter(sx,sy,[],'g','filled')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%test of SL starting direction
if 1
hold on
for i = 1:size(SL,1)
    for j = 1:size(SL,2)
        x = [SL(i,j).x];
        v = [SL(i,j).d0];
        if size(x,1)>0
        quiver(x(1,1),x(1,2),v(1),v(2),0.2,'r')
        end
    end
end 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
hold off
axis equal
end

if 0
%figure
quiver(x,y,u1,v1,0.3,'b') %plot original cross vectors
hold off
axis equal
end
end


