function Q = angle_smoothing(Q,fd,ex,s_bdy,poly)
%angle smooth
%INPUT:
%Q: Quad class
%fd: boundary function
%ex: Example class
%s_bdy: fixed pts on bdy
%OUTPUT:
%Q: quad after movement

tol = 1e-5; %tolerance to check a bdy pt
for iter = 1:2 %smooth 2 times
    for id = 1:size(Q.P,1) %loop through each point
        p = Q.P(id,:); %record original pt
        fix = 0; %flag of fixed pt
        if poly
            temp = abs(fd(p,s_bdy));
        else
            temp = abs(fd(p));
        end
        if temp<tol %bdy pt
            for i = 1:size(s_bdy,1)
                if norm(p-s_bdy(i,:))<tol %fixed pt
                    pnew = p;
                    fix = 1;
                    break
                end
            end
            if ~fix %not a fix pt
                nei_ids = unique(Q.E(Q.C{id,:},:)); 
                %neighbour pt ids, column vector
                quad_ids = neighbour_quads(Q,id); %neighbour quad ids
                for i = 1:size(nei_ids,1)
                    if nei_ids(i) == id
                        nei_ids(i) = []; %remove itself
                        break
                    end
                end
                p_nei = Q.P(nei_ids,:);
                for i = 1:ex.curve_num
                    t0 = ex.curve(i).find_t(p);
                    if t0>=0
                        curve_id = i;
                        break
                    end
                end
                ptemp = zeros(2,2);
                count = 0;
                for i = 1:size(p_nei,1)
                    if poly
                        temp = abs(fd(p_nei(i,:),s_bdy));
                    else
                        temp = abs(fd(p_nei(i,:)));
                    end
                    if temp<tol %neighbour bdy pt
                        if ex.curve(curve_id).find_t(p_nei(i,:))>=0
                            %should be on the same curve
                            %may connect to another bdy curve
                            count = count+1;
                            ptemp(count,:) = p_nei(i,:);
                        end
                    end %should be only two neighbour pts
                end
                t1 = ex.curve(curve_id).find_t(ptemp(1,:));
                t2 = ex.curve(curve_id).find_t(ptemp(2,:));
                tmin = min(t1,t2);
                tmax = max(t1,t2);
                Qnew = Q; %copy Q to calculate beta
                acc = 0;
                if tmin<t0 && t0<tmax
                    dt = tmin+0.5*(tmax-tmin)-t0;
                    for loop = 0:4
                        tnew = t0+0.5^loop*dt;
                        pnew = ex.curve(curve_id).position_at(tnew);
                        Qnew.P(id,:) = pnew;
                        beta_new = distortion_metric(Qnew.P,Qnew.Q(quad_ids,:));
                        if min(beta_new) > 0 %no inverted quad
                            %should also consider no convex quad
                            %modify distortion_metric/acc_criteria later
                            acc = 1;%acceptable
                            break
                        end
                    end
                else
                    if t0<tmin %0<t0<tmin
                        for loop = 0:4
                            tnew = -t0+0.5^loop*(0.5*(1-tmax-tmin)+t0);
                            if tnew<0
                                tnew = -tnew;
                            elseif tnew>0
                                tnew = 1-tnew;
                            end
                            pnew = ex.curve(curve_id).position_at(tnew);
                            Qnew.P(id,:) = pnew;
                            beta_new = distortion_metric(Qnew.P,Qnew.Q(quad_ids,:));
                            if min(beta_new) > 0 %no inverted quad
                                acc = 1;%acceptable
                                break
                            end
                        end
                        if ~acc %not acceptable after 5 loops
                            pnew = p;
                        end
                    else %tmax<t0<1
                        for loop = 0:4
                            tnew = 1-t0+0.5^loop*(0.5*(1-tmax-tmin)-1+t0);
                            if tnew<0
                                tnew = -tnew;
                            elseif tnew>0
                                tnew = 1-tnew;
                            end
                            pnew = ex.curve(curve_id).position_at(tnew);
                            Qnew.P(id,:) = pnew;
                            beta_new = distortion_metric(Qnew.P,Qnew.Q(quad_ids,:));
                            if min(beta_new) > 0 %no inverted quad
                                acc = 1;%acceptable
                                break
                            end
                        end
                        if ~acc %not acceptable after 5 loops
                            pnew = p;
                        end
                    end
                end
                if acc %moved
                    %find tangent vector at each p
                    t = tnew+0.01;
                    if t>1
                        t = t-1;
                    end
                    v1 = ex.curve(curve_id).position_at(t);
                    t = tnew-0.01;
                    if t<0
                        t = -t;
                    end
                    v2 = ex.curve(curve_id).position_at(t);
                    v=(v1-pnew)+(pnew-v2); %tangent vector
                    v = v/norm(v);
                    Q.M(id,:) = [1,v]; %only store m for bdy pts
                end
            end
        else %not a bdy pt
            quad_ids = neighbour_quads(Q,id); %neighbour quad ids
            xnew = 0;
            ynew = 0;
            neighbour_pt_in_quads = zeros(size(quad_ids,1),1);
            %record diagonal neighbour points
            for i = 1:size(quad_ids,1) %each quad
                [~,loc] = ismember(id,Q.Q(quad_ids(i),:));
                id1 = loc+1;
                if id1>4
                    id1 = id1-4;
                end
                id1 = Q.Q(quad_ids(i),id1);
                id0 = loc+2;
                if id0>4
                    id0 = id0-4;
                end
                id0 = Q.Q(quad_ids(i),id0);
                neighbour_pt_in_quads(i) = id0;
                id2 = loc+3;
                if id2>4
                    id2 = id2-4;
                end
                id2 = Q.Q(quad_ids(i),id2);
                p0 = Q.P(id0,:);
                p1 = Q.P(id1,:);
                p2 = Q.P(id2,:);
                v0 = p-p0;
                v1 = p1-p0;
                v2 = p2-p0;
                a1 = acos(dot(v0,v1)/(norm(v0)*norm(v1)));
                a2 = acos(dot(v0,v2)/(norm(v0)*norm(v2)));
                b = 0.5*(a2-a1);
                x = p0(1)+(p(1)-p0(1))*cos(b)-(p(2)-p0(2))*sin(b);
                y = p0(2)+(p(1)-p0(1))*sin(b)+(p(2)-p0(2))*cos(b);
                v0 = [x,y]-p0;
                a = acos(dot(v0,v1)/(norm(v0)*norm(v1)));
                if a<min(a1,a2) || a>max(a1,a2) %opposite direction
                    b = -b;
                    x = p0(1)+(p(1)-p0(1))*cos(b)-(p(2)-p0(2))*sin(b);
                    y = p0(2)+(p(1)-p0(1))*sin(b)+(p(2)-p0(2))*cos(b);
                end
                xnew = xnew+x;
                ynew = ynew+y;
                %{
            figure
            pp = [p;p1;p0;p2;p];
            plot(pp(:,1),pp(:,2))
            hold on
            scatter(p(1),p(2),'filled')
            scatter(x,y)
            text(x,y,num2str(b))
            text(p1(1),p1(2),num2str(a1))
            text(p2(1),p2(2),num2str(a2))
            hold off
            axis equal
                %}
            end
            nei_ids = unique(Q.E(Q.C{id,:},:)); 
            %neighbour pt ids, column vector
            for i = 1:size(nei_ids,1)
                if nei_ids(i) == id
                    nei_ids(i) = []; %remove itself
                    break
                end
            end
            for i = 1:size(nei_ids,1)
                nei_ids2 = unique(Q.E(Q.C{nei_ids(i),:},:)); 
                %neighbour pt ids, column vector
                p_id = zeros(2,2);
                id_count = 0;
                for j = 1:size(nei_ids2,1)
                    if ismember(nei_ids2(j),neighbour_pt_in_quads)
                        id_count = id_count+1;
                        p_id(id_count,:) = Q.P(nei_ids2(j),:); 
                        %find neighbour diagonal points
                        if id_count == 2
                            break
                        end
                    end
                end
                p0 = Q.P(nei_ids(i),:);
                p1 = p_id(1,:);
                p2 = p_id(2,:);
                v0 = p-p0;
                v1 = p1-p0;
                v2 = p2-p0;
                a1 = acos(dot(v0,v1)/(norm(v0)*norm(v1)));
                a2 = acos(dot(v0,v2)/(norm(v0)*norm(v2)));
                b = 0.5*(a2-a1);
                x = p0(1)+(p(1)-p0(1))*cos(b)-(p(2)-p0(2))*sin(b);
                y = p0(2)+(p(1)-p0(1))*sin(b)+(p(2)-p0(2))*cos(b);
                v0 = [x,y]-p0;
                a = acos(dot(v0,v1)/(norm(v0)*norm(v1)));
                if a<min(a1,a2) || a>max(a1,a2) %opposite direction
                    b = -b;
                    x = p0(1)+(p(1)-p0(1))*cos(b)-(p(2)-p0(2))*sin(b);
                    y = p0(2)+(p(1)-p0(1))*sin(b)+(p(2)-p0(2))*cos(b);
                end
                xnew = xnew+x;
                ynew = ynew+y;
            end
            pnew = 1/(size(quad_ids,1)+size(nei_ids,1))*[xnew,ynew];
        end
        Q.P(id,:) = pnew;
    end
end
