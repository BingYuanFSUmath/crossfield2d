function x = SL_interp2(SL1,SL2)
%interpolate 2 streamlines, SL1 duplicate SL2 direction
%INPUT:
%SL1: streamline end with S
%SL2: streamline end with bdy or spiral, accurate at S
%OUTPUT:
%x: nx2 matrix to store interpolation points locations
n1 = size(SL1.x,1); %number of pts on SL1
n2 = size(SL2.x,1); %number of pts on SL2
n = 5; %number of interpolation pts
if (n1<n) || (n2<n)
    n = min(n1,n2);
end
x = zeros(n,2); %initialization
SL11 = SL1; %copy SL1
SL22 = SL2; %copy SL2
SL11.x = SL1.x(end-n+1:end,:);
SL22.x = SL2.x(1:n,:);
for i = 1:n
    s = (i-1)/(n-1); %parameter
    x(i,:) = SL_fun(SL11,s)*(1-s)+SL_fun(SL22,1-s)*s;
end
x = [SL1.x(1:end-n,:);x];