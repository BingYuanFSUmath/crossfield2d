function f = dirac2d(x,a,d0)
%diract function in 2d
%Input
%x: coordinates of evaluation point [x y]
%a: Dirac delta function at point a
%d0: grid size decides epsilon of jump function
f = 0;
if norm(x-a)<d0
    f = 1/(pi*d0^2);
end
end