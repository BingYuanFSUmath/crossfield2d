function theta3 = compute_cross(N,N1,N2,N3)
x3 = N(N1).x; %coordinates of 3 nodes
y3 = N(N1).y;
x1 = N(N2).x;
y1 = N(N2).y;
x2 = N(N3).x;
y2 = N(N3).y;
theta1 = N(N2).cross; %theta of N2,N3
theta2 = N(N3).cross;
M = [x1 x2 x3;y1 y2 y3; 1 1 1]; %the coefficient matrix
M = inv(M); % inverse of the coef matrix
a = M(:,1); %first column
b = M(:,2); %second column
%x = [x1 x2 x3]; %x/y coordinates of triangle vertices
%y = [y1 y2 y3];
%A = ploarea(x,y); %area of the triangle
%%%%%%%%do not need A right now%%%%%%%%
theta3 = -(theta1*(a(1)*a(3)+b(1)*b(3))+theta2*(a(2)*a(3)+b(2)*b(3)))...
         /(a(3)^2+b(3)^2);
%theta3 = mod(theta3,pi/2);
end