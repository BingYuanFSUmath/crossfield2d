function plot_quad_E_x(Quad)
%plot quad by E_x
figure
text(Quad.P(:,1),Quad.P(:,2),num2str((1:size(Quad.P,1))'))
hold on
for i = 1:size(Quad.E_x,1)
    x = Quad.E_x{i};
    plot(x(:,1),x(:,2),'b')
    hold on
end
hold off
axis equal
