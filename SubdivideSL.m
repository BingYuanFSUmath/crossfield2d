%subdivide SLs according to order of polynomial and tolerance
%generate Quad class and add boundary P,E
function Quad = SubdivideSL(SL,Inter_p,S0,ex,N_poly,Tol_poly,p)
%OUTPUT:
%SLcopy_new: SLs after subdividion
%Quad: Quad class

Quad = QuadClass; %initialize
%find all boundary points
[SL,p_bdy] = exact_boundary_points(SL,S0,p,ex);
%{
figure
triplot(TR)
axis equal
hold on
for i = 1:size(p_bdy,2)
scatter(p_bdy(i).x(1),p_bdy(i).x(2),[],'r','filled')
text(p_bdy(i).x(1),p_bdy(i).x(2),...
    ['\leftarrow' num2str(p_bdy(i).curve_id)])
hold on
end
hold off
%}
%load ginger_exact.mat
%generate boundary SLs, SL_bdy store p_bdy ids, ccw
SL_bdy = boundarySL(p_bdy,ex); %curve_num x 1 cell

%subdivide each boundary streamline
N_pts = N_poly+1; %num of interpolant points
locations = cell(ex.curve_num,1); %store subdivide locations
for i = 1:ex.curve_num
    %can choose subdivide which curves
    locations{i} = cell(size(SL_bdy{i},1),1); %double layer cell
    for j = 1:size(SL_bdy{i},1)
        list = SL_bdy{i};
        tmin = p_bdy(list(j,1)).t;
        tmax = p_bdy(list(j,2)).t;
        if tmax<tmin %last interval, loop back
            tmax = tmax+1;
        end
        curve = ex.curve(i);
        curve_interp = AdaptCurveInterp(N_pts,curve,Tol_poly,[tmin tmax]);
        locations{i}{j} = mod(curve_interp.locations,1);
        %avoid t_end>1
    end
end

%add bdy pts to Quad(subdivision pts not included)
for i = 1:ex.curve_num
    n = size(locations{i},1); %num of pts on closed curve
    P = zeros(n,2); %store pt location
    for j = 1:n
        P(j,:) = ex.curve(i).position_at(locations{i}{j}(1));
    end
    Quad.P = [Quad.P;P];
    E = zeros(n,2); %a bdy curve is always closed
    E_x = cell(n,1);
    for j = 1:n
        j1 = j;
        j2 = j+1;
        if j == n
            j2 = 1;
        end
        E(j,:) = [j1,j2];
        %find E_x
        %accurate,but can do it after subdivision
        %only need a vector to find angle
        E_x{j} = zeros(4,2);%p0,p0+d,p1-d,p1
        E_x{j}(1,:) = P(E(j,1),:); %start pt
        E_x{j}(4,:) = P(E(j,2),:); %end pt
        tmin = locations{i}{j}(1);
        tmax = locations{i}{j}(end);
        if tmax<tmin %tmax=0, end pt
            tmax = tmax+1;
        end
        dt = min(1e-3,(tmax-tmin)/3);
        E_x{j}(2,:) = ex.curve(i).position_at(tmin+dt);
        E_x{j}(3,:) = ex.curve(i).position_at(tmax-dt);
    end
    m = size(Quad.E,1);
    Quad.E = [Quad.E;E+m];
    Quad.E_x = [Quad.E_x;E_x];
end

%reshape SLcopy
SL = reshape(SL,[size(SL,1)*size(SL,2),1]);
SL([SL.status] == 0) = [];

%generate Quad class
Quad = Quad.generator(SL,Inter_p);

%adjust invalid quads
Quad = Invalid_adjust(Quad,ex);

%propagate new points 
Quad = propagate_bdy_Q(Quad,locations,ex,N_poly);

end




