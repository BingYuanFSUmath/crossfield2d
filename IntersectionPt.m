classdef IntersectionPt
    %find intersection pt on SLs
    properties
        locations %Intersection pt locations
        SL_ids %ids of intersect SLs
        Child %2x2 child box
    end
    methods
        function self = IntersectionPt(SL,d0,fh,box)
            %initialize
            self.locations = [];
            self.SL_ids = [];
            self.Child = [];
            x_min = box(1,1);
            x_max = box(2,1);
            y_min = box(1,2);
            y_max = box(2,2);
            %count number of SLs in each box
            count = size(SL,1); %SL does not include bdy_SLs
            if count >= 2 %two or more inner SLs
                %subdivide the box
                self.Child = IntersectionPtChild; 
                %or self.Child(2,2) will be recognized as double
                self.Child(2,2) = IntersectionPtChild; %2x2
                for i = 1:2
                    for j = 1:2
                        newbox = zeros(2,2);
                        hx = 0.5*(x_max-x_min);
                        hy = 0.5*(y_max-y_min);
                        newbox(1,1) = x_min+hx*(i-1);
                        newbox(2,1) = newbox(1,1)+hx;
                        newbox(1,2) = y_min+hy*(j-1);
                        newbox(2,2) = newbox(1,2)+hy;
                        [self.Child(i,j),self.locations,self.SL_ids]...
                            = self.Child(i,j).init_Child...
                            (d0,fh,newbox,SL,self.locations,self.SL_ids);
                    end
                end
            end
        end  
    end
end