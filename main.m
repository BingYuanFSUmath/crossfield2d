%CrossField 2D quad mesh generator
clc
clearvars

for exampleID = 39 %number of examples
   disp(exampleID)
%
%1. Set parameters
r = 0.5; %interpolation ratio of acute angle
N_poly = 5; %order of interpolation polynomials
Tol_poly = 1e-5; %tolerance of interpolation error

%2. Generate trimesh data
[s_bdy,p,t,e,ex] = trimesh_generator(exampleID);
%s_bdy: Boundary singularity points(pts where bdy is not continuous)
%p: points coordinates
%t: triangle vertex pt ids
%e: boundary edges
d0 = ex.d0; %min grid size
fd = ex.fd; %distance function, -:inside, +:outside
fh = ex.fh; %scaled edge length function
%1:Square
%2:Concave rectangles
%3:Convex polygon(d=0.05)
%4:Concave polygon
%5:Jagged mesh
%6:Equilateral Triangle
%7:Right Triangle
%8:Obtuse Triangle(r=0.4,d0=0.05)
%9:Half circle
%10:Circle
%11:Ellipse
%12:Rectangle with hole
%13:Square in square
%perfect after using adaptive mesh
%14:Two circles inside rectangle
%15:Three circles inside rectangle
%16:Airfoil(NACA0012) %d0 is hmax
%17:example 6A(half circle+rectangle)
%18:example 6B(half circle+rectangle)
%19:Concave rectangles compare with quad tree
%20:Three circles inside rectangle with various r
%21:Three circles inside ellipse with various r
%22:Rectangle with half hole
%23:Four circles inside ellipse with various r
%24:Square minus quarter circle
%25:Four circles inside circle r=0.5
%26:Four circles inside circle r=0.3
%27:Example in Nicolas K
%28:Example in Fogg 12
%29:Example in Fogg 19, spiral
%30:Example in Bunin 6 (same grid size)
%31:Example in Bunin 6 (coerse grid size)
%32:Example in Bunin 7
%33:Example in Bunin 8
%34:Rectangle with hole with non-uniform h
%35:Three element airfoil
%36:Gingerman
%37:NACA0012 modify
%38:rotated half circle
%39:Indian ocean
    
%3. Initialisation
TR = triangulation(t,p);%generate triangulation class
%triplot(TR)
[N,S0,SL0,S0_id] = initialize_N(s_bdy,p,t,e,TR);
if size(S0_id,1)%exists boundary singularity S0
    N = acute(N,e,S0_id,r);%modify acute angles
end

%4. Solve cross field using FEM
U = fem(p,t,e,N);%complex solution, represent vector(angle does not work)
U = atan2(imag(U),real(U));%convert to angle [-pi,pi]
for i = 1:size(N,1)
    N(i).cross = U(i)/4;%cross field
    %can not use mod(U(i),2*pi)/4
    %theta0iter need continuously changed crosses
    %theta0iter can be modified later
    N(i).d = mod(U(i),2*pi);%[0 2pi]
end

%5. Find singularity points
S = Singularity;
S = find_S_new(S,N,TR);
S = S(1,2:end); %remove the initial zero
S = collide_S(S,d0,fh);
plot_vector_field(N,S,TR,exampleID)
%
%subdivide elements contain S
%[p,t] = subdived_t(p,t,S,hmin);
%S = subdived_t_new(p,t,S,N,hmin);
%plot_vector_field(N,S,TR,exampleID)
%
%6. Initialize streamlines
SL = Streamline;
if size(S,2) %initialize SL when S exists
    SL = SL.initializeSL(S,TR,N);%find starting directions
end
Scopy = [S0 S]; %combine bdy and inside singularities
%combine SLs
SLcopy(size(SL0,1)+size(SL,1),max(size(SL0,2),size(SL,2))) = Streamline;
SLcopy(1:size(SL0,1),1:size(SL0,2)) = SL0;
SLcopy(size(SL0,1)+1:end,1:size(SL,2)) = SL;
%
%7. Propagate streamlines
d0 = 2*d0;
N_S0 = size(S0,2); %number of bounday singularities
for i = 1:size(SLcopy,1) %propagate each streamline
    for j = 1:size(SLcopy,2) %column num i is Sid
        if SLcopy(i,j).status%only propagate exist SL
            SLcopy(i,j) = propagateSL(SLcopy(i,j),Scopy,TR,N,i,e,d0,fh,N_S0,SLcopy);
        end
    end
end
%plot_mesh(N,S,SLcopy,e,exampleID)

%8. Cleanups 
%8.1 Smooth SLs
%smooth first to avoid great direction change on a SL
for i = 1:5 %smooth N times
    SLcopy = SmoothSL(SLcopy,Scopy,fd,fh,d0);
end
%plot_mesh(N,S,SLcopy,e,exampleID)

%8.2 Remove duplicated SLs
SLcopy = RemoveSL_new(SLcopy,Scopy,TR,N,e,d0,fh,N_S0);
plot_mesh(N,Scopy,SLcopy,e,exampleID)
%

%9. Subdivide SLs
%filename = [num2str(exampleID),'_before_subdivision.mat'];
%load(filename)
%{
%9.1 Find inner intersection pts
SLnew = reshape(SLcopy,[size(SLcopy,1)*size(SLcopy,2),1]);
SLnew([SLnew.status] == 0) = [];
Inter_p = IntersectionPt(SLnew,d0,fh,ex.box);
%{
%test plot
if size(Inter_p.locations,1)>0 %exist intersection pts
    Stest(1,size(Inter_p.locations,1)) = Singularity;
    for i = 1:size(Inter_p.locations,1)
        Stest(i).x = Inter_p.locations(i,1);
        Stest(i).y = Inter_p.locations(i,2);
        Stest(i).k = -1;
    end
    plot_mesh(N,Stest,SLnew,e,exampleID)
end
%}
%9.2 Subdivide streamlines and generate quad class
N_poly = 14; Tol_poly = 1e-3;
Quad = SubdivideSL(SLcopy,Inter_p,S0,ex,N_poly,Tol_poly,p);
%Quad.plot_quad(N,e,exampleID);
%plot_quad_E_x(Quad);
plot_mesh_Q(S,Quad,ex)
%ccw_check(Quad)
%}

%{  
filename = [num2str(exampleID),'_after_subdivision.mat'];
load(filename)
plot_mesh(N,S,SLnew,e,exampleID)
%}

%{
filename = [num2str(exampleID),'_before_smoothing.mat'];
load(filename)
plot_mesh_Q(S,Quad,ex)
%plot_mesh(N,S,SLnew,e,exampleID)
%}

%Qtest = Subdivide_smooth_new(Quad,3,ex,s_bdy);

clearvars -except exampleID
end %exampleID
%{
%%%%%%%%%%%%smooth test%%%%%%%%%%%%
Q = smoothing(Quad,d0,fd,ex,s_bdy,poly);
Q.plot_quad(N,e);
Q.hermite(fd,e,N,poly,s_bdy);
%Q = angle_smoothing(Quad,fd,ex,s_bdy,poly);
%Q.plot_quad(N,e);
%Q.hermite(fd,e,N,poly,s_bdy);
%Jacobian(Q,1);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%}



