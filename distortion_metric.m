function beta = distortion_metric(p,q)
%find distortion metric beta for each element
%INPUT:
%p: point coordinates, nx2
%q: quad elements, store 4 pt ids, mx4
%OUTPUT:
%beta: distortion metric of each quad element
%beta<0: inverted/convex  beta=1:squre
beta = zeros(size(q,1),1);
for i = 1:size(q,1) %loop through each element
    v = zeros(4,2); %vertex coordinates
    e = zeros(3,2); %edge vector of each subtriangle
    alpha = zeros(4,1); %alphas of subtriangles
    for j = 1:4
        v(j,:) = p(q(i,j),:);
    end
    for j = 1:4 %loop through each subtriangle
        t1 = j;
        t2 = j+1;
        t3 = j+2;
        if t2>4
            t2 = t2-4;
        end
        if t3>4
            t3 = t3-4;
        end
        t = [t1,t2,t3]; %subtriangle
        for k = 1:3
            t2 = k+1;
            if t2>3
                t2 = t2-3;
            end
            e(k,:) = v(t(t2),:)-v(t(k),:);
        end
        value = 2*sqrt(3)*cross([e(1,:),0],[e(2,:),0])/...
            (norm(e(1,:))^2+norm(e(2,:))^2+norm(e(3,:))^2);
        alpha(j) = value(3);
    end
    alpha = sort(alpha);
    %for each triangle
    %ccw: cross>0(++++), cw:cross<0(----)
    %convex element: -+++
    %inverted element: --++
    if alpha(1)<=0 && alpha(4)>0 %inverted or concave element
        beta(i) = -1;
    elseif alpha(1)<0 && alpha(4)<0 %cw
        beta(i) = alpha(3)*alpha(4)/(alpha(1)*alpha(2));
    else
        beta(i) = alpha(1)*alpha(2)/(alpha(3)*alpha(4));
    end
end
        
        
        