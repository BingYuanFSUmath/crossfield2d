function quad_ids = neighbour_quads(Q,id)
%find neighbour quads given p_id
%INPUT:
%Q: Quad class
%id: pt id
%OUTPUT:
%quad_ids: neighbour quad ids, column vector

quad_ids = [];
q = Q.Q;
for i = 1:size(q,1)
    for j = 1:4
        if q(i,j) == id %p on element Q_i
            quad_ids = [quad_ids;i];
        end
    end
end
