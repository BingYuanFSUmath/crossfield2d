function A = unique_row(A,tol)
%find unique rows in matrix within tol
%INPUT:
%A: matrix
%tol: tolerance
%OUTPUT:
%A: unique matrix by rows

for i = 1:size(A,1)
    r_size = size(A,1); %new row number
    flag = ones(r_size,1); %record removed element
    if i == r_size
        break
    end
    for j = i+1:r_size
        if norm(A(i,:)-A(j,:))<tol
            flag(j) = 0;
        end
    end
    A(flag==0,:)=[];
end
