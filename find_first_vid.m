function [x,y,vid1,vid2] = find_first_vid(S,eid,d,TR)
%find vids of first propagating point in a SL

vid = TR.ConnectivityList(eid,:); %vertex IDs
x0 = S.x; %coordinates of S
y0 = S.y;
error = 1e-10;%error when check whether 2 value equals or not
%1e-14 is too small if x is close to x0

if 0
    %test routine
    %plot triangle
    figure
    plot(TR.Points(vid,1),TR.Points(vid,2))
    hold on
    %plot singularity point
    scatter(x0,y0,[],'r','filled')
    hold on
    %plot d
    quiver(x0,y0,d(1),d(2),0.3,'b')
    hold on
end

for i = 1:3 %vertex order
    if i == 3
        j = 1; %31
    else
        j = i+1; %12/23
    end
    x1 = TR.Points(vid(i),1); %coordinates of v1,v2
    y1 = TR.Points(vid(i),2);
    x2 = TR.Points(vid(j),1);
    y2 = TR.Points(vid(j),2);
    if abs(d(1))>error %d is not vertival, avoid cancellation in x
        k0 = d(2)/d(1); %slope of line passes S, with direction d
        if abs(x1-x2)<error %x1-x2 is a vertical line
            x = x1; %intersection point
            y = k0*(x-x0)+y0;
        else
            k1 = (y1-y2)/(x1-x2); %slope of line passes v1,v2
            if abs(k0-k1)<error %d parallel to one edge
                continue %continue next loop
            end
            x = (y1-y0-k1*x1+k0*x0)/(k0-k1); %coordinates of intersection point
            y = k0*(x-x0)+y0;
        end
    else %d is vertical, use x-x0=k(y-y0)
        k0 = d(1)/d(2); %inverse slope of line passes S, with direction d
        if abs(y1-y2)<error %horizontal line
            y = y1; %intersection point
            x = k0*(y-y0)+x0;
        else
            k1 = (x1-x2)/(y1-y2); %inverse slope of line passes v1,v2
            if abs(k0-k1)<error %d parallel to one edge
                continue %continue next loop
            end
            y = (x1-x0-k1*y1+k0*y0)/(k0-k1); %coordinates of intersection point
            x = k0*(y-y0)+x0;
        end
    end
    
    if 0
        %test routine
        %plot intersection point
        scatter(x,y,[],'b','filled')
        hold on
    end
    
    if (min([x1 x2])-error<=x && x<=max([x1 x2])+error)...
            &&(min([y1 y2])-error<=y && y<=max([y1 y2])+error)... %need BOTH
            && dot(d,[x-x0 y-y0])>error %remove duplicated points
        %intersection point on the edge
        %intersection direction is the same as d
        vid1 = vid(i);
        vid2 = vid(j);
        break
    end
end

end