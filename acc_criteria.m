function acc = acc_criteria(u0,u)
%wheather move is acceptable or not
%INPUT:
%u0: distortion metric before move
%u: distortion metric after move
%OUTPUT:
%acc: acceptability
acc = 0;
N = size(u,1); %number of quads
%delta_u = sum(u-u0)/N;
%umin = 0.05;
N1 = 0; %num of quads improved
%N11 = 0; %significant improvement
N0 = 0; %num of quads get worse
%N00 = 0; %significant worse
if min(u) > 0 %no inverted quad
    for i = 1:N
        if u(i)-u0(i)>0
            N1 = N1+1;
        elseif u(i)-u0(i)<0
            N0 = N0+1;
        end
        %{
    if (u0(i)<0 && u(i)>=0)||(u0(i)<umin && u(i)>=umin)
        N11 = N11+1;
    end
    if (u0(i)>0 && u(i)<=0)||(u(i)<=umin)
        N00 = N00+1;
    end
        %}
    end
    %N,N1,N11,N0,N00
    %if (N0==N)||(N00>N11)||(delta_u<-umin)
    %    acc = 0;
    %elseif (N1==N)||(N11>0 && N00==0)||(N11>N00 && delta_u>-umin)
    %    acc = 1;
    %end
    if (N1>=N0) %more pts improved than get worse
        acc = 1;
    end
end
