function y = theta(theta1,theta2)
y = 0.5*(1-cos(4*(theta1-theta2)));
end