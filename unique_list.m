function list = unique_list(list,tol)
%find unique elements in list within tol
%INPUT:
%list: vector
%tol: tolerance
%OUTPUT:
%list: unique list
row = size(list,1);
col = size(list,2);
if row<col %row vector
    list = list';
end
list_size = size(list,1); %list size
for i = 1:list_size
    l_size = size(list,1); %new list size
    flag = ones(l_size,1); %record removed element
    if i == l_size
        break
    end
    for j = i+1:l_size
        if abs(list(i)-list(j))<tol
            flag(j) = 0;
        end
    end
    list(flag==0)=[];
end
if row<col %row vector
    list = list';
end