classdef AdaptCurveHermiteChild
    properties
        child_left
        child_right
    end
    methods
        function self = AdaptCurveHermiteChild
            self.child_left = [];
            self.child_right = [];
        end
        function [self,locations] = init_child(self,...
                interval,curve,tol,locations)
            if interval(1) > locations(end)
                locations(end+1) = interval(1);
            end
            %see if single interpolant is good enough
            tmin = interval(1);
            tmax = interval(2);
            p0 = curve.position_at(mod(tmin,1));
            p1 = curve.position_at(mod(tmax,1));
            p0t = curve.position_at(mod(tmin+1e-3,1));
            p1t = curve.position_at(mod(tmax-1e-3+1,1));
            m0 = p0t-p0;
            m1 = p1-p1t;
            a = norm(p1-p0);
            m0 = a*m0/norm(m0);
            m1 = a*m1/norm(m1);
            e_max = max_hermite_error(curve,[tmin tmax],p0,p1,m0,m1);
            if e_max > tol
                %subdivide the interval
                new_interval = zeros(2,1);
                new_interval(1) = interval(1);
                new_interval(2) = interval(1)+0.5*...
                    (interval(2)-interval(1));
                self.child_left = AdaptCurveHermiteChild;
                [self.child_left,locations] = ...
                    self.child_left.init_child...
                    (new_interval,curve,tol,locations);
                new_interval(1) = new_interval(2);
                new_interval(2) = interval(2);
                self.child_right = AdaptCurveHermiteChild;
                [self.child_right,locations] = ...
                    self.child_right.init_child...
                    (new_interval,curve,tol,locations);
            end
        end
    end
end