%find end point on a boundary
function endpoint = boundarypoint(x0,y0,vedge,N)
%endpoint should be a 1x2 array
%vedge:edge vertex ids
x1 = N(vedge(1)).x; %coordinates of v1,v2
y1 = N(vedge(1)).y;
x2 = N(vedge(2)).x;
y2 = N(vedge(2)).y;
if x1 == x2 %vertical boundary
    x = x1;
    y = y0;
else
    k1 = (y1-y2)/(x1-x2); %slope of boundary edge
    if k1 == 0 %y1=y2, horizontal boundary
        x = x0;
        y = y1;
    else
        k0 = -1/k1; %slope orthogonal to boundary
        x = (y1-y0-k1*x1+k0*x0)/(k0-k1);
        y = k0*(x-x0)+y0;
    end
end
endpoint = [x y];
%%%%%%%%%%%%%%%%%%%%%%%%
%endpoint could be outside of triangle!
%%%%%%%%%%%%%%%%%%%%%%%%
if 0
% This is a comprpmise:keep original intersection pt
% should not happen, bad topology
if x<x1 || x>x2 %orth intersection pt is not on the egde
    x = x0;
    y = y0;
end
end
end