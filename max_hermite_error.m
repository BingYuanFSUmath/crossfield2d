function error = max_hermite_error(curve,curve_interval,p0,p1,m0,m1)
%find max hermite error using linear search
%INPUT:
%curve: curve class
%curve_interval: [tmin,tmax], tmax could be >1
%OUTPUT:
%error: max interpolation error
m = 20; %divide the curve to 20 pieces
xcurve = zeros(m+1,2); %record sample pts on curve
dt = (curve_interval(2)-curve_interval(1))/m;
t = 0:0.01:1; %101 sample pts on hermite 
t = t'; %column vector
x = (2*t.^3-3*t.^2+1)*p0...
    +(t.^3-2*t.^2+t)*m0...
    +(-2*t.^3+3*t.^2)*p1...
    +(t.^3-t.^2)*m1; %pts on hermite, nx2
error = 0;
for j = 1:m+1
    t = curve_interval(1)+(j-1)*dt;
    t = mod(t,1);
    xcurve(j,:) = curve.position_at(t);
    distVector = sqrt((x(:,1)-xcurve(j,1)).^2+(x(:,2)-xcurve(j,2)).^2);
    e = min(distVector);
    error = max(error,e);
end
%{
figure
plot(x(:,1),x(:,2),'-r')
hold on
plot(xcurve(:,1),xcurve(:,2),'-k')
text(x(1,1),x(1,2),num2str(error))
hold off
axis equal
%}
end
