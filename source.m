function f = source(p,S,d0)
%Evaluate source term at x
%Input
%p: coordinates of evaluation point [x y]
%S: Calculated singularities
f = 0;
for i = 1:size(S,2)
    f = f+S(i).k*dirac2d(p,[S(i).x S(i).y],d0);
end
f = pi/2*f;
end