%plot vector function value
%the vector field should be continuous
%using triangulation class
function plot_vector_function(N,S,TR)
n = size(N);
x = zeros(n);
y = zeros(n);
d = zeros(n);
for i = 1:size(N,1)
    x(i) = N(i).x;
    y(i) = N(i).y;
    d(i) = N(i).cross;
end
sx = zeros(size(S)); %singularity pts coordinates
sy = zeros(size(S));
for i = 1:size(S,2)
    sx(i) = S(i).x;
    sy(i) = S(i).y;
end

figure
triplot(TR,'y')
hold on
scatter(sx,sy,[],'r','filled')
hold on
pointsize = 50;
scatter(x,y,pointsize,d,'filled');
hold off
axis equal
end



