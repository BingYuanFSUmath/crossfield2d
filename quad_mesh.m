function [P,T,SLnew] = quad_mesh(s_bdy,SL,e,p)
%generate quad mesh using SLs
%INPUT:
%s_bdy: fix points
%SL: existing streamlines
%e: boundary edges
%S: existing singularity points
%OUTPUT:
%P: points of quad mesh, Nx2
%T: edge connectivity of quad mesh, Mx(N_sample+1)

%%%%%%%%%%%%%%%%%%%example of half circle%%%%%%%%%%%%%%%%%%%%%
P = zeros(32,2);
E = zeros(11,4);
E_count = 0;
P(1:size(s_bdy,1)-1,:) = s_bdy(1:end-1,:); %fixed points
P_count = size(s_bdy,1)-1;
P_end = 1:1:size(s_bdy,1)-1;%all end points of streamlines
for i = 1:size(SL,1)
    for j = 1:size(SL,2)
        if SL(i,j).status
            E_count = E_count+1; %a SL represent an edge
            x = SL(i,j).x(1,:); %starting point
            [P,P_id,P_count] = dup_check(P,x,P_count);
            E(E_count,1) = P_id;
            P_end(end+1) = P_id;
            num = size(SL(i,j).x,1);%number of points on a streamline
            t = floor(num/3.0);
            x = SL(i,j).x(1+t,:);
            [P,P_id,P_count] = dup_check(P,x,P_count);
            E(E_count,2) = P_id;
            x = SL(i,j).x(1+2*t,:);
            [P,P_id,P_count] = dup_check(P,x,P_count);
            E(E_count,3) = P_id;
            x = SL(i,j).x(end,:);
            [P,P_id,P_count] = dup_check(P,x,P_count);
            E(E_count,4) = P_id;
            P_end(end+1) = P_id;
        end
    end
end
P_end = unique(P_end);
status = ones(size(p,1),1); %record checked bdy pts 
SLnew(size(P_end,2),2) = Streamline;
for i = 1:size(P_end,2)
    [Lia,Locb] = ismember(P(P_end(i),:),p(unique(e),:),'rows');
    if Lia %end point is on boundary
        for k = 1:2
        SLnew(i,k).status = 1;
        SLnew(i,k).x = p(Locb,:);
        for j = 1:size(e,1)
            if Locb == e(j,1) && status(e(j,2))
                P_id = e(j,2);
                status(e(j,2)) = 0;
                break
            elseif Locb == e(j,2) && status(e(j,1))
                P_id = e(j,1);
                status(e(j,1)) = 0;
                break
            end
        end
        SLnew(i,k).x(end+1,:) = p(P_id,:);
        while ~ismember(P_id,P_end)%not reach another end point
            for j = 1:size(e,1)
                if P_id == e(j,1) && status(e(j,2)) 
                    P_id = e(j,2);
                    status(e(j,2)) = 0;
                    break
                elseif P_id == e(j,2) && status(e(j,1))
                    P_id = e(j,1);
                    status(e(j,1)) = 0;
                    break
                end
            end
            SLnew(i,k).x(end+1,:) = p(P_id,:);
        end
        end
    end
end
            
   T=1;         
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            