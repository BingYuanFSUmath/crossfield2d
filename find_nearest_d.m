function dnew = find_nearest_d(x,y,d,vid1,vid2,N)
%find nearest directions of 2 crosses, then interpolate the directions
%INPUTS:
%x,y: point coordinates
%d: incoming direction
%vid1,vid2: point index
%N: node class
%OUTPUT:
%dnew: interpolating direction

%find nearest cross direction of v1
theta = ones(4,1)*pi; %initialize dtheta
dx = zeros(1,4);
dy = zeros(1,4);
for i = 1:4 %four cross vectors of v1
    dx(i) = cos(N(vid1).cross+(i-1)*pi/2);
    dy(i) = sin(N(vid1).cross+(i-1)*pi/2);
    costheta = dot(d,[dx(i) dy(i)]);
    if costheta>0 %acute angle
        theta(i) = acos(costheta); %theta in [0,pi]
    end
end %find four dtheta
[~,index] = min(abs(theta)); %find index of min dtheta
v1d = [dx(index) dy(index)]; %nearest direction at v1
%find nearest cross direction of v2
theta = ones(4,1)*pi; %initialize dtheta
dx = zeros(1,4);
dy = zeros(1,4);
for i = 1:4 %four cross vectors of v2
    dx(i) = cos(N(vid2).cross+(i-1)*pi/2);
    dy(i) = sin(N(vid2).cross+(i-1)*pi/2);
    costheta = dot(d,[dx(i) dy(i)]);
    if costheta>0 %acute angle
        theta(i) = acos(costheta); %theta in [0,pi]
    end
end %find four dtheta
[~,index] = min(abs(theta)); %find index of min dtheta
v2d = [dx(index) dy(index)]; %nearest direction at v2
%interpolates v1d,v2d
x1 = N(vid1).x; %coordinate of v1
y1 = N(vid1).y;
x2 = N(vid2).x; %coordinate of v2
y2 = N(vid2).y;
if abs(x1-x2)>1e-10
    t = (x-x1)/(x2-x1); %parameter
else
    t = (y-y1)/(y2-y1);
end
%!!!!!!!(1-t) is the ratio when x is close to x1!!!!!! 
dnew = (1-t)*v1d+t*v2d; %interpolation directions between v1,v2
if 0
figure
quiver(x1,y1,v1d(1),v1d(2),0.3,'c') %plot cross field
hold on
quiver(x1,y1,v2d(1),v2d(2),0.3,'c') %plot cross field
hold on
quiver(x1,y1,dnew(1),dnew(2),0.3,'r') %plot cross field
hold off
end
end