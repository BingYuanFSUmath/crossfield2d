function S = subdived_t_new(p,t,S,N,hmin)
%subdived triangle contains S
%INPUT:
%p,t: points and triangles
%S: inner singularity
%hmin: minimun grid size
%OUTPUT:
%S: singularity after moving
for sid = 1:size(S,2)
    eid = S(sid).E_ID;
    d1 = norm(p(t(eid,1))-p(t(eid,2)));
    d2 = norm(p(t(eid,3))-p(t(eid,2)));
    d3 = norm(p(t(eid,1))-p(t(eid,3)));
    d = max([d1,d2,d3]); %max side length of triangle
    if d >hmin %need subdivision
        n = ceil(d/hmin)+1; %num of edges on one side
        np = 0.5*(n+1)*(n+2); %num of local pts
        ne = n^2; %num of local triangles
        p0 = zeros(np,2); %record local pt locations
        tnew = zeros(ne,3); %new t
        p1 = p(t(eid,1),:);
        p2 = p(t(eid,2),:);
        p3 = p(t(eid,3),:);
        %generate new pts
        for i = 1:n+1
            for j = 1:i
                id = 0.5*i*(i-1)+j;%local pt id
                if i == 1
                    p0(id,:) = p1;
                elseif i == n+1
                    if j == 1
                        p0(id,:) = p2;
                    elseif j == n+1
                        p0(id,:) = p3;
                    else %inner pts on last row
                        s = (j-1)/n;
                        p0(id,:) = (1-s)*p2+s*p3;
                    end
                else %inner rows
                    s1 = (i-1)/n;
                    s2 = (j-1)/n;
                    p0(id,:) = (1-s1)*p1+(s1-s2)*p2+s2*p3;
                end
            end
        end
        %generate new triangles
        if 1
            for i = 1:n
                for j = 1:i
                    id = 0.5*i*(i-1)+j; %local t id
                    %e_id = size(t,1)+id; %global t id
                    id1 = id; %local pt id
                    id2 = 0.5*i*(i+1)+j;
                    id3 = id2+1;
                    tnew(id,:) = [id1,id2,id3];
                end
            end
        end
        if 1
            for i = n+1:-1:3
                for j = 2:i-1
                    id = 0.5*n*(n+1)+0.5*(n+i-2)*(n+1-i)+j-1; %local t id
                    %e_id = size(t,1)+id; %global t id
                    id1 =0.5*i*(i-1)+j; %local pt id
                    id2 = 0.5*(i-1)*(i-2)+j-1;
                    id3 = id2+1;
                    tnew(id,:) = [id1,id2,id3];
                end
            end
        end
        TR = triangulation(tnew,p0); %local triangulation
        Nnew = Node(TR); %local nodes
        id1 = 1;
        id2 = 0.5*n*(n+1)+1;
        id3 = np;
        Nnew(id1).d = N(t(eid,1)).d; %d at three vertices
        Nnew(id2).d = N(t(eid,2)).d;
        Nnew(id3).d = N(t(eid,3)).d;
        %find smallest angle to interpolate
        dtheta12 = Nnew(id2).d-Nnew(id1).d; %find dtheta
        if abs(dtheta12)>pi
            dtheta12 = dtheta12-sign(dtheta12)*2*pi;
        end
        dtheta13 = Nnew(id3).d-Nnew(id1).d; %find dtheta
        if abs(dtheta13)>pi
            dtheta13 = dtheta13-sign(dtheta13)*2*pi;
        end
        dtheta23 = Nnew(id3).d-Nnew(id2).d; %find dtheta
        if abs(dtheta23)>pi
            dtheta23 = dtheta23-sign(dtheta23)*2*pi;
        end
        e = zeros(3*n,2); %boundary edges
        for i = 1:n %interpolate boundary N.d
            s = i/n;
            pid = 0.5*i*(i+1)+1; %left side
            %Nnew(pid).d = (1-s)*Nnew(id1).d+s*Nnew(id2).d;
            Nnew(pid).d = Nnew(id1).d+s*dtheta12;
            pid_old = 0.5*i*(i-1)+1;
            e(i,:) = [pid_old,pid];
            pid = 0.5*(i+1)*(i+2); %right side
            %Nnew(pid).d = (1-s)*Nnew(id1).d+s*Nnew(id3).d;
            Nnew(pid).d = Nnew(id1).d+s*dtheta13;
            pid_old = 0.5*(i+1)*i;
            e(n+i,:) = [pid_old,pid];
            pid = id2+i; %bottom side
            %Nnew(pid).d = (1-s)*Nnew(id2).d+s*Nnew(id3).d;
            Nnew(pid).d = Nnew(id2).d+s*dtheta23;
            e(2*n+i,:) = [pid-1,pid];
        end
        U = fem(p0,tnew,e,Nnew);
        U = atan2(imag(U),real(U));
        for i = 1:np
            Nnew(i).d = mod(U(i),2*pi);%[0 2pi]
        end
        Snew = Singularity;
        Snew = find_S_new(Snew,Nnew,TR);
        Snew = Snew(1,2:end);
        if size(Snew,2)
        S(sid).x = Snew.x;
        S(sid).y = Snew.y;
        end
        %plot_vector_field(Nnew,S(sid),TR)
    end
end
end
