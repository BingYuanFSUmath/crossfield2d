function P = LegendrePoly(n,x)
%calculate legendre polynomial
%INPUT:
%n: order of polynomial
%x: evaluation value
%OUTPUT:
%P: value of P_n(x)

switch n
    case 0
        P = 1;
    case 1
        P = x;
    otherwise
        P = 1/n*((2*n-1)*x*LegendrePoly(n-1,x)...
            -(n-1)*LegendrePoly(n-2,x));
end