 classdef AdaptCurveInterp
    %automatically refined piecewise polynomial iterpolant
    %of a curve to a given tolerance.
    properties
        N
        glNodes
        glWeights
        baryWeights
        values
        locations
        child_left
        child_right
    end
    methods
        function self = AdaptCurveInterp(N,curve,tol,curve_interval)
            %initialize with order and curve
            self.N = N;
            self.child_left = [];
            self.child_right = [];
            tmin = curve_interval(1);
            tmax = curve_interval(2);
            %tmax could be greater than 1, last interval
            self.locations = tmin;
            xpts = zeros(N,2);
            [self.glNodes,self.glWeights] = LegendreLobattoNandW(N);
            % see if single interpolant is good enough
            for j = 1:N
                t = 0.5*(self.glNodes(j)+1);
                t = tmin+t*(tmax-tmin);%physical t
                t = mod(t,1);
                xpts(j,:) = curve.position_at(t);
            end
            self.baryWeights = BaryWeights(N,self.glNodes);
            e_max = max_interp_error(self.glNodes,xpts,...
                self.baryWeights,curve,[tmin tmax]);
            if e_max <= tol
                %accurate enough
                self.values = xpts;
            else
                %subdivide the interval
                self.child_left = AdaptCurveInterpChild;
                self.child_right = AdaptCurveInterpChild;
                [self.child_left,self.locations] = ...
                    self.child_left.init_child...
                    (self.glNodes,self.baryWeights,...
                    [tmin 0.5*(tmin+tmax)],...
                    curve,tol,self.locations);
                [self.child_right,self.locations] = ...
                    self.child_right.init_child...
                    (self.glNodes,self.baryWeights,...
                    [0.5*(tmin+tmax) tmax],...
                    curve,tol,self.locations);
            end
            %gather location of subdivisions
            self.locations(end+1) = tmax;
        end
        %{
        function x = interp_position_at(self,xi)
            x = zeros(1,2);
            if size(self.child_left,1) == 0 %not associated
                for j = 1:2
                    x(j) = LagrangeInterp(xi,self.N,self.glNodes,...
                        self.values(:,j),self.baryWeights);
                end
            else
                if xi <= 0
                    eta = 1+2*xi;
                    x = child_position_at(self.child_left,eta);
                else
                    eta = -1+2*xi;
                    x = child_position_at(self.child_right,eta);
                end
            end
        end
        %}
    end
end
            
                
            
        
    
   
