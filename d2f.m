        function d2f = d2f(curve,t)
            %approximate second derivative
            %INPUT:
            %curve:pCurve class
            %t; parameter
            %OUTPUT:
            %d2f: absolute value of second derivative
            dt = 1e-5; %can adjust to tmax-tmin
            t1 = mod(t-dt,1);
            t2 = mod(t+dt,1);
            p1 = curve.position_at(t1);
            p2 = curve.position_at(t2);
            p = curve.position_at(t);
            dy1 = (p(2)-p1(2))/(p(1)-p1(1));
            dy2 = (p2(2)-p(2))/(p2(1)-p(1));
            d2f = (dy2-dy1)/(p2(1)-p1(1));
            d2f = abs(d2f);
        end