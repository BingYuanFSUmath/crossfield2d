function plot_SL(N,TR,S,SL)
%plot triangle mesh, crosses, singularities, streamlines and boundary
figure
%plot triangle mesh
triplot(TR,'y')
hold on
%plot crosses
n = size(N);
x = zeros(n);
y = zeros(n);
d = zeros(n);
theta = zeros(n);
for i = 1:size(N,1)
    x(i) = N(i).x;
    y(i) = N(i).y;
    d(i) = N(i).d;
    theta(i) = N(i).cross;
end
%
u1 = cos(theta); %vector represents on direction of a cross
v1 = sin(theta);
u2 = cos(theta+pi/2); 
v2 = sin(theta+pi/2);
u3 = cos(theta+pi); 
v3 = sin(theta+pi);
u4 = cos(theta+1.5*pi); 
v4 = sin(theta+1.5*pi);
quiver(x,y,u1,v1,0.3,'c') %plot cross field
hold on
quiver(x,y,u2,v2,0.3,'c')
hold on
quiver(x,y,u3,v3,0.3,'c')
hold on
quiver(x,y,u4,v4,0.3,'c')
%}
%plot S with SLs
sx = NaN(size(S)); %singularity pts coordinates
sy = NaN(size(S));
for i = 1:size(S,2)
    if S(i).k ~= -4 && S(i).k ~= 0
       sx(i) = S(i).x;
       sy(i) = S(i).y;
    end
    scatter(sx(i),sy(i),[],'r','filled')
    %text(sx(i),sy(i),num2str(i),'Color','b','FontWeight','bold','FontSize',14)
end
%scatter(sx,sy,[],'r','filled')
%hold on
%plot SLs
for i = 1:size(SL,1)
    for j = 1:size(SL,2)
        if SL(i,j).status
            plot(SL(i,j).x(:,1),SL(i,j).x(:,2),'r')
            n = size(SL(i,j).x,1);
            %n = floor(n/2);
            text(SL(i,j).x(n,1),SL(i,j).x(n,2),...
                [num2str(i),',',num2str(j)])
            hold on
        end
    end
end    
hold off
axis equal
end