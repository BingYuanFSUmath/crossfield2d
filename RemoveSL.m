function SL = RemoveSL(SL,S,nS0)
%remove extra SLs
%INPUT:
%SL: combined streamlines
%S: combined singularities
%nS0: number of boundary singularities

n=6; %num of points used to interpolate
nS = size(S,2)-nS0; %number of inner singularities
starts = zeros(size(SL));%record SL start S id
ends = zeros(size(SL));%record SL end S id
SL_num = zeros(nS0+nS,1); %number of SLs connected to S
for i = 1:size(SL,1)%record start and end pts
    for j = 1:size(SL,2)
        if SL(i,j).status%SL exists
            [~, id] = find_element(SL(i,j).x(1,:),[[S.x]' [S.y]']);
            starts(i,j) = id; %S id, if not a S, id=0
            if id %starts with S
                %recrod number of SL connected to S(id)
                SL_num(id) = SL_num(id)+1;
            end
            [~, id] = find_element(SL(i,j).x(end,:),[[S.x]' [S.y]']);
            ends(i,j) = id;
            if id %ends with S
                %recrod number of SL connected to S(id)
                SL_num(id) = SL_num(id)+1;
            end
        end
    end
end

%remove duplicated SLs
for i = 1:size(SL,1)%search SL with same start/end pts
    for j = 1:size(SL,2)
        if SL(i,j).status%SL(i,j) exists
            for k = 1:size(SL,1)
                for l = 1:size(SL,2)
                    if k<=i && l<=j %exclude test pts and itself
                        continue
                    else
                        if SL(k,l).status%SL(k,l) exists
                            if starts(i,j)*ends(i,j)*starts(k,l)*ends(k,l)>0 ...
                                    && starts(i,j)==ends(k,l) && ends(i,j)==starts(k,l)
                                %start and end with singularities
                                %have same start/end pts
                                %check the direction
                                d1 = SL(i,j).x(2,:)-SL(i,j).x(1,:);
                                d2 = SL(k,l).x(end-1,:)-SL(k,l).x(end,:);
                                if acos(dot(d1,d2)/(norm(d1)*norm(d2)))>pi/2
                                    %opposite direction: a loop
                                    continue
                                else
                                    SL(k,l).status = 0;%remove SL(k,l)
                                    %adjust SL number at S
                                    SL_num(k) = SL_num(k)-1;
                                    SL_num(i) = SL_num(i)-1;
                                    %interpolate SL(i,j) and SL(k,l)
                                    SL(i,j).x = SL_interp(SL(i,j),SL(k,l));
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end


%remove extra SLs in one direction: keep SL staring with S0 and ending
%with S, remove closest SL starting from S
if nS %exists inner singularities
    for i = 1:nS0 %SL starts from S0
        for j = 1:size(SL,2)
            if SL(i,j).status %exists SL
                id = ends(i,j);%end point S id
                if id>nS0 %end with inner S
                    min_angle = pi;%record minimum angle
                    min_id = 0;
                    %use 4 points to approx direction
                    t = size(SL(i,j).x,1); %num of points on SL
                    if t<4 %less than 4 points
                        v0 = SL(i,j).x(1,:)-SL(i,j).x(end,:);
                    else
                        v0 = SL(i,j).x(end-3,:)-SL(i,j).x(end,:);
                    end
                    for k = 1:S(id).k+4
                        %if SL(id,k).status %only check existing SL
                            v = SL(id,k).x(2,:)...
                                -SL(id,k).x(1,:);
                            theta = acos(dot(v0,v)/(norm(v0)*norm(v)));
                            if theta<min_angle
                                min_angle = theta;
                                min_id = k;
                            end
                        %end
                    end
                    if SL(id,min_id).status %have not been removed before
                        SL(id,min_id).status = 0; %remove SL starting from S
                        %interpolate SL(i,j) with SL(id,min_id)
                        t = min(size(SL(i,j).x,1),size(SL(id,min_id).x,1));
                        %num of points on SL
                        if t>=n % >= n points
                            t = n;
                        end
                        SLtemp1 = Streamline;
                        SLtemp1.x = SL(i,j).x(end-t+1:end,:);
                        SLtemp2 = Streamline;
                        SLtemp2.x = SL(id,min_id).x(1:t,:);
                        SL(i,j).x(end-t+1:end,:) = SL_interp(SLtemp1,SLtemp2);
                        %smooth SL(i,j)
                        for loop = 1:5 %smooth N times
                            SL(i,j) = SmoothSL(SL(i,j));
                        end
                        %adjust SL number
                        SL_num(id) = SL_num(id)-1;
                        if ends(id,min_id)>0 %not end with boundary
                            SL_num(ends(id,min_id)) = SL_num(ends(id,min_id))-1;
                        end
                    end
                end
            end
        end
    end
end

%remove extra SLs in one direction: keep SL at S ending with a singularity
%without extra SL, remove closest SL ending with a singularity
%with extra SL in the same direction, or ending with boundary
if nS %exists inner singularities
    for i = nS0+1:nS0+nS %SL starts from S
        if SL_num(i)>S(i).k+4 %has extra SL ending with S_i
            for j = 1:size(SL,1) %loop through ends
                for k = 1:size(SL,2)
                    if SL(j,k).status && ends(j,k)==i
                        %SL ends with S_i, can be more than 1
                        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        %CASE1: CHECK INCOMING SL
                        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        %SL(j,k) starts with S_j, ends with S_i
                        if SL_num(j)>S(j).k+4
                            %extra SL starting at start singularity
                            break_flag = 0; %record break status
                            for l= 1:size(SL,1) %loop through ends
                                for m = 1:size(SL,2)
                                    if SL(l,m).status && ends(l,m)==j
                                        d1 = SL(j,k).x(2,:)-SL(j,k).x(1,:);
                                        %use 4 points to approx direction
                                        t = size(SL(l,m).x,1); %num of points on SL
                                        if t<4 %less than 4 points
                                            d2 = SL(l,m).x(1,:)-SL(l,m).x(end,:);
                                        else
                                            d2 = SL(l,m).x(end-3,:)-SL(l,m).x(end,:);
                                        end
                                        if acos(dot(d1,d2)/(norm(d1)*norm(d2)))<pi/(S(j).k+4)
                                            %extra SL at S_Sid in the same
                                            %direction
                                            SL(j,k).status = 0;%remove SL(j,k)
                                            %interpolate SL(l,m) with SL(j,k)
                                            t = min(size(SL(l,m).x,1),size(SL(j,k).x,1));
                                            %num of points on SL
                                            if t>=n % >= n points
                                                t = n;
                                            end
                                            SLtemp1 = Streamline;
                                            SLtemp1.x = SL(l,m).x(end-t+1:end,:);
                                            SLtemp2 = Streamline;
                                            SLtemp2.x = SL(j,k).x(1:t,:);
                                            SL(l,m).x(end-t+1:end,:) = SL_interp(SLtemp1,SLtemp2);
                                            %smooth SL(l,m)
                                            for loop = 1:5 %smooth N times
                                                SL(l,m) = SmoothSL(SL(l,m));
                                            end
                                            %update SL_num
                                            SL_num(j) = SL_num(j)-1;
                                            SL_num(i) = SL_num(i)-1;
                                            break_flag = 1;
                                            break
                                        end
                                    end
                                end
                                if break_flag
                                    break
                                end
                            end
                        end
                        %finish checking extra SL ending with S_i
                        if SL(j,k).status %SL(j,k) has not been removed
                            %need to remove nearest SL
                        %check the nearest SL starting with S_i
                        min_angle = pi;%record minimum angle
                        min_id = 0;
                        %use 4 points to approx direction
                        t = size(SL(j,k).x,1); %num of points on SL
                        if t<4 %less than 4 points
                            v0 = SL(j,k).x(1,:)-SL(j,k).x(end,:);
                        else
                            v0 = SL(j,k).x(end-3,:)-SL(j,k).x(end,:);
                        end
                        for id = 1:S(i).k+4
                            v = SL(i,id).x(2,:)-SL(i,id).x(1,:);
                            theta = acos(dot(v0,v)/(norm(v0)*norm(v)));
                            if theta<min_angle
                                min_angle = theta;
                                min_id = id;
                            end
                        end
                        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        %CASE2: CHECK OUTGOING SL
                        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        if SL(i,min_id).status %same direction SL exists
                            Sid = ends(i,min_id); %end singularity id
                            %SL(i,min_id) starts with S_i, ends with S_Sid
                            %or boundary
                            if Sid>0 %not end with boundary
                                if SL_num(Sid)>S(Sid).k+4
                                    %extra SL ending at end singularity
                                    break_flag = 0; %record break status
                                    for l= 1:size(SL,1) %loop through starts
                                        for m = 1:size(SL,2)
                                            if SL(l,m).status && starts(l,m)==Sid
                                                d1 = SL(l,m).x(2,:)-SL(l,m).x(1,:);
                                                %use 4 points to approx direction
                                                t = size(SL(i,min_id).x,1); %num of points on SL
                                                if t<4 %less than 4 points
                                                    d2 = SL(i,min_id).x(1,:)...
                                                        -SL(i,min_id).x(end,:);
                                                else
                                                    d2 = SL(i,min_id).x(end-3,:)...
                                                        -SL(i,min_id).x(end,:);
                                                end
                                                if acos(dot(d1,d2)/(norm(d1)*norm(d2)))...
                                                        <pi/(S(l).k+4)
                                                    %extra SL at S_Sid in the same
                                                    %direction
                                                    SL(i,min_id).status = 0;
                                                    %remove SL(i,min_id)
                                                    %update SL_num
                                                    SL_num(i) = SL_num(i)-1;
                                                    SL_num(Sid) = SL_num(Sid)-1;
                                                    break_flag = 1;
                                                    break
                                                end
                                            end
                                        end
                                        if break_flag
                                            break
                                        end
                                    end
                                end
                            else %end with boundary
                                SL(i,min_id).status = 0;
                                %remove SL(i,min_id)
                                %update SL_num
                                SL_num(i) = SL_num(i)-1;
                            end
                        end
                        if ~SL(i,min_id).status %outgoing SL has been removed
                            %interpolate SL(j,k) with SL(i,min_id)
                            t = min(size(SL(j,k).x,1),size(SL(i,min_id).x,1));
                            %num of points on SL
                            if t>=n % >= n points
                                t = n;
                            end
                            SLtemp1 = Streamline;
                            SLtemp1.x = SL(j,k).x(end-t+1:end,:);
                            SLtemp2 = Streamline;
                            SLtemp2.x = SL(i,min_id).x(1:t,:);
                            SL(j,k).x(end-t+1:end,:) = SL_interp(SLtemp1,SLtemp2);
                            %smooth SL(j,k)
                            for loop = 1:5 %smooth N times
                                SL(j,k) = SmoothSL(SL(j,k));
                            end
                        end
                        end
                    end
                end
            end
        end
    end
end









