%propagate along ONE bdy SL
%using triangulation class
function SL = propagateSL0(SL,S,TR,N,Sid,C,d0)
%Sid=i,the ith singularity point
status = zeros(1,size(TR.ConnectivityList,1)); %status of each triangle
%status = false; %no triangle has been checked !becomes a CONSTANT
%for i = 1:1
for i = 1:size(N)
    %%%%%%%%%%%%%%%%%%%%%%%%%
    %CASE1:REACH BOUNDARY
    %%%%%%%%%%%%%%%%%%%%%%%%%
    %initialize:x,y,eid,vid1,vid2,status(i)
    if size(SL.x,1)==1 %the initial point (column number = point number)
        d = SL.d0; %initial direction
        %%%%%%%%%%%%%%%%%%%%%%%%%
        %THE ONLY DIFFERENCE
        %eid = pointLocation(TR,SL.x)
        eid = pointLocation(TR,SL.x+1e-6.*d);
        %%%%%%%%%%%%%%%%%%%%%%%%%
        status(eid) = true; %record checked triangle ID
        %%%%%%%%%%%%%%%%%%%%%%%%%
        %find vertex ID given a point in a triangle
        [x,y,vid1,vid2] = find_first_vid(S(Sid),eid,d,TR); %TEST--OK!
        SL.x(end+1,:) = [x y]; %add first intersection point
        %%%%%%%%%%%%%%%%%%%%%%%%%
        %if not the initial point
        %eid,vid1,vid2 has already been calculated from previous step
        %%%%%%%%%%%%%%%%%%%%%%%%%
    end
    %check whether a bdy point or not
    boundary_status = 0;%check boundary
    Lia1 = ismember([vid1 vid2],C,'rows');
    Lia2 = ismember([vid2 vid1],C,'rows');
    if Lia1 || Lia2 %edge is on boundary
        boundary_status = 1;
        break
    end
    if boundary_status %x is on boundary
        break %exist propagate
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%
    %CASE2:ANOTHER SINGULARITY POINT
    %%%%%%%%%%%%%%%%%%%%%%%%%
    %check this before spiral, because goes back to another singularity
    %point does not counted as spiral
    if size(SL.x,1)==2 %the initial point & first intersection
        eid = find_eid(vid1,vid2,status,TR); %find next triangle id
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%
    %{
    if ismember(eid,[S.E_ID]) %next triangle contains a singularity point
        [~,Locb] = ismember(eid,[S.E_ID]); %Locb stores position of eid
        SL.x(end+1,:) = [S(Locb).x S(Locb).y]; %end with another singularity point
        break
    end
    %}
    %%%%%%%%%%%%%%%%%%%%%%%%%
    break_status = 0;
    for j = 1:size(S,2)%find the S inside circlr d
        if j~=Sid && sqrt((x-S(j).x)^2+(y-S(j).y)^2)<d0%exclude Si itself
            SL.x(end+1,:) = [S(j).x S(j).y]; %end with another singularity point
            break_status = 1;
            break
        end
    end
    if break_status
        break %break propagation
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%
    %CASE3:SPIRAL
    %%%%%%%%%%%%%%%%%%%%%%%%%
    %goes back to a triangle which has already been passed
    ti = edgeAttachments(TR,vid1,vid2); %triangle attached to v1,v2
    %should be 2 edges, becouse bby has already been checked
    Eid1 = ti{1}(1); %ti: cell
    Eid2 = ti{1}(2);
    if status(Eid1) && status(Eid2) %both have been passed
        break
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%
    %AFTER CHECK CRITERIA
    %%%%%%%%%%%%%%%%%%%%%%%%%
    %x: intersection point on an edge
    %d: direction to next triangle
    %eid: next triangle id
    %vid1,vid2: vertex ids of edge which has x on it
    %status(eid): stauts of the present triangle is true, update first
    %find the first intersection direction
    dt1 = find_nearest_d(x,y,d,vid1,vid2,N);
    %find the first intersection point
    [xt1,yt1,vid1t1,vid2t1,eid] = update(x,y,dt1,eid,vid1,vid2,TR,N);
    %xt1
    %yt1
    %find the second intersection direction
    %dt2 = find_nearest_d(xt1,yt1,dt1,vid1t1,vid2t1,N);
    %the nearest direction is RELATED to d
    dt2 = find_nearest_d(xt1,yt1,d,vid1t1,vid2t1,N);
    %update d
    d = 0.5*(dt1+dt2);
    %update x,y,vid1,vid2
    [x,y,vid1,vid2,eid] = update(x,y,d,eid,vid1,vid2,TR,N);
    %update eid
    status(eid) = true; %update status of present triangle
    eid = find_eid(vid1,vid2,status,TR); %find next triangle id
    SL.x(end+1,:) = [x y];
    %end
    
end
end



