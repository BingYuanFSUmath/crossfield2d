function L = Lagrange(x,N,x0,y0)
% Given a set of (x0,y0) and barycentric weights, evaluate
% the Lagrange interpolation at x.
%INPUT:
%x: evaluate point, 1D
%N: number of points
%x0,y0: given set of points
%OUTPUT:
%L: value at x

L = 0;
for j = 1:N
    lj = 1;
    for i = 1:N
        if (i~=j)
            lj = lj*(x-x0(i))/(x0(j)-x0(i));
        end
    end
    L = L+y0(j)*lj;
end

