function [N,S0,SL0,S0_id] = initialize_N(s_bdy,p,t,e,TR)
%initialize Nodes, find bdy crosses

N = Node(TR); %initialize Nodes
%initialize bdy pts cross and streamlines starts at bdy
S0_id = zeros(size(s_bdy,1)-1,4); %store S0/neighbour ids and angle
if size(s_bdy,1)%exists s_bdy
    %find all bdy pts and its neighbours
    S0 = Singularity; %initialize bdy singularity pts
    SL0(size(s_bdy,1)-1,3) = Streamline; 
    %SL starts from bdy, at most 3 SLs
    %size of SL0 is greater than S0
    e_unique = unique(e);%find all bdy point ids
    for index = 1:size(e_unique,1)%loop through bdy pts
        i = e_unique(index);%id of current pt
        for j = 1:size(e,1) %find vid1
            if i==e(j,1)
                vid1 = e(j,2);
                break
            elseif i==e(j,2)
                vid1 = e(j,1);
                break
            end
        end
        for j = 1:size(e,1) %find vid2
            if i==e(j,1) && e(j,2)~=vid1
                vid2 = e(j,2);
                break
            elseif i==e(j,2) && e(j,1)~=vid1
                vid2 = e(j,1);
                break
            end
        end
        x0 = p(i,1);%current pt location
        y0 = p(i,2);
        x1 = p(vid1,1);%neighbour pts locations
        y1 = p(vid1,2);
        x2 = p(vid2,1);
        y2 = p(vid2,2);
        %nonuniform triangle size
        theta1 = atan2(y1-y0,x1-x0);%[-pi,pi]
        theta2 = atan2(y0-y2,x0-x2);%[-pi,pi]
        N(i).cross = 0.5*(theta1+theta2);
        N(i).cross = mod(N(i).cross,pi/2);%[0,pi/2]
        if abs(N(i).cross-pi/2)<1e-6
            N(i).cross = 0;
        end
        N(i).d = 4*N(i).cross;%[0,2pi),countinuous
        %generate SL starting from bdy singularity pts
        %ismember is not reliable, have computational error
        [Lia,Locb] = find_element(p(i,:),s_bdy);
        if Lia %a singularity point
            %decide angle
            a = [x1-x0 y1-y0];
            b = [x2-x0 y2-y0];
            theta = acos(dot(a,b)/(norm(a)*norm(b)));%[0,pi],angle between a,b
            xq = [x0 y0]+0.25*(a+b);
            in = pointLocation(TR,xq); %find triangle ID
            if isnan(in)%query pt not in the trimesh
                theta = 2*pi-theta;
            end
            S0_id (Locb,:)= [i vid1 vid2 theta];%store S0/neighbour ids and angle
            theta1 = atan2(a(2),a(1));%[-pi,pi]
            theta2 = atan2(b(2),b(1));
            theta1 = mod(theta1,2*pi);%[0,2pi]
            theta2 = mod(theta2,2*pi);
            theta0 = theta1;
            if abs(mod(theta1+theta,2*pi)-theta2)>1e-6 %theta1+theta~=theta2
                theta0 = theta2;
            end
            %decide streamline directions
            for j = 1:3
                if ismember(i,t) %search each column
                    [~,loc] = ismember(i,t); %store row number->Eid
                    break
                end
            end
            id = size(S0,2);%number of S0
            S0(id+1).E_ID = loc;
            S0(id+1).x = p(i,1);
            S0(id+1).y = p(i,2);
            %RELAX
            if theta<3*pi/4+1e-2*pi
                S0(id+1).k = -4;
            elseif theta<5*pi/4+1e-2*pi
                S0(id+1).k = -3;
                for j = 1:1
                    SL0(id,j).x = p(i,:);
                    SL0(id,j).status = true;
                    cross = theta0+0.5*theta;
                    SL0(id,j).d0 = [cos(cross) sin(cross)];
                end
            elseif theta<7*pi/4+1e-2*pi
                S0(id+1).k = -2;
                for j = 1:2
                    SL0(id,j).x = p(i,:);
                    SL0(id,j).status = true;
                    cross = theta0+(j/3)*theta;
                    SL0(id,j).d0 = [cos(cross) sin(cross)];
                end
            else
                S0(id+1).k = -1;
                for j = 1:3
                    SL0(id,j).x = p(i,:);
                    SL0(id,j).status = true;
                    cross = theta0+(j/4)*theta;
                    SL0(id,j).d0 = [cos(cross) sin(cross)];
                end
            end
        end
    end
    for i = 1:size(S0_id,1)%recalculate N.d at S0
        a = [cos(N(S0_id(i,2)).d) sin(N(S0_id(i,2)).d)];
        b = [cos(N(S0_id(i,3)).d) sin(N(S0_id(i,3)).d)];
        a = 0.5*(a+b); %average of neighbour vectors
        N(S0_id(i,1)).d = atan2(a(2),a(1));%[-pi,pi]
        N(S0_id(i,1)).d = mod(N(S0_id(i,1)).d, 2*pi);%[0 2pi]
        N(S0_id(i,1)).cross = N(S0_id(i,1)).d/4;
    end
    S0 = S0(1,2:end); %remove the initial zero
else %no bdy singularity pts
    e_unique = unique(e);%find all bdy point ids
    for index = 1:size(e_unique,1)%loop through bdy pts
        i = e_unique(index);%id of current pt
        for j = 1:size(e,1) %find vid1
            if i==e(j,1)
                vid1 = e(j,2);
                break
            elseif i==e(j,2)
                vid1 = e(j,1);
                break
            end
        end
        for j = 1:size(e,1) %find vid2
            if i==e(j,1) && e(j,2)~=vid1
                vid2 = e(j,2);
                break
            elseif i==e(j,2) && e(j,1)~=vid1
                vid2 = e(j,1);
                break
            end
        end
        x0 = p(i,1);%current pt location
        y0 = p(i,2);
        x1 = p(vid1,1);%neighbour pts locations
        y1 = p(vid1,2);
        x2 = p(vid2,1);
        y2 = p(vid2,2);
        %nonuniform triangle size
        theta1 = atan2(y1-y0,x1-x0);%[-pi,pi]
        theta2 = atan2(y0-y2,x0-x2);%[-pi,pi]
        N(i).cross = 0.5*(theta1+theta2);
        N(i).cross = mod(N(i).cross,pi/2);%[0,pi/2]
        if abs(N(i).cross-pi/2)<1e-6
            N(i).cross = 0;
        end
        N(i).d = 4*N(i).cross;%[0,2pi),countinuous
        S0 = Singularity;%empty
        SL0 = Streamline;
    end
end
