function plot_mesh_Q(S,Quad,ex)
%plot triangle mesh, crosses, singularities, streamlines and boundary
figure
%plot S with SLs
sx = NaN(size(S)); %singularity pts coordinates
sy = NaN(size(S));
for i = 1:size(S,2)
    if S(i).k ~= -4 && S(i).k ~= 0
        sx(i) = S(i).x;
        sy(i) = S(i).y;
    end
end
scatter(sx,sy,[],'r','filled')
hold on
%plot SLs
for i = 1:size(Quad.E_x,1)
    if i>Quad.nbpts
        x = Quad.E_x{i};
        plot(x(:,1),x(:,2),'r')
        hold on
    end
end
%{
for i = 1:size(e,1)
    plot([N(e(i,:)).x]',[N(e(i,:)).y]','b','LineWidth',1.25)
    hold on
end
%}
%plot boundaries
for i = 1:ex.curve_num
    name = [ex.curve(i).name,'_p'];
    f = str2func(name);
    nt = 100;
    x = zeros(nt+1,2);
    dt = 1/nt;
    for n = 0:nt
        x(n+1,:) = f(dt*n);
    end
    plot(x(:,1),x(:,2),'b','LineWidth',1.25)
    hold on
end
hold off
axis equal
%title(['Example ',num2str(id)])
end