%boundary point class
classdef pBoundary
    properties
        x %point coordinates
        t %parameter value
        curve_id %id of boundary curve
    end
    methods
        function p_bdy = pBoundary %construct 
            p_bdy.x = [];
            p_bdy.t = [];
            p_bdy.curve_id = [];
        end
    end
end