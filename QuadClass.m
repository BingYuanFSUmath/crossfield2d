classdef QuadClass
    properties
        P %point locations
        E %edges, store pt ids
        E_x %edges, store SL pts locations on each egde, include ends
        Q %quad connectivity, use 4 pts,ccw
        QE %quad connectivity, use 4 edges, ccw
        C %pt connectivity, edges connect to a pt, store edge ids
        M %tangent vectors at vertices on bdy, P_numx3, [flag, vector]
        H %store hermite information on each edge, 4x2xsize(E) [p0,p1,m0,m1]
        nbpts %number of boundary pts
    end
    methods
        function Quad = QuadClass %initialize
            Quad.P = [];
            Quad.E = [];
            Quad.E_x = []; %cell
            Quad.Q = []; %cell
            Quad.QE = []; %cell
            Quad.C = []; %cell
            Quad.M = []; %cell
            Quad.H = [];
            Quad.nbpts = [];
        end
        function Quad = generator(Quad,SL,inter_p)
            bdy_p_num = size(Quad.P,1); %boundary pts number
            Quad.nbpts = bdy_p_num;
            bdy_e_num = size(Quad.E,1); %boundary edges number
            %clean up inter_p
            [p_copy,SL_id] = Quad.clean_up(Quad,inter_p);
            %generate P
            Quad.P = [Quad.P;p_copy]; %bdy+inner pts
            %Add Quad.E/E_x along each SL
            %(the pt on it or just before it)
            for i = 1:size(SL,1) %SL(i)
                count = 0;
                p = p_copy;
                p_id = zeros(size(p,1),2); %store pt id on SL(i)/Quad
                for j = 1:size(p,1) %p(j)
                    for k = 1:size(SL_id{j},2) %SL_id{j}(k)
                        if i==SL_id{j}(k) %p(j) on SL(i)
                            count = count+1;
                            p_id(j,2) = j+bdy_p_num;%pt id on Quad.P
                            for l = 2:size(SL(i).x,1) %SL(i).x(l)
                                p1 = SL(i).x(l-1,:);
                                p2 = SL(i).x(l,:);
                                xmin = min(p1(1),p2(1));
                                xmax = max(p1(1),p2(1));
                                ymin = min(p1(2),p2(2));
                                ymax = max(p1(2),p2(2));
                                e = 1e-10; %epsilon
                                if (xmin-e<=p(j,1)&&p(j,1)<=xmax+e)&&...
                                        (ymin-e<=p(j,2)&&p(j,2)<=ymax+e)
                                    %between x(l-1,l)
                                    %could be several pts on x(l-1,l)
                                    ds = norm(p(j,:)-p2)/norm(p2-p1);
                                    p_id(j,1) = l-ds;
                                    %{
                                    if norm(p(j,:)-p1)<e %on p1
                                        p_id(j,1) = l-1;
                                    end
                                    if norm(p(j,:)-p2)<e %on p2
                                        p_id(j,1) = l;
                                    end
                                    %}
                                    break %l
                                end %x(l-1,l)
                            end %l
                        end %i==SL_id{j}(k)
                    end %k
                end %j
                p_sort = [p p_id];
                p_sort = sortrows(p_sort,3); %sort by p_id(1,:)
                p = p_sort(end-count+1:end,1:2);
                p_id = p_sort(end-count+1:end,3:4);
                if size(p,1) == 0 %no inner pt on SL
                    E_x = SL(i).x;
                    [~,id1,~] = dup_check(Quad.P,SL(i).x(1,:),bdy_p_num,1e-5);
                    [~,id2,~] = dup_check(Quad.P,SL(i).x(end,:),bdy_p_num,1e-5);
                    E = [id1,id2];
                else
                    if abs(p_id(1,1)-1)>e %not the start point
                        p = [SL(i).x(1,:);p];
                        [~,id,~] = dup_check(Quad.P,SL(i).x(1,:),bdy_p_num,1e-5);
                        p_id = [1,id;p_id];
                    end
                    if abs(p_id(end,1)-size(SL(i).x,1))>e
                        %not the end point
                        p = [p;SL(i).x(end,:)];
                        [~,id,~] = dup_check(Quad.P,SL(i).x(end,:),bdy_p_num,1e-5);
                        p_id = [p_id;size(SL(i).x,1),id];
                    end
                    p_num = size(p,1);
                    E = zeros(p_num-1,2); %#ok<*PROPLC>
                    E_x = cell(p_num-1,1);
                    for j = 1:p_num-1
                        E(j,1) = p_id(j,2);
                        E(j,2) = p_id(j+1,2);
                        E_x{j} = SL(i).x(ceil(p_id(j,1)):floor(p_id(j+1,1)),:);
                        E_x{j} = [p(j,:);E_x{j};p(j+1,:)];
                        E_x{j} = unique_row(E_x{j},e);
                    end
                end
                Quad.E = [Quad.E;E];
                Quad.E_x = [Quad.E_x;E_x]; %cell
            end %i
            Quad.M = zeros(size(Quad.P,1),3);
            %generate Quad.C
            P = Quad.P;
            E = Quad.E;
            C = cell(size(P,1),1);
            for i = 1:size(P,1) %point id
                for j = 1:size(E,1) %edge id
                    if (i==E(j,1))||(i==E(j,2)) %on edge(j)
                        C{i} = [C{i},j];
                    end
                end
            end
            Quad.C = C;
            %generate Quad.Q
            E_status = zeros(size(E));
            %status of edge, L/R side, ccw
            if bdy_e_num == size(E,1)
                %only bdy edges, i.e. squre
                Quad.Q{1} = [1,2,3,4];
                Quad.QE{1} = [1,2,3,4];
            end
            for i = (bdy_e_num+1):size(E,1)
                %loop through inner edges
                if ~E_status(i,1) || ~E_status(i,2)
                    %one side of E(i) has not been checked
                    p0 = E(i,2);
                    p1 = E(i,1);
                    %find pts and egde ids connnected to p0
                    [p1,E_id] = Quad.pt_connection(p0,p1,i,E,C);
                    %find Q ccw
                    if ~E_status(i,1) %L side has not been checked
                        r_ccw = zeros(5,3);
                        %record edge id,L/R side status, pt id
                        r_ccw(1,:) = [i,1,p0];
                        p0_ccw = p0;
                        p1_ccw = p1;
                        eid = E_id;
                        for loop = 2:5
                            [p0_ccw,p1_ccw,eid,r] = Quad.ccw...
                                (Quad,p0_ccw,p1_ccw,eid,P,E,C);
                            r_ccw(loop,:) = r;
                        end
                        %find invalid quads
                        if r_ccw(1,3) == r_ccw(5,3) %goes back, 4 pts
                            Quad.Q{end+1,1} = r_ccw(1:4,3)';
                        elseif r_ccw(1,3) == r_ccw(4,3) %3 pts
                            Quad.Q{end+1,1} = r_ccw(1:3,3)';
                        else %5 or more pts
                            [~,~,~,r] = Quad.ccw...
                                (Quad,p0_ccw,p1_ccw,eid,P,E,C);
                            if r_ccw(1,:) == r %5 pts
                                Quad.Q{end+1,1} = r_ccw(1:5,3)';
                            else %more than 5 pts
                                error('Mesh generation failed. Choose another base trimesh.')
                            end
                        end
                        for j = 1:5
                            E_status(r_ccw(j,1),r_ccw(j,2)) = 1;
                            %record checked edge side
                        end
                    end
                    %
                    %find Q cw
                    if ~E_status(i,2) %R side has not been checked
                        r_cw = zeros(5,3);
                        %record edge id,L/R side status, pt id
                        r_cw(1,:) = [i,2,p0];
                        p0_cw = p0;
                        p1_cw = p1;
                        eid = E_id;
                        for loop = 2:5
                            [p0_cw,p1_cw,eid,r] = Quad.cw...
                                (Quad,p0_cw,p1_cw,eid,P,E,C);
                            r_cw(loop,:) = r;
                        end
                        %record cw Q, find invalid quads
                        if r_cw(1,3) == r_cw(5,3) %goes back, 4 pts
                            Quad.Q{end+1,1} = fliplr(r_cw(1:4,3)');
                        elseif r_cw(1,3) == r_cw(4,3) %3 pts
                            Quad.Q{end+1,1} = fliplr(r_cw(1:3,3)');
                        else %5 or more pts
                            [~,~,~,r] = Quad.cw...
                                (Quad,p0_cw,p1_cw,eid,P,E,C);
                            if r_cw(1,:) == r %5 pts
                                Quad.Q{end+1,1} = fliplr(r_cw(1:5,3)');
                            else %more than 5 pts
                                error('Mesh generation failed. Choose another base trimesh.')
                            end
                        end
                        for j = 1:5
                            E_status(r_cw(j,1),r_cw(j,2)) = 1;
                            %record checked edge side
                        end
                    end
                    %
                end
            end
            %generate Quad.QE
            Quad.QE = Quad.Q; %same size
            for i = 1:size(Quad.Q,1)
                n = size(Quad.Q{i},2); %num of pts of each Q
                for j = 1:n
                    if j < n
                        pid1 = Quad.Q{i}(j);
                        pid2 = Quad.Q{i}(j+1);
                    else
                        pid1 = Quad.Q{i}(n);
                        pid2 = Quad.Q{i}(1);
                    end
                    for k = 1:size(Quad.E,1)
                        if (pid1==Quad.E(k,1)&&pid2==Quad.E(k,2))...
                                ||(pid1==Quad.E(k,2)&&pid2==Quad.E(k,1))
                            id = k; %edge id
                            break
                        end
                    end
                    Quad.QE{i}(j) = id;
                end
            end
        end
        function plot_quad(Quad,N,e,id)
            %plot P and E
            figure
            %scatter(Quad.P(:,1),Quad.P(:,2))
            hold on
            text(Quad.P(:,1),Quad.P(:,2),num2str((1:size(Quad.P,1))'))
            hold on
            x = [Quad.P(Quad.E(:,1),1)';Quad.P(Quad.E(:,2),1)'];
            y = [Quad.P(Quad.E(:,1),2)';Quad.P(Quad.E(:,2),2)'];
            plot(x,y,'b')
            hold on
            for i = 1:size(e,1)
                plot([N(e(i,:)).x]',[N(e(i,:)).y]','r','LineWidth',1.25)
                hold on
            end
            hold off
            axis equal
            title(['Example ',num2str(id)])
        end
        function Quad = hermite(Quad,fd,e,N,id)
            Quad.H = zeros(4,2,size(Quad.E,1));
            hermite_x = Quad.E_x; %record hermite plot pts
            for i = 1:size(Quad.E,1)
                %a = 0.5;%a=0.1,strait line;a=1,spiral
                pid1 = Quad.E(i,1);
                pid2 = Quad.E(i,2);
                p0 = Quad.P(pid1,:);
                p1 = Quad.P(pid2,:);
                a = norm(p1-p0); %tangent size factor
                if size(Quad.E_x{i},1)<2 %only two points
                    m0 = p1-p0;
                    m1 = p0-p1;
                else
                    m0 = Quad.E_x{i}(2,:)-Quad.E_x{i}(1,:);
                    m1 = Quad.E_x{i}(end,:)-Quad.E_x{i}(end-1,:);
                end
                m0 = a*m0/norm(m0);
                m1 = a*m1/norm(m1);
                Quad.H(:,:,i) = [p0;p1;m0;m1];
                t = 0:0.05:1; %can modify later to spectral nodes
                t = t'; %column vector
                hermite_x{i} = (2*t.^3-3*t.^2+1)*p0...
                    +(t.^3-2*t.^2+t)*m0...
                    +(-2*t.^3+3*t.^2)*p1...
                    +(t.^3-t.^2)*m1;
            end
            %plot hermite quad
            figure
            %scatter(Quad.P(:,1),Quad.P(:,2))
            %hold on
            tol = 1e-5;
            for i = 1:size(Quad.E_x,1)
                flag = 1;
                p1 = Quad.P(Quad.E(i,1),:);
                p2 = Quad.P(Quad.E(i,2),:);
                if abs(fd(p1))< tol && abs(fd(p2))<tol
                    if size(Quad.E_x{i},1)>2
                        p0 = Quad.E_x{i}(ceil(size(Quad.E_x{i},1)/2),:);
                    else
                        p0 = 0.5*(p1+p2);
                    end
                    if abs(fd(p0))<tol
                        flag = 0;
                    end
                end
                if flag %not on boundary
                    x = hermite_x{i}(:,1);
                    y = hermite_x{i}(:,2);
                    plot(x,y,'b')
                    hold on
                end
            end
            %plot boundary
            for i = 1:size(e,1)
                plot([N(e(i,:)).x]',[N(e(i,:)).y]','r','LineWidth',1.25)
                hold on
            end
            %text(Quad.P(:,1),Quad.P(:,2),num2str((1:size(Quad.P,1))'))
            hold off
            axis equal
            title(['Example ',num2str(id)])
        end
    end
    methods(Static)
        function [p,SL_id] = clean_up(Q,inter_p)
            %clean up inter_p
            p0 = Q.P; %bdy points
            n0 = size(p0,1);
            n = size(inter_p.locations,1);
            p = zeros(n0+n,2); %store unique locations
            p(1:n0,:) = p0;
            %p(1,:) = inter_p.locations(1,:);
            count = n0;
            SL_id = cell(n0+n,1); %store SL_ids
            %SL_id{1} = inter_p.SL_ids(1,:); %row vector
            for i = 1:n
                [p,id,count] = dup_check(p,inter_p.locations(i,:),count,1e-5);
                SL_id{id} = [SL_id{id},inter_p.SL_ids(i,:)];
            end
            p = p(n0+1:count,:);
            SL_id = SL_id(n0+1:count);
            %id = []; %inter_p may be bdy pts
            for i = 1:size(SL_id,1)
                SL_id{i} = unique(SL_id{i});
                %if fd(p(i))<1e-10
                %    id = [id;i]; %record id of bdy pts
                %end
            end
            %p(id,:) = [];
            %SL_id{id} = [];
        end
        function [p,E_id] = pt_connection(p0,p1,id,E,C)
            %find points and edge ids connected to p0
            %INPUT:
            %p0: end point id
            %p1: start point id, 1x1
            %id: edge id of p0_p1
            %E,C: edges, point-edge connections
            %OUTPUT:
            %p: points connected to p0, ids, p(1)=p1
            %E_id: edges connected to p0, ids
            p = zeros(size(C{p0},2),1);
            E_id = zeros(size(p));
            p(1) = p1;
            E_id(1) = id;
            count = 1;
            %E is not unique
            for i = 1:size(p,1)
                if (E(C{p0}(i),1)~=p0)&&(E(C{p0}(i),1)~=p1)
                    count = count+1;
                    p(count) = E(C{p0}(i),1);
                    E_id(count) = C{p0}(i);
                end
                if (E(C{p0}(i),2)~=p0)&&(E(C{p0}(i),2)~=p1)
                    count = count+1;
                    p(count) = E(C{p0}(i),2);
                    E_id(count) = C{p0}(i);
                end
            end
        end
        function [p0,p1,E_id,r] = ccw(Quad,p0,p1,E_id,P,E,C)
            %find next p0,p1 ccw, record edge id, L/R status, pt id
            %INPUT:
            %p0: end point id
            %p1: start point ids, nx1
            %E_id: edge ids of p0_p1, nx1
            %E,C: edges, point-edge connections
            %OUTPUT:
            %p0: new end point id
            %p1: new start point ids, nx1
            %E_id: edge ids of p0_p1, nx1
            %r: record, [edge id, L/R status, pt id]
            v = zeros(size(E_id,1),2);
            %use p1-p0 to find angle may cause error when highly curved
            %use E_x instead. E_x includes end points p0,p1
            for i = 1:size(v,1)
                if norm(Quad.E_x{E_id(i)}(1,:)-P(p0,:))<1e-10
                    v(i,:) = Quad.E_x{E_id(i)}(2,:)-P(p0,:);
                else
                    v(i,:) = Quad.E_x{E_id(i)}(end-1,:)-P(p0,:);
                end
            end
            %v = P(p1,:)-ones(size(p1,1),1)*P(p0,:);
            theta = atan2d(v(:,2),v(:,1));
            theta = theta-theta(1); %theta(1) is copied
            theta = mod(theta,360); %atan2d [-180,180], clockwise
            theta_max = 0;%cw_max = ccw_min
            for i = 2:size(p1,1) %find theta_max
                if theta(i)>theta_max
                    theta_max = theta(i);
                    id = i;
                end
            end
            if p1(id) == E(E_id(id),2) %L side
                r = [E_id(id),1,p1(id)];
            else %R side
                r = [E_id(id),2,p1(id)];
            end
            t = p0; %exchange p0,p1
            p0 = p1(id);
            p1 = t;
            [p1,E_id] = Quad.pt_connection(p0,p1,E_id(id),E,C);
        end
        function [p0,p1,E_id,r] = cw(Quad,p0,p1,E_id,P,E,C)
            %find next p0,p1 cw, record edge id, L/R status, pt id
            %INPUT:
            %p0: end point id
            %p1: start point ids, nx1
            %E_id: edge ids of p0_p1, nx1
            %E,C: edges, point-edge connections
            %OUTPUT:
            %p0: new end point id
            %p1: new start point ids, nx1
            %E_id: edge ids of p0_p1, nx1
            %r: record, [edge id, L/R status, pt id]
            v = zeros(size(E_id,1),2);
            %use p1-p0 to find angle may cause error when highly curved
            %use E_x instead. E_x includes end points p0,p1
            for i = 1:size(v,1)
                if norm(Quad.E_x{E_id(i)}(1,:)-P(p0,:))<1e-10
                    v(i,:) = Quad.E_x{E_id(i)}(2,:)-P(p0,:);
                else
                    v(i,:) = Quad.E_x{E_id(i)}(end-1,:)-P(p0,:);
                end
            end
            %v = P(p1,:)-ones(size(p1,1),1)*P(p0,:);
            theta = atan2d(v(:,2),v(:,1));
            theta = theta-theta(1); %theta(1) is copied
            theta = mod(theta,360); %atan2d [-180,180], clockwise
            theta_min = Inf;
            for i = 2:size(p1,1) %find theta_min
                if theta(i)<theta_min
                    theta_min = theta(i);
                    id = i;
                end
            end
            if p1(id) == E(E_id(id),2) %R side
                r = [E_id(id),2,p1(id)];
            else %L side
                r = [E_id(id),1,p1(id)];
            end
            t = p0; %exchange p0,p1
            p0 = p1(id);
            p1 = t;
            [p1,E_id] = Quad.pt_connection(p0,p1,E_id(id),E,C);
        end
    end
end
