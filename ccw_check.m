function ccw_check(Quad)
%check ccw for each Q
for i = 1:size(Quad.Q,1)
    p1 = Quad.P(Quad.Q{i}(1),:);
    p2 = Quad.P(Quad.Q{i}(2),:);
    p3 = Quad.P(Quad.Q{i}(3),:);
    v1 = p2-p1;
    v2 = p3-p2;
    ccw = cross([v1,0],[v2,0]);
    if ccw<0
        i
    end
end
