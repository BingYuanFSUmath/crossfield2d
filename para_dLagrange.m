function p = para_dLagrange(t,N,x,y)
%t: parameter [0,1]
exchange = false;
for i = 1:N-1
    if (x(i+1)<x(i))
        exchange  = true;
        break
    end
end
if abs(max(x)-min(x))<eps
    exchange = true;
end
if exchange
    temp = x;
    x = y;
    y = temp;
end
x0 = t*(x(N)-x(1))+x(1);
y0 = dLagrange(x0,N,x,y)*(x(N)-x(1));
if exchange
    p = [y0,x0];
else
    p = [x0,y0];
end