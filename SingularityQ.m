%define singularity class using quad class
classdef SingularityQ
    properties
        qid %quad id in Q
        pids %vertex ids in Q
        eids %edge ids in Q
        pnew %new point ids in Qnew
        enew %new edge ids in Qnew
        Snew %vertex ids in Qnew
        status %status of each direction meet boundry
    end
    methods
        function S = SingularityQ %construct default values
            S.qid = 0;
            S.pids = 0;
            S.eids = 0;
            S.pnew = 0;
            S.enew = 0;
            S.Snew = 0;
            S.status = 0;
        end
    end
end