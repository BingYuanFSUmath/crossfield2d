%subdivide SLs according to tolerance
%generate Quad class and add boundary P,E
function [SLcopy_new,Quad] = SubdivideSL_hermite(SLcopy,S0,ex,Tol_poly,...
    TR,N,p,e,d0,fh,fd)
%OUTPUT:
%SLcopy_new: add new SLs after subdividion
%Quad: store bdy P & E
Quad = QuadClass; %initialize
%find all boundary points
[SLcopy,p_bdy] = exact_boundary_points(SLcopy,S0,p,ex);
%{
figure
triplot(TR)
axis equal
hold on
for i = 1:size(p_bdy,2)
scatter(p_bdy(i).x(1),p_bdy(i).x(2),[],'r','filled')
text(p_bdy(i).x(1),p_bdy(i).x(2),...
    ['\leftarrow' num2str(p_bdy(i).curve_id)])
hold on
end
hold off
%}

%generate boundary SLs, SL_bdy store p_bdy ids, ccw
SL_bdy = boundarySL(p_bdy,ex); %curve_num x 1 cell


%subdivide each boundary streamline
locations = cell(ex.curve_num,1); %store subdivide locations
for i = 1:ex.curve_num
    %can choose subdivide which curves
    locations{i} = cell(size(SL_bdy{i},1),1); %double layer cell
    for j = 1:size(SL_bdy{i},1)
        list = SL_bdy{i};
        tmin = p_bdy(list(j,1)).t;
        tmax = p_bdy(list(j,2)).t;
        if tmax<tmin %last interval, loop back
            tmax = tmax+1;
        end
        curve = ex.curve(i);
        curve_interp = AdaptCurveHermite(curve,Tol_poly,[tmin tmax]);
        locations{i}{j} = mod(curve_interp.locations,1);
        %avoid t_end>1
    end
end

%add boundary pts to bdy_pt_list to generate Quad
bdy_pt_list = cell(ex.curve_num,1);
for i = 1:ex.curve_num
    bdy_pt_list{i} = [locations{i}{:}]';
    %bdy_pt_list{i} = unique(bdy_pt_list{i});
    %unique is not reliable for real numbers
    bdy_pt_list{i} = unique_list(bdy_pt_list{i},1e-10);
    bdy_pt_list{i} = sort(bdy_pt_list{i});
end

%reshape SLcopy
SL(size(SLcopy,1)*size(SLcopy,2),1) = Streamline;
for i = 1:size(SLcopy,1)
    for j = 1:size(SLcopy,2)
        SL((i-1)*size(SLcopy,2)+j) = SLcopy(i,j);
    end
end
SL([SL.status] == 0) = []; %remove SL which does not exist
SLcopy = SL;

%Add new points
p_list = [];
for i = 1:ex.curve_num
    t_list = []; %new point parameter on curve i
    for j = 1:size(locations{i},1) %num of SL_bdy on curve i
        list = locations{i}{j};
        %new points parameters on SL_bdy(j) on curve i
        t_list = [t_list,list(2:end-1)]; %remove end points
    end
    %calculate point location
    list = zeros(size(t_list,2),2);
    for k = 1:size(t_list,2)
        list(k,:) = ex.curve(i).position_at(t_list(k));
    end
    p_list = [p_list;list];
end

if size(p_list,1)>0 %exists subdivision
    %Add new streamlines
    S(1,size(p_list,1)) = Singularity;
    SLnew(size(p_list,1),1) = Streamline;
    for i = 1:size(S,2)
        %initialize S, SLnew
        S(i).x = p_list(i,1);
        S(i).y = p_list(i,2);
        [S(i),SLnew(i)] = initialize_SLnew(S(i),SLnew(i),p,e,TR,fd);
        %S(i) is not accurate, changed in initialize_SLnew
        %if use exact S(i), propagateSL does not work well
    end
    for i = 1:size(SLnew,1)
        SLnew(i) = propagateSL(SLnew(i),S,TR,N,i,e,d0,fh,0,SLnew);
        %starting point is not an exact bdy pt
        SLnew(i).x = [p_list(i,:);SLnew(i).x(2:end,:)];
        %add exact boundary point
    end
    %use accurate S
    for i = 1:size(S,2)
        %initialize S, SLnew
        S(i).x = p_list(i,1);
        S(i).y = p_list(i,2);
    end
    %change end pts to exact S
    for i = 1:size(SLnew,1)
        id = SLnew(i).end_type(1);
        if id>0 %end with a S
            SLnew(i).x(end,:) = [S(id).x,S(id).y];
        end
    end
    for i = 1:5 %smooth N times
        SLnew = SmoothSL(SLnew,S,fd,fh,d0);
    end
    SLnew = RemoveSL_new(SLnew,S,TR,N,e,d0,fh,0);
    %change end pts to exact boundary pts
    [SLnew,~] = exact_boundary_points(SLnew,S0,p,ex);
    SLcopy_new = [SLcopy;SLnew];
    %add end pts of SLnew to bdy_pt_list
    for i = 1:size(SLnew,1)
        if SLnew(i).end_type == 0 %end with a boundary
            x = SLnew(i).x(end,:); %end pt
            for j = 1:ex.curve_num
                t = ex.curve(j).find_t(x);
                if t>=0 %t=-1: not on that curve
                    bdy_pt_list{j}(end+1) = t; %add end pt
                    break
                end
            end
        end
    end
else %no subdivision
    %SLnew = Streamline; %for test output
    SLcopy_new = SLcopy;
end
SLcopy_new([SLcopy_new.status] == 0) = []; %remove SL which does not exist

%add bdy pts to Quad
for i = 1:ex.curve_num
    %bdy_pt_list{i} = unique(bdy_pt_list{i});
    %unique is not reliable for real numbers
    bdy_pt_list{i} = unique_list(bdy_pt_list{i},1e-5);
    bdy_pt_list{i} = sort(bdy_pt_list{i});
    n = size(bdy_pt_list{i},1); %num of pts on closed curve
    P = zeros(n,2); %store pt location
    for j = 1:n
        P(j,:) = ex.curve(i).position_at(bdy_pt_list{i}(j));
    end
    Quad.P = [Quad.P;P];
    E = zeros(n,2); %a bdy curve is always closed
    E_x = cell(n,1);
    for j = 1:n
        j1 = j;
        j2 = j+1;
        if j == n
            j2 = 1;
        end
        E(j,:) = [j1,j2];
        tmin = bdy_pt_list{i}(j1);
        tmax = bdy_pt_list{i}(j2);
        %store 4 pts
        E_x{j} = zeros(4,2);
        E_x{j}(1,:) = P(E(j,1),:); %start pt
        E_x{j}(4,:) = P(E(j,2),:); %end pt
        E_x{j}(2,:) = ex.curve(i).position_at(mod(tmin+1e-3,1));
        E_x{j}(3,:) = ex.curve(i).position_at(mod(tmax-1e-3+1,1));
    end
    m = size(Quad.E,1);
    Quad.E = [Quad.E;E+m];
    Quad.E_x = [Quad.E_x;E_x];
end
%if size(S,1)>0 %exists subdivision
%plot_mesh(N,S,SLnew,e);%check added SLs
%end
end




