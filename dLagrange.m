function dL = dLagrange(x,N,x0,y0)
% Given a set of (x0,y0) evaluate
% the the first derivative of Lagrange interpolation at x.
%INPUT:
%x: evaluate point, 1D
%N: number of points
%x0,y0: given set of points
%OUTPUT:
%L: value at x

dL = 0;
for j = 1:N
    dlj = 0;
    bot = 1;
    for k = 1:N
        if (k~=j)
        bot = bot*(x0(j)-x0(k));
        top = 1;
        for i = 1:N
            if (i~=j && i~=k)
                top = top*(x-x0(i));
            end
        end
        dlj = dlj+top;
        end
    end
    dlj = dlj/bot;
    dL = dL+y0(j)*dlj;
end

