function L = LagrangeInterp(x,N,x0,y0,w)
% Given a set of (x0,y0) and barycentric weights, evaluate
% the Lagrange interpolation at x.
%INPUT:
%x: evaluate point, 1D
%N: number of points
%x0,y0: given set of points
%w: barycentric weights
%OUTPUT:
%L: value at x

l = 1; %l(x)
for i = 1:N
    l = l*(x-x0(i));
end
L = 0;
for i = 1:N
    L = L+w(i)*y0(i)/(x-x0(i));
end
L = l*L;
%barycentric form is undefined at x=x_j
for i = 1:N
    if x-x0(i) == 0 %x is a node
        L = y0(i); %return exact value
        break
    end
end

