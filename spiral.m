function p = spiral(n)
%return points on the spiral
%INPUT:
%n: partition number
%OUTPUT:
%p: coordinates of points on the spiral

a = 1/sqrt(1.5);
b = log(1.5)/(2*pi);
x = zeros(n+1,1); %coordinates on the spiral
y = zeros(n+1,1);
for i = 1:n+1
    theta = pi+2*pi/n*(i-1);
    x(i) = a*cos(theta)*exp(b*theta);
    y(i) = a*sin(theta)*exp(b*theta);
end
p = [x y];

    