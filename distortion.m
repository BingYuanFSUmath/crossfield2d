function dmin = distortion(Q)
%INPUT:
%Q: quad class
%OUTPUT: minJ/maxJ

Ne = size(Q,1); %num of quads
d = zeros(Ne,1);
Np = int16(sqrt(size(Q(1).P,1))); %num of pt on each direction
N = 11; %num of pt on each direction to evaluate Jacobian
J = zeros(N,N,Ne);
x = zeros(Np,2,4);
[xi0,eta0] = meshgrid(0:1.0/(N-1):1,0:1.0/(N-1):1);
for i = 1:Ne
    %points on each quad edge
    x(:,:,1) = Q(i).P(1:Np,:);
    x(:,:,3) = Q(i).P(Np^2-Np+1:Np^2,:);
    for j = 1:Np
        x(j,:,2) = Q(i).P(j*Np,:);
        x(j,:,4) = Q(i).P((j-1)*Np+1,:);
    end
    Gamma = cell(4,1); %store gamma functions
    dGamma = cell(4,1);
    for j = 1:4 %each quad edge
        Gamma{j} = @(t)para_Lagrange(t,Np,...
            x(:,1,j),x(:,2,j));
        dGamma{j} = @(t)para_dLagrange(t,Np,...
            x(:,1,j),x(:,2,j));
    end
    for m = 1:N
        for n = 1:N
            xi = xi0(m,n);
            eta = eta0(m,n);
            dxxi = x(1,:,1)*(1-eta)-x(1,:,2)*(1-eta)-x(Np,:,3)*eta+x(Np,:,4)*eta...
                +dGamma{1}(xi)*(1-eta)+dGamma{3}(xi)*eta...
                +Gamma{2}(eta)-Gamma{4}(eta);
            dxeta = x(1,:,1)*(1-xi)-x(Np,:,4)*(1-xi)-x(Np,:,3)*xi+x(1,:,2)*xi...
                +dGamma{4}(eta)*(1-xi)+dGamma{2}(eta)*xi...
                +Gamma{3}(xi)-Gamma{1}(xi);
            %J will be negative if vertices are cw
            J(m,n,i) = abs(dxxi(1)*dxeta(2)-dxxi(2)*dxeta(1));
        end
    end
    Jmax = max(reshape(J(:,:,i),[],1));
    Jmin = min(reshape(J(:,:,i),[],1));
    d(i) = Jmin/Jmax;
end
dmin = min(d);


