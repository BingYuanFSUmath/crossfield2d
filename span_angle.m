function acc = span_angle(p,q,acc)
%find angle spaned in each quad to decide wheather element is acceptable or
%not
%INPUT:
%p: point coordinates
%q: quad connectivity
%acc: accepatability
%OUTPUT:
%acc: accepatability
theta_max = 0.8*pi;
theta_min = 0.2*pi;
for i = 1:size(q,1)
    v = zeros(4,2);
    theta = zeros(4,1);
    for j = i:4
        v(j,:) = p(q(i,j),:);
    end
    for j = 1:4
        id0 = j;
        id1 = j-1;
        id2 = j+1;
        if j==1
            id1 = 4;
        end
        if j==4
            id2 = 1;
        end
        a = v(id1,:)-v(id0,:);
        b = v(id2,:)-v(id0,:);
        theta(j) = acos(dot(a,b)/(norm(a)*norm(b)));
    end
    if min(theta)<theta_min
        acc = 0;
    elseif max(theta)>theta_max
        acc = 0;
    elseif abs(sum(theta)-2*pi)>1e-2 %inverted quad
        acc = 0;
    end
end
    
    