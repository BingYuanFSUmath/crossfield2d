%define Node class using triangulation class
classdef Node
    properties
        vertex_ID
        x %node location
        y
        cross %cross angle 
        d %vector angle
        status %status of passed
        neighbour_Ns %neighbour nodes
        neighbour_Es %neighbour elements
    end
    methods
        function N = Node(TR) %initalization
            if nargin ~= 0 %num of input arguments
                n = size(TR.Points,1);
                N(n,1) = Node; %create object array
                for i = 1:n
                    N(i).vertex_ID = i;
                    N(i).x = TR.Points(i,1);
                    N(i).y = TR.Points(i,2);
                    N(i).status = 0; %initial nodes are inactive
                end
                e = TR.ConnectivityList; %triangle elements
                Ne = size(e,1); %number of triangles
                for i = 1:Ne %ith triangle
                    for j = 1:3 %add neighbour nodes and elements
                        N(e(i,j)).neighbour_Es(end+1) = i;
                        t = e(i,j); %vertex_ID of jth vertex
                        for k = 1:3
                            if (k~=j)&&(~any(e(i,k)==N(t).neighbour_Ns))
                                N(t).neighbour_Ns(end+1) = e(i,k);
                            end
                        end
                    end
                end
            end
        end
    end
end