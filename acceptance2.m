function acc = acceptance2(beta,beta_new,quad_ids)
%Acceptance Criteria for Constrained Laplacian Node Movement
%suggested by Scott A
%Nim: Number of elements whose metric improves
%Nwr: Number of elements whose metric gets worse
%dbeta: Average metric change
%N1: Number of elements whose metric improves significantly
%N0: Number of elements whose metric worsens significantly
%N00: Number of inverted elements created

Nim = 0; Nwr = 0; b_ave = 0; b0_ave = 0; N1 = 0; N0 = 0; N00 = 0;
bmin = 0.05; %acceptable quality
N = size(quad_ids,1); %number of neighbour quads
for i = 1:N
    eid = quad_ids(i,1);
    qid = quad_ids(i,2);
    b = beta_new(eid,qid); %new beta
    b0 = beta(eid,qid); %old beta
    %Number of elements whose metric improves/worse
    if b>b0
        Nim = Nim+1;
    elseif b<b0
        Nwr = Nwr+1;
    end
    %Average metric change
    b_ave = b_ave+b;
    b0_ave = b0_ave+b0;
    %Number of elements whose metric improves significantly
    %1.Metric goes from negative to positive
    if b0<0 && b>0
        N1 = N1+1;
    end
    %2.Metric becomes less negative
    if b0<0 && b<0 && b>b0
        N1 = N1+1;
    end
    %3.Metric moves from below acceptable to acceptable quality
    if b0<bmin && b>bmin
        N1 = N1+1;
    end
    %Number of elements whose metric worsens significantly
    %1.Metric goes from positive to negative
    if b0>0 && b<0
        N0 = N0+1;
    end
    %2.Metric becomes more negative
    if b0<0 && b<0 && b<b0
        N0 = N0+1;
    end
    %3.Metric drops below acceptable levels
    if b<bmin
        N0 = N0+1;
    end
    %Number of inverted elements created
    if b<-1 %inverted element
        N00 = N00+1;
    end
end
%Average metric change
dbeta = (b-b0)/N;
%decide acceptance
acc = 1;
if Nwr == N
    acc = 0;
elseif N00 > 0
    acc = 0;
elseif N0 > N1
    acc = 0;
elseif dbeta < -bmin
    acc = 0;
end
if acc>0 %not rulued out 
    acc = 0;
    if Nim == N
        acc = 1;
    elseif (N1>0) && (N0==0)
        acc = 1;
    elseif (N1>=N0) && (dbeta>-bmin)
        acc = 1;
    end
end
%}
    
    
    
    