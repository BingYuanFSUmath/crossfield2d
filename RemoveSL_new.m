function SL = RemoveSL_new(SL,S,TR,N,e,d0,fh,N_S0)
%remove extra SLs
%INPUT:
%SL: combined streamlines
%S: combined singularities

SL_num = zeros(size(SL));
%record num of SL connected to S(i,j) direction
for i = 1:size(SL,1)
    for j = 1:size(SL,2)
        if SL(i,j).status
            SL_num(i,j) = SL_num(i,j)+1;
            if SL(i,j).end_type(1)>0
                SL_num(SL(i,j).end_type(1),SL(i,j).end_type(2)) =...
                    SL_num(SL(i,j).end_type(1),SL(i,j).end_type(2))+1;
            end
        end
    end
end

%remove duplicated SLs
%keep SL at S ending with a singularity, remove closest SL ending with
%boundary or spiral
for i = 1:size(SL,1)%search SL with same start/end pts
    for j = 1:size(SL,2)
        if SL(i,j).status%SL(i,j) exists
            for k = 1:size(SL,1)
                for l = 1:size(SL,2)
                    if SL(k,l).status%SL(k,l) exists
                        if SL(i,j).end_type(1)>0 &&...
                                SL(i,j).end_type(1)==k &&...
                                SL(i,j).end_type(2)==l &&...
                                SL(k,l).end_type(1)>0 &&...
                                SL(k,l).end_type(1)==i &&...
                                SL(k,l).end_type(2)==j
                            %end with a singularity
                            %have same start/end pts and direction
                            SL(k,l).status = 0;%remove SL(k,l)
                            %adjust SL number at S
                            SL_num(k,l) = SL_num(k,l)-1;
                            SL_num(i,j) = SL_num(i,j)-1;
                            %interpolate SL(i,j) and SL(k,l)
                            SL(i,j).x = SL_interp(SL(i,j),SL(k,l));
                        elseif SL(i,j).end_type(1)>0 &&...
                                SL(i,j).end_type(1)==k &&...
                                SL(i,j).end_type(2)==l &&...
                                SL(k,l).end_type(1)<0 
                            %end with spiral
                            %have same start/end pts and direction
                            SL(k,l).status = 0;%remove SL(k,l)
                            %interpolate SL(i,j) and SL(k,l)
                            SL(i,j).x = SL_interp2(SL(i,j),SL(k,l));
                            %adjust SL number at S
                            SL_num(k,l) = SL_num(k,l)-1;
                        elseif SL(i,j).end_type(1)>0 &&...
                                SL(i,j).end_type(1)==k &&...
                                SL(i,j).end_type(2)==l &&...
                                SL(k,l).end_type(1)==0
                            %end with boundary 
                            %have same start/end pts and direction
                            if SL_num(i,j) == 1
                                %SL(i,j) is unique in that direction
                                SL(k,l).status = 0;%remove SL(k,l)
                                %interpolate SL(i,j) and SL(k,l)
                                SL(i,j).x = SL_interp2(SL(i,j),SL(k,l));
                                %adjust SL number at S
                                SL_num(k,l) = SL_num(k,l)-1;
                            else
                                %SL(i,j) direction is not unique
                                %find all SLs end with SL(i,j)
                                flag = 0; %flag of check status
                                for kk = 1:size(SL,1)
                                    for ll = 1:size(SL,2)
                                        if SL(kk,ll).status>0 ...
                                            &&SL(kk,ll).end_type(1)==i...
                                            &&SL(kk,ll).end_type(2)==j...
                                            &&SL_num(kk,ll)==1
                                            %SL(kk,ll) end with SL(i,j)
                                            %%and has only one SL 
                                            flag = 1;%SL(kk,ll) kept
                                            SL(i,j).status = 0;%remove SL(i,j)
                                            %interpolate SL(i,j) and SL(kk,ll)
                                            SL(i,j).x = SL_interp2(SL(i,j),SL(kk,ll));
                                            %adjust SL number at S
                                            SL_num(i,j) = SL_num(i,j)-1;
                                            SL_num(k,l) = SL_num(k,l)-1;
                                            break
                                        end
                                    end
                                    if flag
                                        break
                                    end
                                end
                                if ~flag %no unique SL connected
                                    SL(k,l).status = 0;%remove SL(k,l)
                                    %interpolate SL(i,j) and SL(k,l)
                                    SL(i,j).x = SL_interp2(SL(i,j),SL(k,l));
                                    %adjust SL number at S
                                    SL_num(k,l) = SL_num(k,l)-1;
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end

%remove SL starts/ends with more than one SL in bothdirection
for i = 1:size(SL,1)%search SL with same start/end pts
    for j = 1:size(SL,2)
        if SL(i,j).status && SL(i,j).end_type(1)>0 
            k = SL(i,j).end_type(1);
            l = SL(i,j).end_type(2);
            if SL_num(i,j)>1 && SL_num(k,l)>1
                SL(i,j).status = 0;
                SL_num(i,j) = SL_num(i,j)-1;
                SL_num(k,l) = SL_num(k,l)-1;
            end
        end
    end
end

%propagate SL if streamline starting from S exists and ends with another S
for i = 1:size(SL,1)%search SL with same start/end pts
    for j = 1:size(SL,2)
        if SL(i,j).status && SL(i,j).end_type(1)>0 
            k = SL(i,j).end_type(1);
            l = SL(i,j).end_type(2);
            %more than one SL connect to SL(k,l)
            %keep on propagating if the other SL ends in a unique direction
            %remove the other SL if it ends with more than one SL in that
            %direction
            Stemp = S; %record S
            while SL_num(k,l)>1
                %CASE1: extra SL emits from S(k,l)
                if SL(k,l).status
                    if SL(k,l).end_type(1)>0 %end with S
                        kk = SL(k,l).end_type(1);%check the end of the other direction
                        ll = SL(k,l).end_type(2);
                        if SL_num(kk,ll)>1 %not the unique SL, remove
                            SL(k,l).status = 0;
                            SL_num(k,l) = SL_num(k,l)-1;
                            SL_num(kk,ll) = SL_num(kk,ll)-1;
                            break
                        end
                    else %end with boundary or spiral
                        SL(k,l).status = 0;
                        SL_num(k,l) = SL_num(k,l)-1;
                        break
                    end
                else %CASE2: extra SL end with S(k,l)
                    for kk = 1:size(SL,1)
                        for ll = 1:size(SL,2)
                            if SL(kk,ll).status && SL(kk,ll).end_type(1)>0 ...
                                    && SL(kk,ll).end_type(1) == k...
                                    && SL(kk,ll).end_type(2) == l
                                if SL_num(kk,ll)>1 %not the unique SL, remove
                                    SL(kk,ll).status = 0;
                                    SL_num(k,l) = SL_num(k,l)-1;
                                    SL_num(kk,ll) = SL_num(kk,ll)-1;
                                    break
                                end
                            end
                        end
                    end
                    %{
                    %only propagate when SL(i,j) is worse than SL(kk,ll)
                    v0 = SL(k,l).x(2,:)-SL(k,l).x(1,:); %S(k,l) direction
                    v1 = SL(i,j).x(end,:)-SL(i,j).x(end-1,:); %SL(i,j) direction
                    v2 = SL(kk,ll).x(end,:)-SL(kk,ll).x(end-1,:);%SL(kk,ll) direction
                    theta1 = acos(dot(v0,v1)/(norm(v0)*norm(v1)));
                    theta2 = acos(dot(v0,v2)/(norm(v0)*norm(v2)));
                    if theta1 < theta2 %break, propagate SL(kk,ll) in another loop
                        break
                    end
                    %}
                end
                %no break, continue propagate
                Stemp(k).k = -4; %avoid connect to S(k,l)
                SLtemp = SL(i,j);
                SLtemp.x = SL(i,j).x(1,:); %initialize
                SLtemp = propagateSL(SLtemp,Stemp,TR,N,i,e,d0,fh,N_S0,SL);
                if SLtemp.end_type(1)==0 %end with boundary
                    SL(i,j) = SLtemp;
                    SL_num(k,l) = SL_num(k,l)-1;
                    break
                elseif SLtemp.end_type(1)>0 %end with another S
                    kk = SLtemp.end_type(1);
                    ll = SLtemp.end_type(2);
                    if SL(kk,ll).status>0 && SL(kk,ll).end_type(1)<=0
                        %end with bdy or spiral
                        %remove SL(kk,ll)
                        SL(kk,ll).status = 0;
                        SL(i,j) = SLtemp;
                        SL_num(k,l) = SL_num(k,l)-1;
                        break
                    else %update fails
                        break
                    end
                else %end with spiral, update fails
                    %try the other direction in another loop
                    break
                end
            end
        end
    end
end








