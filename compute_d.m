function d1 = compute_d(N,N1,N2,N3)
x1 = N(N1).x; %coordinates of 3 nodes
y1 = N(N1).y;
x2 = N(N2).x;
y2 = N(N2).y;
x3 = N(N3).x;
y3 = N(N3).y;
d2 = N(N2).d; %d of N2,N3
d3 = N(N3).d;
u3 = d3-d2; %relative d3 to d2
rx3 = sqrt((x2-x3)^2+(y2-y3)^2); %distance between N2,N3
d23 = rx3;
ry1 = abs((x3-x2)*(y2-y1)-(x2-x1)*(y3-y2))/sqrt((x3-x2)^2+(y3-y2)^2);
%relative coordinates to N2, distance from N1 to N2N3
d12 = sqrt((x1-x2)^2+(y1-y2)^2); %distance between N1,N2
d13 = sqrt((x1-x3)^2+(y1-y3)^2); %distance between N1,N3
k = (x1-x3)/(y3-y1); %slope of line orthogonal to N2N3
a = k; %kx-y-kx2+y2=0
b = -1.0; %coefs of line eqn vertial to N2N3 and passes N2
c = -k*x2+y2;
d12v = abs(a*x1+b*y1+c)/sqrt(a^2+b^2); %distance between N1,N2 when N1 is above N2
d13v = sqrt(d12v^2+d23^2); %distance between N1,N3 when N1 is above N2
rx1 = sqrt(d12^2-ry1^2);
if d12<d13 && d13>d13v % N1 left to mid of N2N3 & N1 left to N2
    rx1 = -rx1;
end
d1 = d2+u3/rx3*rx1+sqrt(1-(u3/rx3)^2)*ry1;
end