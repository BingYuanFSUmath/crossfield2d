function beta = distortion_metric_new(Q,ids,beta)
%find distortion metric beta for each element
%INPUT:
%Q: Quad class, Nex1
%ids: id of quads need to calculate beta, Nx2, [eid,qid]
%beta: distortion metric of each quad element
%OUTPUT:
%beta: updated distortion metric of each quad element
%beta<0: inverted/concave  

for i = 1:size(ids,1) %loop through each element
    eid = ids(i,1);
    qid = ids(i,2);
    q = Q(eid).Q(qid,:); %quad connectivity, 4 pts
    v = zeros(4,2); %vertex coordinates
    e = zeros(3,2); %edge vector of each subtriangle
    alpha = zeros(4,1); %alphas of subtriangles
    for j = 1:4
        v(j,:) = Q(eid).P(q(j),:);
    end
    for j = 1:4 %loop through each subtriangle
        t1 = j;
        t2 = j+1;
        t3 = j+2;
        if t2>4
            t2 = t2-4;
        end
        if t3>4
            t3 = t3-4;
        end
        t = [t1,t2,t3]; %subtriangle
        for k = 1:3
            t2 = k+1;
            if t2>3
                t2 = t2-3;
            end
            e(k,:) = v(t(t2),:)-v(t(k),:);
        end
        %e1 = AB, e2 = BC, e3 = CA
        value = 2*sqrt(3)*cross([e(2,:),0],-[e(1,:),0])/...
            (norm(e(1,:))^2+norm(e(2,:))^2+norm(e(3,:))^2);
        alpha(j) = value(3);
    end
    %beta(eid,qid) = min(alpha);
    beta(eid,qid) = 2*min(alpha)/sqrt(3); %modify to 1
    ngnum = 0; %count genative alpha_i
    for j = 1:4
        if alpha(j)<0
            ngnum = ngnum+1;
        end
    end
    %ngnum = 1: convex element
    if ngnum > 1 %inverted
        beta(eid,qid) = beta(eid,qid)-(ngnum-1);
    end
    %alpha = sort(alpha);
    %for each triangle
    %ccw: cross>0(++++), cw:cross<0(----)
    %convex element: -+++
    %inverted element: --++
    %{
    if alpha(1)<=0 && alpha(4)>0 %inverted or concave element
        beta(eid,qid) = -1;
    elseif alpha(1)<0 && alpha(4)<0 %cw
        beta(eid,qid) = alpha(3)*alpha(4)/(alpha(1)*alpha(2));
    else
        beta(eid,qid) = alpha(1)*alpha(2)/(alpha(3)*alpha(4));
    end
    %}
end


