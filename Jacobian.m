function Jacobian(Q,hermite)
%plot Jacobian on each element
%INPUT:
%Q: quad class
%hermite: flag to indicate hermite function, 0:not hermite
%OUTPUT: plot

Ne = size(Q.Q,1); %num of elements
N = 11; %num of pt on each direction to evaluate Jacobian
x_plot = zeros(N,N,Ne);
y_ploy = zeros(N,N,Ne);
J = zeros(N,N,Ne);
Js = zeros(N,N,Ne); %scaled Jacobian
for i = 1:Ne
    [xi0,eta0] = meshgrid(0:1.0/(N-1):1,0:1.0/(N-1):1);
    x = zeros(4,2);
    for j = 1:4
        x(j,:) = Q.P(Q.Q(i,j),:);
    end
    
    Gamma = cell(4,1); %store gamma functions
    dGamma = cell(4,1);
    for j = 1:4 %each quad edge
        p0 = Q.H(1,:,Q.QE(i,j));
        p1 = Q.H(2,:,Q.QE(i,j));
        m0 = Q.H(3,:,Q.QE(i,j));
        m1 = Q.H(4,:,Q.QE(i,j));
        if j==1 || j==2
            if Q.E(Q.QE(i,j),1) ~= Q.Q(i,j) %exchange
                t = p0;
                p0 = p1;
                p1 = t;
                t = m0;
                m0 = -m1;
                m1 = -t;
            end
        end
        if j==3 || j==4
            if Q.E(Q.QE(i,j),1) == Q.Q(i,j) %exchange
                t = p0;
                p0 = p1;
                p1 = t;
                t = m0;
                m0 = -m1;
                m1 = -t;
            end
        end
        if hermite
            Gamma{j} = @(t)(2*t^3-3*t^2+1)*p0...
                +(t^3-2*t^2+t)*m0...
                +(-2*t^3+3*t^2)*p1...
                +(t^3-t^2)*m1;
            dGamma{j} = @(t) (6*t^2-6*t)*p0+...
                +(3*t^2-4*t+1)*m0...
                +(-6*t^2+6*t)*p1...
                +(3*t^2-2*t)*m1;
        else %straight sides
            Gamma{j} = @(t)(1-t)*p0+t*p1;
            dGamma{j} = @(t)-p0+p1;
        end
    end
    for m = 1:N
        for n = 1:N
            xi = xi0(m,n);
            eta = eta0(m,n);
            dxxi = x(1,:)*(1-eta)-x(2,:)*(1-eta)-x(3,:)*eta+x(4,:)*eta...
                +dGamma{1}(xi)*(1-eta)+dGamma{3}(xi)*eta...
                +Gamma{2}(eta)-Gamma{4}(eta);
            dxeta = x(1,:)*(1-xi)-x(4,:)*(1-xi)-x(3,:)*xi+x(2,:)*xi...
                +dGamma{4}(eta)*(1-xi)+dGamma{2}(eta)*xi...
                +Gamma{3}(xi)-Gamma{1}(xi);
            %J will be negative if vertices are cw
            J(m,n,i) = abs(dxxi(1)*dxeta(2)-dxxi(2)*dxeta(1));
            xy = (1-eta)*Gamma{1}(xi)+eta*Gamma{3}(xi)...
                +(1-xi)*Gamma{4}(eta)+xi*Gamma{2}(eta)...
                -x(1,:)*(1-xi)*(1-eta)-x(2,:)*xi*(1-eta)...
                -x(3,:)*eta*xi-x(4,:)*(1-xi)*eta;
            x_plot(m,n,i) = xy(1);
            y_ploy(m,n,i) = xy(2);
        end
    end
    Jmax = max(reshape(J(:,:,i),[],1));
    Js(:,:,i) = J(:,:,i)/Jmax;
    
end
%plot
figure
for i = 1:Ne
    surf(x_plot(:,:,i),y_ploy(:,:,i),J(:,:,i),'EdgeColor','None')
    hold on
end
hold off
axis equal
view(2)

%plot
figure
for i = 1:Ne
    surf(x_plot(:,:,i),y_ploy(:,:,i),Js(:,:,i),'EdgeColor','None')
    hold on
end
hold off
axis equal
view(2)
end


