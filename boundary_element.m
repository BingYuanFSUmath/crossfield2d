%decide whether an element is bdy element(one vertex is on bdy) or not
%TR: tiangulation
%C: nx2 bdy edges, store vertex id
%id: element id

function status = boundary_element(TR,C,id)
status = false;
Tri_ids = TR.ConnectivityList(id,:); %copy triangle vertex ids
for i = 1:3
    if ismember(Tri_ids(i),C)
        status = true;
        break
    end
end