function SL_bdy = boundarySL(p_bdy,ex)
%find boundary streamlines
%INPUT:
%p_bdy: boundary points
%ex: example class
%OUTPUT:
%SL_bdy: column cell stores bdy SL start/end p_bdy id

SL_bdy = cell(ex.curve_num,1);
for i = 1:ex.curve_num
    list = []; %store p_bdy ids on curve_i
    for j = 1:size(p_bdy,2)
        if p_bdy(j).curve_id == i
            list(end+1) = j;
        end
    end
    for j = 2:size(list,2)
        for k = size(list,2):-1:j
            if p_bdy(list(k)).t < p_bdy(list(k-1)).t
                temp = list(k-1); %exchange list k/k-1
                list(k-1) = list(k);
                list(k) = temp;
            end
        end
    end
    SL_bdy{i} = [list' [list(2:end)';list(1)]];
    %[p_bdy(list).t]' %test-ok
end


