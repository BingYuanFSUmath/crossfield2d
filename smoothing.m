function Q = smoothing(Q,hmin,fd,ex,s_bdy,poly)
%smoothing method
%INPUT:
%Q: Quad class
%hmin: smallest grid size
%OUTPUT:
%Q: Quad class after smoothing
p = Q.P; %point coordinates
beta = distortion_metric(p,Q.Q); %initial distortion
move_tol = 0.01*hmin; %%%%%%%%%what's the ctriteria?
niter = 1;
max_iter = 5; %max number of iteration
move = ones(size(p,1),1); %movability of pts
move_num = 1; %number of pts moved
dmax = 2*move_tol; %max move distance
while (niter<=max_iter && move_num>0 && dmax>=move_tol)
    dmax = 0; 
    move_num = 0; 
    %n_bdy_pt = 0; %num of bdy pts
    for i = 1:size(p,1)
        if move(i)==0 %p is not movable or deactivated
            continue
        end
        %Laplacian smoothing
        if niter<3 %not moved by optimization
            [Q,pnew,d] = Laplacian_smooth(Q,i,ex,beta,s_bdy);
            %[Q,pnew,d] = Weighted_Laplacian_smooth(Q,i,fd,ex,beta,s_bdy,poly);
            %Add bdy layer feature later
            if abs(d)<move_tol
                move(i) = 0;
            else
                move_num = move_num+1;
                move(unique(Q.E(Q.C{i,:},:)))=1;
                %active neighbours of p_i
                if abs(d)>dmax
                    dmax = abs(d); %record max move
                end
                quad_ids = neighbour_quads(Q,i);
                beta(quad_ids) = distortion_metric(p,Q.Q(quad_ids,:));
                p(i,:) = pnew;
            end
        end
        %Add optimization/angle-based smoothing later
    end
    Q.P = p;
    niter = niter+1;
end